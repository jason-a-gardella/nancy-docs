(TeX-add-style-hook
 "preamble"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "ebproof"
    "amssymb"
    "amsmath"
    "amsthm"
    "enumitem"
    "stmaryrd"
    "bm"
    "listings"
    "hyperref"
    "setspace")
   (TeX-add-symbols
    '("xtwoheadrightarrow" ["argument"] 1)
    '("inecq" 1)
    '("inecf" 1)
    '("canontrlctx" 1)
    '("tibranches" 3)
    '("tibranch" 2)
    '("itetrm" 3)
    '("eqtrm" 2)
    '("plustrm" 2)
    '("booltrm" 1)
    '("inttrm" 1)
    '("titrm" 1)
    '("lettrm" 3)
    '("bangtrm" 2)
    '("apptrm" 2)
    '("lamtrm" 2)
    '("avartrm" 1)
    '("vartrm" 1)
    '("itetrl" 3)
    '("eqtrl" 2)
    '("plustrl" 2)
    '("trpltrl" 1)
    '("lettrl" 3)
    '("apptrl" 2)
    '("lamtrl" 2)
    '("titrl" 2)
    '("bbtrl" 3)
    '("batrl" 3)
    '("ttrllong" 2)
    '("ttrl" 2)
    '("rtrl" 1)
    '("rcd" 1)
    '("itewit" 3)
    '("eqwit" 2)
    '("pluswit" 2)
    '("tiwit" 1)
    '("letwit" 3)
    '("bangwit" 1)
    '("appwit" 2)
    '("lamwit" 2)
    '("avarwit" 1)
    '("varwit" 1)
    '("boolwit" 1)
    '("intwit" 1)
    '("mstep" 2)
    '("sstep" 2)
    '("bstep" 4)
    '("reduce" 3)
    '("withenvs" 3)
    '("trmtypwit" 3)
    '("trmtyp" 2)
    '("inwenv" 1)
    '("intenv" 1)
    '("dom" 1)
    '("bangtyp" 2)
    '("lamtyp" 2)
    '("trmsubfull" 5)
    '("trmsubrule" 2)
    '("trmsub" 1)
    '("trlsubfull" 5)
    '("trlsubrule" 2)
    '("trlsub" 1)
    '("witsubauditedrule" 2)
    '("witsubsimplerule" 2)
    '("appsub" 3)
    '("sub" 2)
    '("cdrule" 2)
    '("cd" 1)
    '("tgtrule" 2)
    '("tgt" 1)
    '("srcrule" 2)
    '("src" 1)
    '("namesigdate" 1)
    "twoheadrightarrowfill"
    "betabox"
    "lamh"
    "lamhc"
    "inttyp"
    "booltyp"
    "noenv"
    "tenv"
    "wenv"
    "trmbranches"
    "witbranches"
    "trlbranches"
    "rbrch"
    "tbrch"
    "betabrch"
    "betaboxbrch"
    "tibrch"
    "lambrch"
    "appbrch"
    "letbrch"
    "trplebrch"
    "trplcbrch"
    "plusbrch"
    "eqbrch"
    "itebrch"
    "dbrch"
    "hole"
    "ecf"
    "ecq")
   (LaTeX-add-amsthm-newtheorems
    "exmp")
   (LaTeX-add-amsthm-newtheoremstyles
    "break")))

