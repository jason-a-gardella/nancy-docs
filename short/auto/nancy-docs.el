(TeX-add-style-hook
 "nancy-docs"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "preamble")))

