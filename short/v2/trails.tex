

\section{Trails}
\label{sec:trails}

A trail is
an expression representing a computation from a source term
to a target term. Configurations decorate
audited units with the current execution trail ($\bangtrm{\rho}{M}$,
where $\rho$ denotes a trail) and are presented in Sec.~\ref{sec:configurations}.
The set \trailsName\ of \defin{trails} is defined as follows:
\begin{center}
 $\begin{array}{rcl}
  \rho, \sigma, \tau, \mu, \nu &::=& \rtrl{s} \,|\,\batrl{a}{s}{t}\,|\,\bbtrl{u}{s}{t}\,|\,\titrl{\rho}{A}\,|\,\ttrllong{\rho}{\sigma}\,|\,\lamtrl{a}{\rho}\,|\,\apptrl{\rho}{\sigma}\,|\,\lettrl{u}{\rho}{\sigma} 
                  % |&\ &\trpltrl{\trlbranches}
  \end{array}$
\end{center}


The trail consisting of a term $s$ denotes a \defin{unit step} from
$s$ to itself. A \defn{unary step} is a trail which has a unique occurrence of
$\batrl{a}{s}{t}$, $\bbtrl{u}{s}{t}$ or $\titrl{\rho}{A}$, denoting
one of three kinds of computational steps that can take place in \lamh: a
$\beta$-step, a $\betabox$-step or a trail inspection step,
resp. These steps are described in detail in 
Sec.~\ref{sec:cbv}. Expression $\ttrllong{\rho}{\sigma}$ is the composition of
$\rho$ with $\sigma$. Expressions $\lamtrl{a}{\rho}$,
$\apptrl{\rho}{\sigma}$ and $\lettrl{u}{\rho}{\sigma}$ denote
computation taking place under lambda, application and let, resp. 

Trails are equipped with a minimal structure which we borrow from
proof-terms in first-order term rewriting~\cite[8.3.1]{TERESE:2003}. This
structure includes,  among other things, the statement that composition is associative and that unit steps are neutral for
composition.

\begin{definition}
\label{def:structural_equivalence}
Structural equivalence of trails is the contextual closure
of the following axioms: 
\begin{center}
  $
      \begin{array}{rcl}
        \rho;s & \simeq & \rho \\
        s;\rho & \simeq &\rho \\
        (\rho;\sigma);\tau & \simeq & \rho;(\sigma;\tau) \\
        (\apptrl{\rho_1}{\sigma_1}); (\apptrl{\rho_2}{\sigma_2}) & \simeq & \apptrl{(\rho_1;\rho_2)}{(\sigma_1;\sigma_2)}\\
        \lamwit{a}{\rho};\lamwit{a}{\sigma} &\simeq & \lamwit{a}{(\rho;\sigma)}\\
        \letwit{u}{\rho_1}{\sigma_1}; \letwit{u}{\rho_2}{\sigma_2} & \simeq & \letwit{u}{\rho_1;\rho_2}{\sigma_1;\sigma_2}
      \end{array}
 $
\end{center}
  \end{definition}


  Unique representatives of $\simeq$-equivalence classes are known as
  trails in \defin{canonical form}.   The canonical form of $\rho$,
  written $\canonical{\rho}$, can be obtained by computing the
  (unique)$\stepcan$-normal form of
    $\rho$, where $\stepcan$ is the contextual closure of:

    \[
      \begin{array}{rcl}
        \rho;s & \stepcan & \rho \\
        s;\rho & \stepcan &\rho \\
        (\rho;\sigma);\tau & \stepcan & \rho;(\sigma;\tau) \\
        (\apptrl{\rho_1}{\sigma_1}); (\apptrl{\rho_2}{\sigma_2}) & \stepcan & \apptrl{(\rho_1;\rho_2)}{(\sigma_1;\sigma_2)}\\
        \lamwit{a}{\rho};\lamwit{a}{\sigma} &\stepcan & \lamwit{a}{(\rho;\sigma)}\\
        \letwit{u}{\rho_1}{\sigma_1}; \letwit{u}{\rho_2}{\sigma_2} & \stepcan & \letwit{u}{\rho_1;\rho_2}{\sigma_1;\sigma_2}\\
        (\apptrl{\rho_1}{\sigma_1});
        ((\apptrl{\rho_2}{\sigma_2});\tau) & \stepcan & (\apptrl{(\rho_1;\rho_2)}{(\sigma_1;\sigma_2)});\tau\\
        \lamwit{a}{\rho};((\lamwit{a}{\sigma});\tau) &\stepcan & (\lamwit{a}{(\rho;\sigma)});\tau\\
        \letwit{u}{\rho_1}{\sigma_1};
        ((\letwit{u}{\rho_2}{\sigma_2});\tau) & \stepcan & (\letwit{u}{\rho_1;\rho_2}{\sigma_1;\sigma_2});\tau\\
      \end{array}
    \]

    \begin{lemma}{\cite[8.3.6]{TERESE:2003}}
      $\stepcan$ reduction is strongly normalizing and confluent.
    \end{lemma}
    

 \textbf{Typing for Trails.}  A judgement of the form
$\judgEq{\wenv}{\tenv}{\rho}{s\succcurlyeq_A t}$ is a \defin{trail
  typing judgement}. The meaning of such judgements is given by the
rules in Fig.~\ref{fig:trail_typing}. Most of these rules are
self-explanatory. For example, $\indrulename{R-Refl}$ states that $s$
is a trail from $s$ to itself. Rules $\indrulename{R-$\beta$}$ and
$\indrulename{R-$\betabox$}$ record the redex itself in the trail,
together with an indication that it took place. Rule
$\indrulename{R-TI}$ is about the trail inspection operation. As hinted at above, the target
of this step is a term denoting an iterator over the Church encoding of
the canonical form of the trail.
\begin{definition}[Trail iterator]
  \label{def:trail_iterator}
  The iterator for $\rho$, written $\rec{\rho}$, is defined as follows, where $\vec{a}$ is the
sequence of term variables
$a_r,a_{ba},a_{bb},a_{ti},a_{lam},a_{app},a_{let}$ and $\lambda\vec{a}$
is shorthand for $\lambda a_r.\ldots\lambda a_{let}$:
\begin{center}
 $\begin{array}{rcl}
  \rec{\rtrl{s}} & \eqdef\ & \lamtrmu{\vec{a}}{a_r} \\
  \rec{\batrl{a}{s}{t}} & \eqdef\ & \lamtrmu{\vec{a}}{a_{ba}} \\
  \rec{\bbtrl{u}{s}{t}} & \eqdef\ & \lamtrmu{\vec{a}}{a_{bb}} \\
  \rec{\titrl{\rho}{A}} & \eqdef\ & \lamtrmu{\vec{a}}{a_{ti}\,(\rec{\rho}\,\vec{a})} \\
  \rec{\ttrllong{\rho}{\sigma}} & \eqdef\ & \lamtrmu{\vec{a}}{a_t\, (\rec{\rho}
    \vec{a})\, (\rec{\sigma}\,\vec{a})} \\
  \rec{\lamtrl{a}{\rho}} & \eqdef\ & \lamtrmu{\vec{a}}{a_{lam}}\,(\rec{\rho}\,\vec{a})\\
  \rec{\apptrl{\rho}{\sigma}} & \eqdef\ & \lamtrmu{\vec{a}}{a_{app}\, (\rec{\rho}\,\vec{a})\,(\rec{\sigma}\,\vec{a})} \\
  \rec{\lettrl{u}{\rho}{\sigma}} & \eqdef\ & \lamtrmu{\vec{a}}{a_{let}\,(\rec{\rho}\,\vec{a})\,(\rec{\sigma}\,\vec{a})} 
%                  |&\ &&\trpltrl{\trlbranches}
\end{array}$
\end{center}
\end{definition}
 Since, when presenting the
operational semantics we shall be reasoning over terms modulo
structural equivalence, we want to avoid the behavior of  trail
inspection to differ  in the case of structurally equivalent
trails. Hence why the target in $\indrulename{R-TI}$ is $\rec{\canonical{\rho}}$
rather than $\rec{\rho}$. 

Finally, perhaps most notable is the lack of a rule in
Fig.~\ref{fig:trail_typing} for denoting
computation that takes place under a binder ``!''. Reduction under a
binder is not visible outside its scope.
\begin{remark}
On a more technical note, in the original presentation of
\lamhold~\cite{DBLP:conf/ictac/BaveraB10,DBLP:journals/logcom/BaveraB18}, migration of trails to their
enclosing ``!'' is modeled through 
permutative conversions. These allow trails to be permuted after all
constructors except ``!''. There seems to be no obvious way to define
permutation past ``!'', thus naturally leading to a local scope for
trails. 
\end{remark}

 Given $\wenv$ and $\tenv$, if  $\rho$ is typable, then there is a unique
 $s\succcurlyeq_{A} t $ such that the judgement $
 \judgEq{\wenv}{\tenv}{\rho}{s\succcurlyeq_{A} t}$ is derivable. For
 typable $\rho$ we 
  define $\src{\rho}\eqdef s$ and $\tgt{\rho}\eqdef t$.
We say a typable trail $\rho$ is \defin{composable} with another typable
trail $\sigma$ iff
$\tgt{\rho}=\src{\sigma}$. Moreover,  if we restrict $\simeq$ to
typed trails, then $\rho\simeq \sigma$ implies $\src{\rho}=\src{\sigma}$ and $\tgt{\rho}=\tgt{\sigma}$.

 
    

\begin{figure}[t]
  \begin{center}
    $
  \begin{array}{c}
  \reduce{R-Refl}
  {
    \judgTerm{\wenv}{\tenv}{s}{A}
  }{
    \judgEq{\wenv}{\noenv}{s}{s\succcurlyeq_A s}
    }
    \quad
  \reduce{R-Trans}
  {
    \judgEq{\wenv}{\tenv}{\rho}{r\succcurlyeq_A s}
    \quad
    \judgEq{\wenv}{\tenv}{\sigma}{s\succcurlyeq_A t}
  }{
    \judgEq{\wenv}{\tenv}{\ttrl{\rho}{\sigma}}{r\succcurlyeq_A t}
    }
    \\
    \\
  \reduce{R-$\beta$}
  {
    \judgTerm{\wenv}{\tenv,a:A}{s}{B}
    \quad
    \judgTerm{\wenv}{\tenv}{t}{A}
  }{
    \judgEq{\wenv}{\tenv}{\batrl{a}{\apptrm{(\lamtrm{a}{s})}{t}}{t}}{\apptrm{(\lamtrm{a}{s})}{t}\succcurlyeq_{B} s\sub{t}{a}}
    }
    \\
    \\
    
  \reduce{R-$\betabox$}
  {
    \judgTerm{\wenv}{\cdot}{s}{A}
    \quad
      \judgTerm{\wenv, u : A}{\tenv}{t}{C}
  }{
    \judgEq{\wenv}{\tenv}{\bbtrl{u}{\lettrm{u}{\bangtrm{}{s}}{t}}{t}}{\lettrm{u}{\bangtrm{}{s}}{t}\succcurlyeq_{C\sub{s}{u}} t\sub{s}{u}}
    }
    \\
    \\
      \reduce{R-TI}
  {
        \judgEq{\wenv}{\tenv}{\rho}{s\succcurlyeq_{A}
      t}
 }{
    \judgEq{\wenv}{\tenv}{\titrl{\rho}{A}}{\tiwit{A}\succcurlyeq_{A}
      \rec{\canonical{\rho}}}
  }
    \\
    \\
  \reduce{R-ABS}
  {
    \judgEq{\wenv}{\tenv, \trmtyp{\vartrm{a}}{A}}{\rho}{s\succcurlyeq_B t}
  }{
    \judgEq{\wenv}{\tenv}{\lamtrl{a}{\rho}}{\lamtrm{a}{s}\succcurlyeq_{\lamtyp{A}{B}} \lamtrm{a}{t}}
  }
  \quad
  \reduce{R-APP}
  {
    \judgEq{\wenv}{\tenv}{\rho}{s_1\succcurlyeq_{\lamtyp{A}{B}} t_1}
    \quad
    \judgEq{\wenv}{\tenv}{\sigma}{s_2\succcurlyeq_A t_2}
 }{
    \judgEq{\wenv}{\tenv}{\apptrl{\rho}{\sigma}}{\appwit{s_1}{s_2}\succcurlyeq_B \appwit{t_1}{t_2}}
    }
    \\
    \\
  \reduce{R-LET}
  {
    
     \judgEq{\wenv}{\tenv}{\rho}{s_1\succcurlyeq_{\bangtyp{r}{A}} t_1}
    \quad
    \judgEq{\wenv}{\tenv,u:A}{\sigma}{s_2\succcurlyeq_C t_2}
  }{
    \judgEq{\wenv}{\tenv}{\apptrl{\rho}{\sigma}}{\letwit{u}{s_1}{s_2}\succcurlyeq_{C\sub{r}{u}} \letwit{u}{t_1}{t_2}}
    }
    \end{array}
$
\end{center}
\caption{Trail Typing Rules}\label{fig:trail_typing}
\end{figure}




 

%$\tgt{\trlbranches}$ is defined pointwise.
% As mentioned above, each of 
% $\batrl{a}{s}{t}$, $\bbtrl{u}{s}{t}$ and $\titrl{\rho}{A}$, witnesses
% one of three possible reduction steps. These will be
% presented in detail in Sec.~\ref{sec:evaluation}, however the reader familiar with lambda
% calculus and modal logic might recognize the first two but not the
% third. As mentioned, reduction of the constant $\titrm{A}$ will produce an iterator
% for the trail currently in scope (\ie the one inside the innermost
% enclosing !-constructor), denoted $\rec{\canonical{\rho}}$. 
    
We conclude this subsection with two notions of substitution involving
trails. 

\begin{center}
  \begin{tabular}{|l|l|}
    \hline
    Substitution of trails in terms & $s\sub{\rho}{u}$ \\ \hline

    Substitution of audited unit variables with terms in trails & $\rho\sub{s}{u} $ \\\hline
    \end{tabular}
  \end{center}
%\begin{definition}[Substitution of Trails in Terms]
%\label{def:substitution:trails:in:terms}
The result of substituting all free occurrences of audited unit
variable $u$ in $s$ is the trail denoted $s\sub{\rho}{u}$ and defined
as follows:
\begin{center}
$\begin{array}{rcl}
  \titrm{A}\sub{\rho}{u} &\eqdef &\titrm{A} \\
  \vartrm{a} \sub{\rho}{u} &\eqdef &a \\
  \avartrm{v} \sub{\rho}{u} &\eqdef &\begin{cases}
    \rho,  &u = v \\
    v, &u \neq v
  \end{cases} \\
  (\lamtrm{a}{s}) \sub{\rho}{u} & \eqdef &\lamtrl{a}{s \sub{\rho}{u}  } \\
  (\apptrm{s}{t}) \sub{\rho}{u} &\eqdef &\apptrm{s \sub{\rho}{u} }{t \sub{\rho}{u} } \\
  (\bangwit{s}) \sub{\rho}{u} &\eqdef  &\bangwit{s\sub{\src{\rho}}{u}} \\
  (\lettrm{v}{s}{t}) \sub{\rho}{u} &\eqdef  &\lettrl{v}{s \sub{\rho}{u}}{t \sub{\rho}{u} } 
%  \trlsubrule{\titrm{\trmbranches}}{\trpltrl{\trlsub{\trmbranches}}}
\end{array}$
\end{center}
%\end{definition}
The role of the next-to-last clause above, is best explained
after we introduce substitution of audited units for audited unit
variables (in Sec.~\ref{sec:configurations}).
%(Def.~\ref{def:substitution:auditedunits:for:auditedvariables})
%\begin{definition}[Substitution of Terms in Trails]
%\label{def:substitution:terms:in:trails}
The result of substituting all free occurrences of audited unit
variable $u$ in $\rho$ with term $s$ is the trail denoted $\rho\sub{s}{u}$ and defined
as follows:
\begin{center}
  $\begin{array}{rcl}
  \rtrl{t}\sub{s}{u} &\eqdef &\rtrl{t}\sub{s}{u} \\
  \batrl{a}{r}{t} \sub{s}{u} &\eqdef & \batrl{a}{r \sub{s}{u}}{t \sub{s}{u}} \\
  \bbtrl{u}{r}{t} \sub{s}{u} &\eqdef &\bbtrl{u}{r \sub{s}{u}}{t \sub{s}{u}}\\
   \titrl{\rho}{A} \sub{s}{u} &\eqdef &\titrl{\rho \sub{s}{u}}{A\sub{s}{u}} \\
  (\ttrllong{\rho}{\sigma}) \sub{s}{u} &\eqdef &\ttrllong{\rho \sub{s}{u}}{\sigma\sub{s}{u}}   \\
  (\lamtrl{a}{\rho}) \sub{s}{u} &\eqdef &\lamtrl{a}{\rho \sub{s}{u}} \\
  (\apptrl{\rho}{\sigma}) \sub{s}{u} &\eqdef  &\apptrl{\rho \sub{s}{u}}{\sigma \sub{s}{u}}  \\
  (\lettrl{v}{\rho}{\sigma}) \sub{s}{u} &\eqdef  &\lettrl{v}{\rho \sub{s}{u}}{\sigma \sub{s}{u}} 
   \end{array}$
 \end{center}

  %\end{definition}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
