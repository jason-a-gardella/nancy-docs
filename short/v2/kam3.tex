
\subsection{HKAM}

This section presents an abstract machine (AM) called the \hkam (``A''
for audited), for 
CBN reduction in \lamh based on Krivine's KAM~\cite{DBLP:journals/lisp/Krivine07}. States of the \hkam are tuples of the form
$\kams{s}{e}{\pi}{\xi}{\delta}$ where $s$ is a source \textbf{term}, $e$ is an
\defin{environment}, $\pi$ is a \defin{stack}, $\xi$ is an
\defin{audit trail} and $\delta$ is a \defin{dump}:
  \begin{center}
    $\begin{array}{rcl}
            e & ::= &
               \epsilon\,|\,\pushenv{\map{a}{(t,e)}}{e}\,|\,\pushenv{\map{u}{(\bangtrm{\sigma}{V},e)}}{e}\\

\pi & ::= &
            \epsilon\,|\,\pushst{(t,e)}{\pi}\,|\,\pushst{(t,u,e)}{\pi}\\

     \xi & ::= & (e,s)\,|\,\pushtrl{\batrlk{R}}{\xi}\,|\,\pushtrl{\bbtrll{R}}{\xi}\,|\,\pushtrl{\titrlk(\pi)}{\xi}\,|\,\pushtrl{(\xi,\pi)}{\xi}
                                               \\

\delta & ::= & \epsilon \,|\, \pushdmp{(\xi,\pi)}{\delta}\\

     
     V & ::= & \lamtrm{a}{M}\,|\,\bangtrm{\xi}{V}\\
     
     \end{array}$
   \end{center}
In the notion of value above, $M$ is actually a configuration but where
the trail decorations are in the syntax of trails defined above using
stack notation. The details of the tuple $R$ in $\batrlk{R}$ and $\bbtrll{R}$ above is given below together with the rules
of the AM.
   Given a closed source term $s$, the starting state of the 
AM is  $\kams{s}{\epsilon}{\epsilon}{(\epsilon,s)}{\epsilon}$. The
rules describing the behavior of the \hkam are given in
Fig.~\ref{fig:rules:kam}. Rules $\indrulename{pushb}$,
$\indrulename{appb}$ and $\indrulename{lookupt}$ are the rules
for the standard CBN lambda calculus. $\indrulename{pushb}$ pushes the
argument of an application into the stack (which may be thought of as
the context under which the term in focus is currently placed). Rule
$\indrulename{appb}$ processes the application of an abstraction to an
argument by moving the argument from the stack into the
environment. It also records a witness to the $\beta$-step in the
current trail: $\pushtrl{\batrlk{R}}{\xi}$. The components of the
tuple $R$ will be used for reading back the trail when synthesing a configuration
from an AM state in our simulation proposition (Prop.~\ref{prop:correctness:hkam}). Rule
$\indrulename{lookupt}$ performs lookup for truth variables.  
The remaining rules are specific to \lamh.  Rule
$\indrulename{pushlet}$ plays a similar role to
$\indrulename{push}$ but for let-terms. Rule $\indrulename{bang}$ starts computation
inside an audited unit. Note that the starting trail is the unit step
$(e,s)$. The current stack $\pi$ and trail $\xi$ are placed in the
dump since computation in $s$ will not be recorded in $\xi$.
The related rules $\indrulename{bangbang}$ and $\indrulename{appbb}$
deal with the case where computation inside an audited unit has
concluded. Notice first that the stack is required to be empty since
the value cannot be applied or be the argument of a let-term. The
difference between $\indrulename{bangbang}$ and $\indrulename{appbb}$
is that the former deals with the case in which we have nested audited
units such as when evaluating $\bangtrm{}{\bangtrm{}{s}}$. The latter
covers the case where the just computed audited unit is an argument in
a let-term. Rule $\indrulename{lookupv}$ performs variable lookup for
validity variables\footnote{See Sec.~\ref{sec:conclusions} for further
  comments on this rule.}. Finally, $\indrulename{ti}$ performs trail
lookup. The expression $\decodetrail{\xi}$ is the readback of $\xi$
and is defined below.


\begin{figure}[t]
  \begin{center}
  $\begin{array}{llll}

     \kams{\apptrm{s}{t}}{e}{\pi}{\xi}{\delta}
     & \sstepaxrel_{\indrulename{push}} &
     \kams{s}{e}{\pushst{(t,e)}{\pi}}{\xi}{\delta}
     \\

     \kams{\lamtrm{a}{s}}{e}{\pushst{(t,e')}{\pi}}{\xi}{\delta}
     & \sstepaxrel_{\indrulename{appb}} & 
     \kams{s}{\pushenv{\map{a}{(t,e')}}{e}}{\pi}{\pushtrl{\batrlk{R}}{\xi}}{\delta}
     & R=(\lamtrm{a}{s},e,t,e',\pi)
     \\


     \kams{a}{e}{\pi}{\xi}{\delta}
     & \sstepaxrel_{\indrulename{lookupt}} & 
                     \kams{t}{e'}{\pi}{\xi}{\delta} &  e(a) = (t,e')
                                               \\


               \kams{\lettrm{u}{s}{t}}{e}{\pi}{\xi}{\delta}
     & \sstepaxrel_{\indrulename{pushlet}} & 
                     \kams{s}{e}{\pushst{(t,u,e)}{\pi}}{\xi}{\delta} & 
                                               \\

%     \kams{\bangtrm{\sigma}{s}}{e}{\pushst{(t,u,e')}{\pi}}{\xi}
%     & \sstepaxrel & 
%                     \kams{t}{\pushenv{(s,e,\sigma)}{e'}}{\pi}{\pushtrl{\bbtrll{\decompile{R}}{\pi}}{\xi}}
%     &  R = \lettrm{u}{\bangtrm{\sigma}{s}}{t}
%                                               \\

          \kams{\bangtrm{}{s}}{e}{\pi}{\xi}{\delta}
     & \sstepaxrel_{\indrulename{bang}} & 
                     \kams{s}{e}{\epsilon}{(e,s)}{\pushdmp{(\xi,\pi)}{\delta}}
     & 
                                               \\

               \kams{V}{e}{\epsilon}{\sigma}{\pushdmp{(\xi,\epsilon)}{\delta}}
     & \sstepaxrel_{\indrulename{bangbang}} & 
                     \kams{\bangtrm{\sigma}{V}}{e}{\epsilon}{\xi}{\delta}
     & 
                                               \\

               \kams{V}{e}{\epsilon}{\sigma}{\pushdmp{(\xi,\pushst{(t,u,e')}{\pi})}{\delta}}
     & \sstepaxrel_{\indrulename{appbb}} & 
                     \kams{t}{\pushenv{\map{u}{(\bangtrm{\sigma}{V},e')}}{e}}{\pi}{\pushtrl{\bbtrll{R}}{\xi}}{\delta}
     & R = (V,e,t,u,e',\sigma,\pi)
                                               \\
     
     \kams{u}{e}{\pi}{\xi}{\delta}
     & \sstepaxrel_{\indrulename{lookupv}} & 
                     \kams{V}{e'}{\pi}{\xi}{\delta}  & e(u)
                                                                    = (\bangtrm{\sigma}{V},e')
                                               \\

     \kams{\titrm{A}}{e}{\pi}{\xi}{\delta}
     & \sstepaxrel_{\indrulename{ti}} & 
                     \kams{\rec{\canonical{\decodetrail{\xi}}}}{e}{\pi}{\pushtrl{\titrlk(\pi)}{\xi}}{\delta} & 
                                               \\

\end{array}$
\end{center}
\caption{Rules of the \hkam}\label{fig:rules:kam}
\end{figure}

A sample reduction in the \hkam follows. It executes the term $
\lettrm{u}{\bangtrm{}{(\apptrm{I}{I})}}{\bangtrm{}{(\apptrm{u}{3})}}$
abbreviated $s$.
 We adopt the following additional abbreviations.
$I=\lamtrm{a}{a}$, $R\eqdef (I,\epsilon,I,\epsilon,\epsilon)$,  $S\eqdef
(I,\epsilon,\bangtrm{}{\apptrm{u}{3}},u,\epsilon,
\pushtrl{\batrl{}{R}{}}{(\epsilon,\apptrm{I}{I})},\epsilon)$, $T\eqdef (I,\epsilon,3,e,\epsilon)$, 
and 
$e\eqdef
\pushenv{\map{u}{(\bangtrm{\pushtrl{\bbtrl{}{R}{}}{(\epsilon,\apptrm{I}{I})}}{I},\epsilon)}}{\epsilon}$.


{\footnotesize
\begin{center}
  $\begin{array}{cccccl}
     \mathbf{term} & \mathbf{env} & \mathbf{stack} & \mathbf{trail} &
                                                                      \mathbf{dump}
     \\\hline
     
   s & \epsilon &
                                                                     \epsilon
     &
        (\epsilon,s)
     & \epsilon 
     \\\hline
     
     \bangtrm{}{(\apptrm{I}{I})} & \epsilon &
                                \pushst{(\bangtrm{}{(\apptrm{u}{3})},u,\epsilon)}{\epsilon}
     &
(\epsilon,s)                                                                    &
                                                                      \epsilon
                                                                        &
     \\\hline

     \apptrm{I}{I} & \epsilon & \epsilon & (\epsilon,\apptrm{I}{I}) &
                                             \pushdmp{((\epsilon,s), \pushst{(\bangtrm{}{(\apptrm{u}{3})},u,\epsilon)}{\epsilon})}{\epsilon}
     \\\hline

         I & \epsilon & \pushst{(I,\epsilon)}{\epsilon} & (\epsilon,\apptrm{I}{I}) &
                                             \pushdmp{((\epsilon,s), \pushst{(\bangtrm{}{(\apptrm{u}{3})},u,\epsilon)}{\epsilon})}{\epsilon}
     \\\hline

           a & \pushenv{\map{a}{(I,\epsilon)}}{\epsilon} & \epsilon & \pushtrl{\batrl{}{R}{}}{(\epsilon,\apptrm{I}{I})} &
                                             \pushdmp{((\epsilon,s), \pushst{(\bangtrm{}{(\apptrm{u}{3})},u,\epsilon)}{\epsilon})}{\epsilon}
     \\\hline

                I & \epsilon & \epsilon & \pushtrl{\batrl{}{R}{}}{(\epsilon,\apptrm{I}{I})} &
                                                                                          \pushdmp{((\epsilon,s), \pushst{(\bangtrm{}{(\apptrm{u}{3})},u,\epsilon)}{\epsilon})}{\epsilon}
     \\\hline

                \bangtrm{}{(\apptrm{u}{3})} & \pushenv{\map{u}{(\bangtrm{\pushtrl{\bbtrl{}{R}{}}{(\epsilon,\apptrm{I}{I})}}{I},\epsilon)}}{\epsilon} & \epsilon & \pushtrl{\bbtrl{}{S}{}}{(\epsilon,s)} &
                                                                                          \epsilon
     \\\hline

     \apptrm{u}{3} & e 
                                & \epsilon & (\pushenv{\map{u}{(\bangtrm{\pushtrl{\bbtrl{}{R}{}}{(\epsilon,\apptrm{I}{I})}}{I},\epsilon)}}{\epsilon},\apptrm{u}{3}) &
                                                                                          \pushdmp{(\pushtrl{\bbtrl{}{S}{}}{(\epsilon,s)},\epsilon)}{\epsilon}
     \\\hline

                     u &
                                e & \pushst{(3,e)}{\epsilon} & (\pushenv{\map{u}{(\bangtrm{\pushtrl{\bbtrl{}{R}{}}{(\epsilon,\apptrm{I}{I})}}{I},\epsilon)}}{\epsilon},\apptrm{u}{3}) &
                                                                                          \pushdmp{(\pushtrl{\bbtrl{}{S}{}}{(\epsilon,s)},\epsilon)}{\epsilon}
     \\\hline

                          I & \epsilon & \pushst{(3,e)}{\epsilon} & (\pushenv{\map{u}{(\bangtrm{\pushtrl{\bbtrl{}{R}{}}{(\epsilon,\apptrm{I}{I})}}{I},\epsilon)}}{\epsilon},\apptrm{u}{3}) &
                                                                                          \pushdmp{(\pushtrl{\bbtrl{}{S}{}}{(\epsilon,s)},\epsilon)}{\epsilon}
     \\\hline

                               a &
                                   \pushenv{\map{a}{(3,e)}}{\epsilon}
                                  &  \epsilon & \pushtrl{\batrl{}{T}{}}{(\pushenv{\map{u}{(\bangtrm{\pushtrl{\bbtrl{}{R}{}}{(\epsilon,\apptrm{I}{I})}}{I},\epsilon)}}{\epsilon},\apptrm{u}{3})} &
                                                                                          \pushdmp{(\pushtrl{\bbtrl{}{S}{}}{(\epsilon,s)},\epsilon)}{\epsilon}
     \\\hline

                                    3 &
                                   \epsilon
                                  &  \epsilon & \pushtrl{\batrl{}{T}{}}{(\pushenv{\map{u}{(\bangtrm{\pushtrl{\bbtrl{}{R}{}}{(\epsilon,\apptrm{I}{I})}}{I},\epsilon)}}{\epsilon},\apptrm{u}{3})} &
                                                                                          \pushdmp{(\pushtrl{\bbtrl{}{S}{}}{(\epsilon,s)},\epsilon)}{\epsilon}
     \\\hline

                                         \bangtrm{\pushtrl{\batrl{}{T}{}}{(e,\apptrm{u}{3})}}{3} &
                                   \epsilon
                                  &  \epsilon & \pushtrl{\bbtrl{}{S}{}}{(\epsilon,s)} &
                                                                                          \epsilon
     \\\hline

   \end{array}$
 \end{center}
} 
The main result of this section (Prop.~\ref{prop:correctness:hkam})  is that the AM is sound with
respect to call-by-name reduction. Stating this result requires
reading back configurations  from machine states:
\begin{center}
$\begin{array}{rcl}
\decodestate{\kams{s}{e}{\pi}{\xi}{\delta}} & \eqdef & \decodedump{\delta}\ctxof{\bangtrm{\decodetrail{\xi}}{\decodest{\pi}\ctxof{\decodeenv{e}\ctxof{\compile{s}}}}}

\end{array}$
\end{center}
We next present each of the components on which this readback function
relies on. The readback function for environments and stacks are as
follows, where $\compile{s}$ is the \defin{compilation} of $s$, meaning replacing
all occurrences of $\bangtrm{}{t}$ in $s$ with
$\bangtrm{t}{t}$ and simply traversing expressions of the form $\bangtrm{\sigma}{V}$. Compilation is required since the readback of \hkam
states is to configurations.




\begin{center}
  $\begin{array}{cc}
     \begin{array}{rcl}
\decodeenv{\epsilon} & \eqdef & \Box\\
\decodeenv{\pushenv{\map{a}{(t,e')}}{e}} & \eqdef &
                                             \decodeenv{e}\ctxof{\Box\sub{a}{\decodeenv{e'}\ctxof{\compile{t}}}}
   \\
\decodeenv{\pushenv{\map{u}{(\bangtrm{\sigma}{V},e')}}{e}} & \eqdef &
                                             \decodeenv{e}\ctxof{\Box\sub{u}{\decodeenv{e'}\ctxof{\bangtrm{\sigma}{\compile{V}}}}}
   \\

     \end{array}
                     &
                       \begin{array}{rcl}
\decodest{\epsilon} & \eqdef & \Box\\
\decodest{\pushst{(t,e)}{\pi}} & \eqdef &
                                             \decodest{\pi}\ctxof{\apptrm{\Box}{\decodeenv{e}\ctxof{\compile{t}}}}
   \\
\decodest{\pushst{(t,u,e)}{\pi}} & \eqdef &
                                             \decodest{\pi}\ctxof{\lettrm{u}{\Box}{\decodeenv{e}\ctxof{\compile{t}}}}
   \\

\end{array}
\end{array}$
\end{center}
 
The readback function for trails makes use of $\decompile{\pi}$ which
is defined to be just $\decompile{\decodest{\pi}}$, where recall from
above that $\decompile{\bullet}$ is the decompilation function.

\begin{center}
$\begin{array}{rcl}
\decodetrail{(e,s)} & \eqdef & \decodeenv{e}\ctxof{s}\\
\decodetrail{\pushtrl{\batrlk{R}}{\xi}} & \eqdef &
                                             \ttrl{\decodetrail{\xi}}{\decompile{\pi}\ctxof{\batrl{a}{\apptrm{\decompile{\decodeenv{e}\ctxof{\lamtrm{a}{s}}}}{\decompile{\decodeenv{e'}\ctxof{t}}}}{\cd{V}}}}
   \\
\decodetrail{\pushtrl{\bbtrll{R}}{\xi}} & \eqdef &
\ttrl{\decodetrail{\xi}}{\decompile{\pi}\ctxof{\ttrl{\bbtrl{u}{\lettrm{u}{\decompile{\decodeenv{e}\ctxof{\bangtrm{\sigma}{V}}}}{\decompile{\decodeenv{e'}\ctxof{t}}}}{\cd{t}}}{\decompile{\decodeenv{e'}\ctxof{t}}\sub{\sigma}{u}}}}
   \\
%\decodetrail{\pushtrl{(\sigma,\pi)}{\xi}} & \eqdef &
 %                                            \ttrl{\decodetrail{\xi}}{\decodest{\pi}\ctxof{\decodetrail{\xi'}}} \\
  
\decodetrail{\pushtrl{\titrlk(\pi)}{\xi}} & \eqdef &
                                                  \ttrl{\decodetrail{\xi}}{\decompile{\pi}\ctxof{\titrl{\decodetrail{\xi}}{A}}}
\end{array}$
\end{center}


The readback function for dumps is:


% \begin{center}
% $\begin{array}{rcl}
% \appenv{e}{\rho} & \eqdef & e_{src}(\rho);e(\rho)\\
% \end{array}$
% \end{center}

% For all $u\in \fvv{\tgt{\rho}}$, such that $e(u)=(\bangtrm{\sigma}{V},e')$,
% the expression $e_{src}(\rho)$ replaces  it with $\src{\sigma}$ in
% $\rho$. The expression $e(\rho)$ is $\tgt{\rho}\sub{u}{\sigma}$.

\begin{center}
$\begin{array}{rcl}
\decodedump{\epsilon} & \eqdef & \Box\\
\decodedump{\pushdmp{(\xi,\pi)}{\delta}} & \eqdef &
                                             \decodedump{\delta}\ctxof{\bangtrm{\decodetrail{\xi}}{\decodest{\pi}}}

\end{array}$
\end{center}

We
can now state the main result of this section.

\begin{proposition}[Correctness of the \hkam]
  \label{prop:correctness:hkam}
If $\kams{s}{e}{\pi}{\xi}{\delta}
      \sstepaxrel_{r} 
     \kams{s'}{e'}{\pi'}{\xi'}{\delta'}
     $, then

     \begin{enumerate}
       \item For
         $r\in\{\indrulename{push}, \indrulename{lookupt},
         \indrulename{pushlet}, \indrulename{bang},
         \indrulename{bangbang}, \indrulename{lookupv}\}$:
%         \begin{center}
           $\decodestate{\kams{s}{e}{\pi}{\xi}{\delta}} =
           \decodestate{\kams{s'}{e'}{\pi'}{\xi'}{\delta'}}$.
%         \end{center}
         

       \item For
         $r\in\{\indrulename{appb}, \indrulename{appbb},
         \indrulename{ti}\}$:
%         \begin{center}
           $\decodestate{\kams{s}{e}{\pi}{\xi}{\delta}} \ssteprel
           \decodestate{\kams{s'}{e'}{\pi'}{\xi'}{\delta'}}$.
%         \end{center}
       \end{enumerate}

       
\end{proposition}





%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
