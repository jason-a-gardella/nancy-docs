

\section{Source Syntax}
\label{sec:source_syntax}

\lamh includes a source syntax and a runtime syntax.  This section introduces the source syntax including its typing
  system. The source syntax is the language in which
  programmers write programs. The runtime syntax 
consists of
configurations and trails.  Configurations denote 
  states that describe the execution of programs and are defined with
  the help of trails.  We 
  present trails in Sec.~\ref{sec:trails} and configurations
  in Sec.~\ref{sec:configurations}. 

\begin{center}
  \begin{tabular}{llll}
    \hline
    \defin{Terms} & \termsName & Source syntax & Sec.~\ref{sec:source_syntax}\\\hline
    \defin{Trails} & \trailsName & Computation history& Sec.~\ref{sec:trails}\\\hline
    \defin{Configurations} & \configsName & Runtime syntax & Sec.~\ref{sec:configurations}\\\hline
    \end{tabular}
  \end{center}


  
We assume given a denumerably infinite set of
\defn{term
variables} $a,b,c,\ldots$ and another one of \defn{audited unit variables}
$u,v,w,\ldots$. The set \termsName\ of \defin{terms} is defined as follows:

\begin{center}
  $\begin{array}{rcl}
  r,s, t & ::=&\
     \tiwit{A}\,|\,\varwit{a}\,|\,\avarwit{u}\,|\,\lamwit{a}{s}\,|\,\appwit{s}{t}\,|\,\bangwit{s}\,|\,\letwit{u}{s}{t}
        \end{array}$
\end{center}

Term variables $a$, abstraction and application are as in the standard
lambda calculus. A term of the form $\bangwit{s}$ is an \emph{audited
(computation) unit}: the symbol ``!'' is a binder for the computation history and has $s$ as
scope. Computation below ``!'' is tracked and can be consulted using
the $\tiwit{A}$ constant; here $A$ is a type, types are introduced below. Reduction of the constant $\titrm{A}$ will produce an iterator
for the trail currently in scope (\ie the one inside the innermost
enclosing !-constructor).  The let-term $\letwit{u}{s}{t}$
allows audited units to be composed. 
We call any of $\varwit{a}$, $\avarwit{u}$ or
$\tiwit{A}$ \emph{atoms} and denoted \atom. We use $x,y,z,\ldots$ to denote variables, be they term
or audited unit variables.

For example, the term 
$\lettrmtype{u}{\mathsf{nat}}{s}{\lettrmtype{v}{\mathsf{nat}}{t}{\mathit{if}\ \tiwit{\mathsf{bool}}\vec{r}\
    \mathit{then}\ u+v\ \mathit{else}\ v}}$
executes audited unit $s$ producing its value and a computation trail
for it, then it similarly executes the audited unit $t$. Finally,
based on the truth of   $\tiwit{\mathsf{bool}}\vec{r}$ it either decides to
compute the sum of the values of $s$ and $t$ or just the value of
$t$. Moreover, trails for the results of $s$ and $t$ are persisted by
the run-time system (failure to do so leads to failure of Subject Reduction). In
the term $\tiwit{\mathsf{bool}}\vec{r}$, the constant $\tiwit{\mathsf{bool}}$ produces
an iterator for the current computation trail and arguments $\vec{r}$
are fed to it. The obtained value will be a boolean determining the
branch to be taken. One may also model  history-based access control and
other features involving computation history~\cite{DBLP:conf/ndss/AbadiF03,DBLP:journals/logcom/BaveraB18}.


%\begin{definition}[Substitution of Terms for Variables]
We conclude this subsection by defining substitution of
terms for variables. The term resulting from replacing all free occurrences of $x$ in $s$
with $t$, written  $s\sub{t}{x}$, is defined as follows: 
\begin{center}
  $\begin{array}{rcl}
  \tiwit{A}\sub{t}{x} &\eqdef\ &{\tiwit{A}} \\
  \varwit{y}\sub{t}{x} &\eqdef\ & {\begin{cases}
    t,  & x = y \\
    \varwit{y}, & x \neq y
  \end{cases}} \\
  (\lamwit{b}{s})\sub{t}{x} &\eqdef\ &{\lamwit{b}{s\sub{t}{x}}} \\
  (\appwit{s}{s'})\sub{t}{x} &\eqdef\ &{\appwit{s\sub{t}{x}}{s'\sub{t}{x}}} \\
   (\bangwit{s})\sub{t}{x} &\eqdef\ &{\bangwit{s\sub{t}{x}}} \\
  (\letwit{u}{s}{s'})\sub{t}{x} &\eqdef\ &{\letwit{u}{s\sub{t}{x}}{s'\sub{t}{x}}} 
%  \witsubsimplerule{\tiwit{\witbranches}}{\tiwit{\witbranches\sub{t}{a}}}
\end{array}$
\end{center}
%\end{definition}


%\subsubsection{Simple Variables}

%For $\gamma = \sub{t}{a}$, substitution of simple variables on a witness $r$,
%denoted as $r\gamma$, is defined as:

% \begin{align}
%   (\tiwit{A})\sub{t}{a} \eqdef\ &{\tiwit{A}} \\
%   \varwit{b}\sub{t}{a} \eqdef\ & {\begin{cases}
%     t,  &a = b \\
%     \varwit{b}, &a \neq b
%   \end{cases}} \\
%   \avarwit{v}\sub{t}{a} \eqdef\ &\avarwit{v} \\
%   (\lamwit{b}{s})\sub{t}{a} \eqdef\ &{\lamwit{b}{s\sub{t}{a}}} \\
%   (\appwit{s}{s'})\sub{t}{a} \eqdef\ &{\appwit{s\sub{t}{a}}{s'\sub{t}{a}}} \\
%   (\bangwit{s})\sub{t}{a} \eqdef\ &{\bangwit{s}} \\
%   (\letwit{u}{s}{s'})\sub{t}{a} \eqdef\ &{\letwit{u}{s\sub{t}{a}}{s'\sub{t}{a}}} 
% %  \witsubsimplerule{\tiwit{\witbranches}}{\tiwit{\witbranches\sub{t}{a}}}
% \end{align}

% %$\witbranches\gamma$ is defined pointwise.

% \subsubsection{Audited Variables}

% %For $\delta = \sub{t}{u}$, substitution of audited variables on a witness $r$,
% %denoted as $r\delta$, is defined as:

% \begin{align}
%   (\tiwit{A})\sub{t}{u} \eqdef\ &{\tiwit{A}} \\
%   \varwit{a}\sub{t}{u} \eqdef\ & \varwit{a} \\
%   \avarwit{v}\sub{t}{u} \eqdef\ & {\begin{cases}
%     t,  &u = v \\
%     \varwit{v}, &u \neq v
%   \end{cases}} \\
%   (\lamwit{b}{s)}\sub{t}{u} \eqdef\ &{\lamwit{b}{s\sub{t}{u}}} \\
%   (\appwit{s}{s'})\sub{t}{u} \eqdef\ &{\appwit{s\sub{t}{u}}{s'\sub{t}{u}}} \\
%   (\bangwit{s})\sub{t}{u} \eqdef\ &{\bangwit{s\sub{t}{u}}} \\
%   (\letwit{v}{s}{s'})\sub{t}{u} \eqdef\ &{\letwit{v}{s\sub{t}{u}}{s'\sub{t}{u}}} 
% %  \witsubauditedrule{\tiwit{\witbranches}}{\tiwit{\witbranches\delta1}}
% \end{align}

%$\witbranches\delta$ is defined pointwise.


\textbf{Typing for Terms.}
Types are defined
as follows:
\begin{center}
  $\begin{array}{rcl}
     A, B, C & ::=& P \,|\,\lamtyp{A}{B}\,|\,\bangtyp{s}{A}
                           \end{array}$
\end{center}
\noindent where $P$ ranges over some given set of base types. Type $\lamtyp{A}{B}$ is the type of functions from $A$ to $B$ and type
$\bangtyp{s}{A}$ is that of terms that evaluate to values of type
$A$ \emph{and} produce a proof $s$ that the term itself has type $A$.
 A term variable typing context $\Gamma$ (resp. audited unit variable
 typing context $\Delta$) is a partial function from term
variables (resp. audited unit variables) to types. From the
viewpoint of the judgemental presentation of modal logic of~\cite{DBLP:journals/jacm/DaviesP01,DBLP:journals/logcom/BaveraB18}, variables in $\Gamma$ are truth hypothesis
and variables in $\Delta$ are validity hypothesis. A source term
typing judgement is an expression of the form
$\judgTerm{\wenv}{\tenv}{s}{A}$ whose meaning is defined by the rules
in Fig.~\ref{fig:source_terms_typing}.

Typing rules $\indrulename{T-Var}$, $\indrulename{T-Abs}$ and
$\indrulename{T-App}$ are standard. Rule $\indrulename{T-TI}$ assigns
the type $\rec{A}$ to the constant $\titrm{A}$. As mentioned, reduction of
$\titrm{A}$ 
produces the unfolding of the
iterator for the Church encoding of (a \emph{canonical} form, see below) of the current
trail in scope  (\cf~Def.~\ref{def:trail_iterator}) whose type is
denoted $\rec{A}$ and abbreviates $\lamtyp{A}{\lamtyp{A}{\lamtyp{A}{\lamtyp{A^2}{\lamtyp{A^3}{\lamtyp{A^2}{\lamtyp{A^3}{\lamtyp{A^3}{A}}}}}}}}$
where $A^n \eqdef \underbrace{A\supset\ldots\supset A\supset A}_{n}$,
$n>0$. The iterator takes one argument for each of the possible
constructors with which trails may be built. These constructors are
presented in Sec.~\ref{sec:trails}.
Rule $\indrulename{T-!}$ types audited units. As usual in \Siv-based
modal type systems, $s$ must not have occurrences of truth
variables. Moreover, notice how the term $s$ is internalized into the type
expression $\bangtyp{s}{A}$.
Finally, $\indrulename{T-Let}$ is the eliminator for
audited units. The type $C$ may have free occurrences of the audited
unit variable $u$ and hence must be substituted in the
conclusion. Expression $C\sub{r}{u}$ substitutes all free occurrences
of $u$ in $C$ with $r$; this operation is defined as expected:
$P\sub{r}{u}\eqdef P$; $(\lamtyp{A}{B})\sub{r}{u}\eqdef
\lamtyp{A\sub{r}{u}}{B\sub{r}{u}}$ and
$(\bangtyp{s}{A})\sub{r}{u}\eqdef \bangtyp{s\sub{r}{u}}{A\sub{r}{u}}$.
%\begin{example}
A sample derivation is:
\scriptsize
  \begin{center}
    $
    \regla
    {
      \regla
      {
        \regla
        {}
        {\judgTerm{\cdot}{a:\bangtyp{s}{A}}{a}{\bangtyp{s}{A}}}
        {}
        \quad
        \regla
        {
         \regla
          {
            \regla
            {}
            {\judgTerm{u:A}{\cdot}{u}{A}}
            {}
          }
          {\judgTerm{u:A}{\cdot}{\bangwit{u}}{\bangtyp{u}{A}}}
          {}
        }
        {\judgTerm{u:A}{a:\bangtyp{s}{A}}{\bangwit{\bangwit{u}}}{\bangtyp{\bangwit{u}}{\bangtyp{u}{A}}}}
        {}
      } {\judgTerm{\cdot}
                       {\tenv,a:\bangtyp{s}{A}}
{\lettrm{u}{a}{\bangwit{\bangwit{u}}}}{\bangtyp{\bangwit{s}}{\bangtyp{s}{A}}}}
      {}
    } {\judgTerm{\cdot}{\tenv}{\lamtrmu{a^{\bangtyp{s}{A}}}{\lettrm{u}{a}{\bangwit{\bangwit{u}}}}}{\lamtyp{\bangtyp{s}{A}}{\bangtyp{\bangwit{s}}{\bangtyp{s}{A}}}}}
    {}
        $
    \end{center}
%\end{example}
\normalsize
%An audited unit $\bangwit{s}$ denotes a ``proof'' that $s$ is a proof of $A$.


Were one to define reduction on the source syntax, Subject Reduction would fail~\cite{DBLP:journals/logcom/BaveraB18} since
although derivations are internalized into the object logic,
normalisation of these derivations is not.
This is salvaged by internalizing normalization of
derivations into the type system through an appropriate  generalization
of $\indrulename{T-!}$ (\cf~$\indrulename{C-!}$ on
page~\pageref{rule:C-Bang}). Before introducing this new rule, which
is used for typing configurations and relies on trails, we
first present trails.

% \[
%   \mathcal{T}^{B}(\psi) \triangleq
%     \begin{cases}
%       B &(\psi = \dbrch, \rbrch, \betabrch, \betaboxbrch, \tibrch, \trplebrch) \\
%       \lamtyp{B}{B} &(\psi = \lambrch) \\
%       \lamtyp{B}{\lamtyp{B}{B}} &(\psi = \tbrch, \appbrch, \letbrch, \trplcbrch)
%     \end{cases}
% \]

\begin{figure}[t]
  \begin{center}
    $
  \begin{array}{c}
  \reduce{T-TI}
    {
       \phantom{\intenv{\trmtyp{\vartrm{a}}{A}}}
 }{
    \judgTerm{\wenv}{\tenv}{\titrm{A}}{\rec{A}}
    }
    \quad
  \reduce{T-VAR}
  {
    \intenv{\trmtyp{\vartrm{a}}{A}}
  }{
    \judgTerm{\wenv}{\tenv}{\vartrm{a}}{A}
    }
    \\
    \\
  \reduce{T-ABS}
  {
    \judgTerm{\wenv}{\tenv, \trmtyp{\vartrm{a}}{A}}{s}{B}
  }{
    \judgTerm{\wenv}{\tenv}{
      \lamwit{a}{s}}{\lamtyp{A}{B}}
  }
  \qquad
  \reduce{T-APP}
  {
    \judgTerm{\wenv}{\tenv}{s}{\lamtyp{A}{B}}
    \quad
      \judgTerm{\wenv}{\tenv}{t}{A}
  }{
    \judgTerm{\wenv}{\tenv}{\apptrm{s}{t}}{B}
    }
    \\
    \\
  \reduce{T-AVAR}
  {
    \inwenv{\avartrm{u} : A}
  }{
    \judgTerm{\wenv}{\tenv}{\avartrm{u}}{A}
    }
    \quad
  \reduce{T-!}
  {
      \judgTerm{\wenv}{\noenv}{s}{A}
  }{
    \judgTerm{\wenv}{\tenv}{\bangtrm{}{s}}{\bangtyp{s}{A}}
  }
\quad
  \reduce{T-LET}
  {
    \judgTerm{\wenv}{\tenv}{s}{\bangtyp{r}{A}}
    \quad
      \judgTerm{\wenv, u : A}{\tenv}{t}{C}
  }{
    \judgTerm{\wenv}{\tenv}{
      \lettrm{u}{s}{t}}
        {\appsub{C}{r}{u}}
    }
    \end{array}
% \[
%   \reduce{T-TI}
%   {
%     \begin{gathered}
%       d \in \dom{\trmbranches} \\
%       \dom{\trmbranches} = \dom{\witbranches} \\
%       [\withenvs{\wenv}{\noenv}{\trmtypwit{\trmbranches(\psi)}{\mathcal{T}^B(\psi)}{\witbranches(\psi)}}]_{\psi \in \dom{\trmbranches}}
%     \end{gathered}
%   }{
%     \withenvs{\wenv}{\tenv}{\trmtypwit{\titrm{\trmbranches}}{B}{\tiwit{\witbranches}}}
%   }
% \]
$
\end{center}
\caption{Typing Rules for Terms}\label{fig:source_terms_typing}
\end{figure}





%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
