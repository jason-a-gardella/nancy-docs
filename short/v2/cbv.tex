

\section{Call-by-Value}
\label{sec:cbv}

We introduce evaluation (big-step) semantics and reduction (small-step)
semantics and then prove their correspondence.
We begin by introducing (call-by-value) \defin{evaluation judgements} and give meaning to
them via \defin{evaluation rules}. An evaluation judgement is an
expression of the form:
\begin{center}
  $\lstate{M}{\rho}{\ecf} \eval \rstate{V}{\sigma}$
\end{center}
where $\lstate{M}{\rho}{\ecf} $ is the \defin{start tuple} and
$\rstate{V}{\sigma}$ as the \defin{end tuple}.   
 Letters $\ecf$ and $V$ refer to \defin{shallow contexts} and
\defin{values}, resp.:
\begin{center}
  $\begin{array}{rcl}
     \ecf & ::= &
    \hole\, |\,
   \apptrm{\ecf}{M}\, |\,
    \apptrm{(\lamtrm{a}{M})}{\ecf}\,  |\, 
    \lettrm{u}{\ecf}{M} \\
                      %                       \titrm{\tibranches{\tibranch{V}{\psi}}{\ecf/\psi'}{\tibranch{N}{\psi''}}} 

  V & ::= & \lamtrm{a}{M}\ |\ \bangtrm{\rho}{V}
   \end{array}$
 \end{center}
Start tuple $\lstate{M}{\rho}{\ecf}$
states that trail $\rho$ denotes a computation of $\ecf\ctxof{M}$.  Evaluation judgement $\lstate{M}{\rho}{\ecf} \eval \rstate{V}{\sigma}$ states that evaluation
of $\bangtrm{\rho}{\ecf\ctxof{M}}$ produces the configuration
$\bangtrm{\ttrl{\rho}{\decompile{\ecf}\ctxof{\sigma}}}{\ecf\ctxof{V}}$.
Here $\decompile{\ecf}$ is the result of decompiling the shallow
context $\ecf$ (defined just as for configurations and where $\hole$ is
treated as a variable) and 
$\decompile{\ecf}\ctxof{\sigma}$ may be understood as denoting that computation $\sigma$
takes place under $\ecf$; in other words, 
trail 
$\sigma$ is the ``additional work'' that is
performed under $\ecf$, and on $M$,  to produce $V$.






\subsection{Evaluation}


An evaluation judgement $\lstate{M}{\rho}{\ecf} \eval
\rstate{V}{\sigma} $ is \defin{derivable} if there is a derivation $\pi$ of
it, using the evaluation rules of Fig.~\ref{fig:big-step:rules}. In that case we write
$\proves{\pi}{\lstate{M}{\rho}{\ecf} \eval \rstate{V}{\sigma}}$. We
next briefly describe the evaluation rules. Rule
$\indrulename{E-V}$ does no work to produce $V$ from $V$ and hence
reports the identity trail $\decompile{V}$ as computation history. Rule
$\indrulename{E-!}$ allows evaluation  to take place inside an audited
unit. As mentioned in the introduction, notice that a new scope is opened for trail $\sigma$. Also, the
work performed inside the audited unit is recorded in the trail binder (the
annotation $\tau$ in $\bangtrm{\ttrl{\sigma}{\tau}}{V}$). Moreover,
this work is \emph{not}  visible outside its scope, hence the
identity step $\bangtrm{}{\src{\sigma}}$ is produced as overall computation trail.
Rule $\indrulename{E-$\beta$}$ evaluates an application
$\apptrm{M}{N}$. Notice that the trails of each step in the evaluation
process are collected. In particular, the trail
$\ttrl{\rho}{\decompile{\ecf}\ctxof{\ttrl{(\apptrl{\sigma}{\tau})}{\batrl{a}{(\lamtrm{a}{\decompile{P}})\,\decompile{V}}{\tgt{\tau}}}}
}$  expresses that, in producing $\appsub{P}{V}{a}$, a computation
$\rho$ producing $\apptrm{M}{N}$ was followed by a computation
$\apptrl{\sigma}{\tau}$, where $\sigma$ produces $\lamtrm{a}{P}$ under $\ecf$ from
$M$ and $\tau$ produces $ V$ under $\ecf$  from 
$N$, and then a $\beta$ step follows. Note that $\ttrl{\rho}{\decompile{\ecf}\ctxof{\ttrl{(\apptrl{\sigma}{\tau})}{\batrl{a}{(\lamtrm{a}{\decompile{P}})\,\decompile{V}}{\tgt{\tau}}}}
}$ may also have been written
$\ttrl{\rho}{\ttrl{\decompile{\ecf}\ctxof{\apptrl{\sigma}{\tau}}}{\decompile{\ecf}{\ctxof{\batrl{a}{(\lamtrm{a}{\decompile{P}})\,\decompile{V}}{\tgt{\tau}}}}}}$
or
$\ttrl{\rho}{\ttrl{\ttrl{\decompile{\ecf}\ctxof{\apptrl{\sigma}{\decompile{N}}}}{\decompile{\ecf}\ctxof{\apptrl{(\decompile{\lamtrm{a}{P}})}{\tau}}}}{\decompile{\ecf}{\ctxof{\batrl{a}{(\lamtrm{a}{\decompile{P}})\,\decompile{V}}{\tgt{\tau}}}}}}$,
among others; all of which are structurally equivalent.
Note also that in
$\apptrl{\sigma}{\tau}$ we cannot tell whether we first performed
$\sigma$ on $M$ or $\tau$ on $N$. This is a small price to pay for the
benefit of being able to reason modulo structural equivalence, which
we shall rely on heavily for our correspondence proof with reduction. 
Regarding $\indrulename{E-$\betabox$}$, the trail
$\ttrl{\rho}{\decompile{\ecf}\ctxof{\ttrl{\lettrm{u}{\sigma}{\decompile{N}}}{\ttrl{\bbtrl{u}{\lettrm{u}{\decompile{\bangtrm{\tau}{V}}}{\decompile{N}}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}}}$
appends the witness to the actual $\betabox$ step with
$\decompile{N}\sub{\tau}{u}$. The latter persists the trail $\tau$
that produced $V$.
Finally, rule $\indrulename{E-TI}$ models trail inspection, producing the iterator $\rec{\canonical{\rho}}$ as value and
the unary step $\titrl{\rho}{A}$ as computation history. 




\begin{figure}[t]
  \[
    \begin{array}{c}
  \reduce{E-V}
    {\phantom{\lstate{V}{\rho}{\ecf}\eval \rstate{V}{\decompile{V}}}}
    {\lstate{V}{\rho}{\ecf}\eval \rstate{V}{\decompile{V}}}
  \qquad
      \reduce{E-!}
    {\lstate{M}{\sigma}{\hole}\eval \rstate{V}{\tau}}
    {\lstate{\bangtrm{\sigma}{M}}{\rho}{\ecf}\eval \rstate{\bangtrm{\ttrl{\sigma}{\tau}}{V}}{\bangtrm{}{\src{\sigma}}}}

      \\
      \\

      \reduce{E-$\beta$}
  {
    \begin{gathered}
      \lstate{M}{\rho}{\ecf\ctxof{\hole\,N}}\eval\rstate{\lamtrm{a}{P}}{\sigma} \\
      \lstate{N}{\ttrl{\rho}{\decompile{\ecf}\ctxof{\sigma\,\decompile{N}} }}{\ecf\ctxof{(\lamtrm{a}{P})\,\hole}}
      \eval \rstate{V}{\tau} \\
      \lstate{\appsub{P}{V}{a}}{\ttrl{\rho}{\decompile{\ecf}\ctxof{\ttrl{(\apptrl{\sigma}{\tau})}{\batrl{a}{(\lamtrm{a}{\decompile{P}})\,\decompile{V}}{\tgt{\tau}}}} }}{\ecf}\eval
      \rstate{W}{\upsilon}
    \end{gathered}
  }
  {
    \lstate{\apptrm{M}{N}}{\rho}{\ecf}\eval \rstate{W}{\ttrl{(\apptrl{\sigma}{\tau})}{\ttrl{\batrl{a}{(\lamtrm{a}{\decompile{P}})\,\decompile{V}}{\tgt{\tau}}}{\upsilon}}}
  }
      \\
      \\
      
  \reduce{E-$\betabox$}
  {
    \begin{gathered}
      \lstate{M}{\rho}{\ecf\ctxof{\lettrm{u}{\hole}{N}}} \eval \rstate{\bangtrm{\tau}{V}}{\sigma} \\
%      \delta = [(V, \tau, \src{\tau})/u] \\
\lstate{N\sub{\bangtrm{\tau}{V}}{u}}
          {\ttrl{\rho}{\decompile{\ecf}\ctxof{\ttrl{\lettrm{u}{\sigma}{\decompile{N}}}{\ttrl{\bbtrl{u}{\lettrm{u}{\decompile{\bangtrm{\tau}{V}}}{\decompile{N}}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}}}}
          {\ecf}
\eval
\rstate{W}{\upsilon}
    \end{gathered}
  }
  {
    \lstate{\lettrm{u}{M}{N}}{\rho}{\ecf}
    \eval
    \rstate{W}{\ttrl{\lettrm{u}{\sigma}{\decompile{N}}}{\ttrl{\ttrl{\bbtrl{u}{\lettrm{u}{\decompile{\bangtrm{\tau}{V}}}{\decompile{N}}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}{\upsilon}}}
  }

      \\
      \\

      \reduce{E-TI}
  {}
  {
    \lstate{\titrm{A}}{\rho}{\ecf}
    \eval
  \rstate{\rec{\canonical{\rho}}}{\titrl{\rho}{A}}
  }

  %     \\
  %     \\

  %     \reduce{E-EQU}
  % {e_1\simeq e_1'
  %   \quad
  %   \lstate{M}{e_1'}{\ecf}\eval \rstate{V}{e_2'}
  %   \quad
  % e_2'\simeq e_2}
  %     {\lstate{M}{e_1}{\ecf}\eval \rstate{V}{e_2}}
      \end{array}
\]

% \[
%   \reduce{E-TRPL}
%   {
%     \begin{gathered}
%       \bstep{\trmbranches_{r}}{1}{\trmbranches_{r}^{V}}{q_r} \\
%       \bstep{\trmbranches_{t}}{\ttrl{1}{\trpltrl{q_r, \rcd{\trmbranches_{t}}, \ldots}}}{\trmbranches_{t}^{V}}{q_t} \\
%       \ldots \\
%       \bstep{\trmbranches_{trpl}}{\ttrl{1}{\trpltrl{q_r, q_t, \ldots, \rcd{\trmbranches_{trpl}}}}}{\trmbranches_{trpl}^{V}}{q_{trpl}}
%     \end{gathered}
%   }
%   {
%     \bstep{\trmbranches}{1}{\trmbranches^{V}}{\trpltrl{q_r, q_t, \ldots, q_{trpl}}}
%   }
% \]
\caption{CBV Evaluation}\label{fig:big-step:rules}
\end{figure}

Evaluation is compatible with structural equivalence, a result which
depends on trail lookup reporting the iterator of the canonical
representative of the Church encoding of the current trail
(\cf~$\indrulename{E-TI}$). This is stated in the result below where
$\simeq$ on tuples is the pointwise extension  of  structural
equivalence on trails (Def.~\ref{def:structural_equivalence}).
    
\begin{lemma}
\label{lem:evaluation:welldefined:over:simeq}
If $\proves{\pi}{\lstate{M}{\rho}{\ecf} \eval \rstate{V}{\sigma}}$ and
$\lstate{M}{\rho}{\ecf} \simeq \lstate{M'}{\rho'}{\ecf'}$, then there
exists $\rstate{V'}{\sigma'}$ and $\pi'$ such that $\proves{\pi'}{\lstate{M'}{\rho'}{\ecf'}
\eval \rstate{V'}{\sigma'}}$ and $\rstate{V}{\sigma}\simeq \rstate{V'}{\sigma'}$.
\end{lemma}


A start tuple $\lstate{M}{\rho}{\ecf}  $
 is \defin{well-formed} if 
$\bangtrm{\rho}{\ecf\ctxof{M}}$ is a well-formed
configuration. Recall that a configuration $M$ is \defin{well-formed} if every sub-expression of
$M$ of the form $\bangtrm{\rho}{N}$ is consistent; and an expression of the form $\bangtrm{\rho}{N}$ is said to be \defin{consistent} if
$\tgt{\rho}=\decompile{N}$.
We conclude this
subsection with a simple result on preservation of well-formedness
which will be required for our correspondence proof with reduction semantics.


\begin{lemma}
  \label{lem:big-step_preserves_well_formedness}
  $\lstate{M}{\rho}{\ecf}\eval \rstate{V}{\sigma}$ and
  $\lstate{M}{\rho}{\ecf}$ well-formed implies
  \begin{enumerate}
  \item $\rho$ is composable with $\decompile{\ecf}\ctxof{\sigma}$ (\ie $\src{\decompile{\ecf}\ctxof{\sigma}}=\tgt{\rho}$); and
    
  \item $\bangtrm{\ttrl{\rho}{\decompile{\ecf}\ctxof{\sigma}}}{\ecf\ctxof{V}}$ is a well-formed configuration.
\end{enumerate}

  \end{lemma}


\subsection{Reduction}

This section introduces the one-step CBV reduction relation
$\ssteprel$. Reduction always takes place inside some current audited
unit as modeled by the following  set of reduction contexts:
\begin{center}
$\begin{array}{rcl}
  \ec ::=
    \hole \,|\,
    \bangtrm{\rho}{\ecf\ctxof{\ec}}
%    \titrm{\tibranches{\tibranch{V}{\psi}}{\ecf/\psi'}{\tibranch{N}{\psi''}}}
  \end{array}
$
\end{center}
Recall from the beginning of this section that $\ecf$
denotes a shallow context. Reduction is specified by the following $\indrulename{R-Ctxt}$ rule,
where the relation $\sstepaxrel$ is defined in Fig.~\ref{fig:reduction:small-step}:
\begin{center}
  $\reduce{R-Ctxt}
  {M \sstepaxrel N}
  {\ec\ctxof{M} \ssteprel \ec{\ctxof{N}}}
  $
\end{center}



% \subsection{Trail Evaluation Context}

% \begin{align}
%   \ecq &::=
%     \hole |
%     \ttrllong{\ecq}{q} |
%     \ttrllong{q}{\ecq} |
%     \apptrl{\ecq}{q} |
%     \apptrl{q}{\ecq} |
%     \lamtrl{a}{\ecq} \\ &|
%     \lettrl{\ecq}{u}{q} |
%     \lettrl{q}{u}{\ecq} |
%     \trpltrl{\tibranches{\tibranch{q_1}{\psi}}{\ecq/\psi'}{\tibranch{q_2}{\psi''}}}
% \end{align}




\begin{figure}[t]
  \[
    \begin{array}{c}
  \reduce{R-$\beta$}
  { R = \apptrm{(\lamtrm{a}{M})}{V}}
  {
    {
      \bangtrm{\rho}{\inecf{R}}
    }\sstepaxrel
    {
      \bangtrm{\ttrl{\rho}{\inecq{\batrl{a}{\decompile{R}}{\cd{V}}}}}{\inecf{M\sub{a}{V}}}
    }
  }
     \qquad
  \reduce{R-$\betabox$}
  {
    R=\lettrm{u}{\bangtrm{\sigma}{V}}{N} 
  }
  {
    {
      \bangtrm{\rho}{\inecf{R}}
    }\sstepaxrel
    {
      \bangtrm{\ttrl{\rho}{\inecq{\ttrl{\bbtrl{u}{\decompile{R}}{\cd{N}}}{\decompile{N}\sub{\sigma}{u}}}}}
                    {\inecf{N\sub{\bangtrm{\sigma}{V}}{u}}}
    }
  }
  \\
\\
  \reduce {R-TI}
  {R = \titrm{A}}
  {
    {
      \bangtrm{\rho}{\inecf{R}}
    }\sstepaxrel
    {
      \bangtrm{\ttrl{\rho}{\inecq{\titrl{\rho}{A}}}}{\inecf{\rec{\canonical{\rho}}}}
    }
  }
    \end{array}
    \]
% \[
% \reduce{}
% {M\simeq \ec\ctxof{M'}
% \quad
% M'\sstepaxrel N' 
% \quad
% \ec\ctxof{N'}\simeq N}
% {M\ssteprelmod N}
% \]

    \caption{CBV Reduction Semantics}\label{fig:reduction:small-step}
\end{figure}



%We write $\ssteprelmodc{\ec}$ when we wish to single out the
%evaluation context under which the step takes place. 
 
%\begin{definition}
%$M\ssteprelmod N$ iff $M\simeq M'\sstepaxrel N'\simeq N$.
%\end{definition}

% \begin{lemma}
% \label{lem:congruence:small-step}
% $M\ssteprelmod N$ implies $\bangtrm{\rho}{\ecf\ctxof{M}}\ssteprelmod \bangtrm{\rho}{\ecf\ctxof{N}}$.
% \end{lemma}

% \begin{proof}
%   Immediate from the definition of $\ssteprelmod$.
%   \end{proof}


We next address the correspondence with CBV evaluation. We first present the  reduction-simulates-evaluation
direction (Prop.~\ref{prop:cbv:reduction:simulates:evaluation}) and  then we present the more
involved  evaluation-simulates-reduction case (Prop.~\ref{prop:cbv:evaluation:simulates:reduction}). 
Crucial to
these results is being able to reason modulo structural
equivalence. For example, the proof of Prop.~\ref{prop:cbv:evaluation:simulates:reduction} applies the \ih on subderivations of
$\pi$. Each of these produce trails generated during reduction that have to be composed modulo
$\simeq$ in order to obtain the trail produced during evaluation. We
now state Prop.~\ref{prop:cbv:reduction:simulates:evaluation}, where
$\ssteprelrt $ is the reflexive-transitive closure of $\ssteprel$:

\begin{proposition}[Reduction simulates evaluation]
  \label{prop:cbv:reduction:simulates:evaluation}
  Let $\lstate{M}{\rho}{\ecf}$ be well-formed.
  \[
    \proves{\pi}{\lstate{M}{\rho}{\ecf}\eval
 \rstate{V}{\sigma}} \mbox{ implies }
\bangtrm{\rho}{\ecf\ctxof{M}} \ssteprelrt
\bangtrm{\ttrl{\rho}{\ecfd\ctxof{\sigma}}}{\ecf\ctxof{V}}
\]
\end{proposition}



% \begin{lemma}[Postponement]

%   $M\simeq M'\sstepaxrel N$ implies there exists $N'$ such that
%   $M\sstepaxrel N'\simeq N$.

% \end{lemma}

% \begin{proof}

% \end{proof}
% \begin{lemma}
% \label{lem:merge_small_big:i}
% If $\bangtrm{\rho}{\ecftwo\ctxof{R}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\ecftwod\ctxof{r}}}{\ecftwo\ctxof{R'}}$ and $\lstate{R'}{\ttrl{\rho}{\ecftwod\ctxof{ r}}}{\ecftwo}\eval
%  \rstate{W}{\sigma}$, then $\lstate{R}{\rho}{\ecftwo}\eval
%  \rstate{W}{\ttrl{r}{\sigma}}$.
% \end{lemma}

% \begin{proof}
%   We consider each of the possible three cases for $R$:

%   \begin{itemize}

%   \item $R=\apptrm{(\lamtrm{a}{M})}{V}$, $R'= M[V/a]$,
%     $r=\batrl{a}{\rcd{R}}{\cd{V}}$. Then we reason as follows:

% \[   
% \reduce{$E-\beta$}
%   {
%     \begin{gathered}
%       \lstate{\lamtrm{a}{M}}{\rho}{\ecftwo\ctxof{\hole\,V}}\eval\rstate{\lamtrm{a}{M}}{\decompile{(\lamtrm{a}{M})}} \\
%       \lstate{V}{\ttrl{\rho}{\ecftwod\ctxof{\decompile{R}} }}{\ecftwo\ctxof{(\lamtrm{a}{M})\,\hole}}
%       \eval \rstate{V}{\decompile{V}} \\
%       \lstate{\appsub{M}{V}{a}}{\ttrl{\rho}{\ecftwod\ctxof{\ttrl{\decompile{R}}{\batrl{a}{\decompile{R}}{\tgt{\tau}}}}}}{\ecftwo}
%       \eval
%       \rstate{W}{\sigma}
%     \end{gathered}
%   }
%   {
%     \lstate{\apptrm{(\lamtrm{a}{M})}{V}}{\rho}{\ecftwo}
%     \eval
%     \rstate{W}{\ttrl{\decompile{R}}{\ttrl{\batrl{a}{\decompile{R}}{\tgt{\tau}}}{\sigma}}}
%   }
% \]


% \item $R= \lettrm{u}{\bangtrm{\tau}{V}}{N}$,
%   $R'=N\sub{\bangtrm{\tau}{V}}{u}$ and
%   $r=\ttrl{\bbtrl{u}{\rcd{R}}{\cd{N}}}{N\sub{\tau}{u}}$. Then we
%   reason as follows:
% \[
%   \reduce{$E-\betabox$}
%   {
%     \begin{gathered}
%       \lstate{\bangtrm{\tau}{V}}{\rho}{\ecftwo\ctxof{\lettrm{u}{\hole}{N}}} \eval \rstate{\bangtrm{\tau}{V}}{\decompile{(\bangtrm{\tau}{V})}} \\
% %      \delta = [(V, \tau, \src{\tau})/u] \\
% \lstate{N\sub{\bangtrm{\tau}{V}}{u}}
%           {\ttrl{\rho}{\ecftwod\ctxof{\ttrl{\decompile{R}}{\ttrl{\bbtrl{u}{\decompile{R}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}}}}
%           {\ecftwo}
% \eval
% \rstate{W}{\sigma}
%     \end{gathered}
%   }
%   {
%     \lstate{\lettrm{u}{\bangtrm{\tau}{V}}{N}}{\rho}{\ecftwo}
%     \eval
%     \rstate{W}{\ttrl{\decompile{R}}{\ttrl{\ttrl{\bbtrl{u}{\decompile{R}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}{\sigma}}}
%   }
% \]
%     The judgement just above the line follows from the hypothesis $\lstate{R'}{\ttrl{\rho}{\ecftwod\ctxof{ r}}}{\ecftwo}\eval
%  \rstate{W}{\sigma}$.

% \item $R=\titrm{A}$, $R'=\rec{\rho}$ and
%   $r=\titrl{\rho}{A}$. Note that
%   $\sigma=\decompile{R'}$ since $R'$ is a value. We reason as follows:
% \[
%       \reduce{E-TI}
%   {}
%   {
%     \lstate{\titrm{A}}{\rho}{\ecftwo}
%     \eval
%   \rstate{\rec{\rho}}{\titrl{\rho}{A}}
%   }
% \]
 
% Note that since $\rec{\rho}$ is a value, the hypothesis $\lstate{R'}{\ttrl{\rho}{\ecftwod\ctxof{ r}}}{\ecftwo}\eval
%  \rstate{W}{\sigma}$ reads $    \lstate{\rec{\rho}}{\ttrl{\rho}{\ecftwod\ctxof{\titrl{\rho}{A}}}}{\ecftwo}
%     \eval
%   \rstate{\rec{\rho}}{\decompile{\rec{\rho}}}$. Thus,
%   $\sigma=\decompile{\rec{\rho}}$ and $r;\sigma =
%   \ttrl{\titrl{\rho}{A}}{\decompile{\rec{\rho}}} = \titrl{\rho}{A}$.

%   \end{itemize}
  

% \end{proof}

The proof of the evaluation-simulates-reduction result is more
subtle. First we introduce some auxiliary results. In stating and
proving them, we will make use of the following \defn{redex table}:
 \begin{center}
    $\begin{array}{l|l|l}
       \multicolumn{1}{c|}{R} & \multicolumn{1}{c|}{R'} &
                                                        \multicolumn{1}{c}{r}\\ \hline
        \apptrm{(\lamtrm{a}{M})}{V} &  M\sub{V}{a} &
\batrl{a}{\decompile{R}}{\cd{V}} \\
\lettrm{u}{\bangtrm{\tau}{V}}{N} & N\sub{\bangtrm{\tau}{V}}{u} & \ttrl{\bbtrl{u}{\decompile{R}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}} \\
       \titrm{A} & \rec{\canonical{\rho}} & \titrl{\rho}{A} 
     \end{array}$
     \end{center}

The main result on which
Prop.~\ref{prop:cbv:evaluation:simulates:reduction} rests is the
Expansion Lemma (Lem.~\ref{lem:big-step:expansion:general})
which essentially winds time backword. In other words, if $\lstate{\ec\ctxof{\bangtrm{\ttrl{\rho}{\ecfd\ctxof{r}}}{\ecf\ctxof{R'}}}}{\sigma}{\ecftwo}\eval
 \rstate{W}{\tau}$, then $\lstate{\ec\ctxof{\bangtrm{\rho}{\ecf\ctxof{R}}}}{\sigma}{\ecftwo}\eval
 \rstate{W}{\tau}$. Note how the witness to the last step (but the not the step
 itself!) performed
 under $\ecf$ in the former judgement, and which is the starting point
 of the evaluation, is included in the evaluation in the latter judgement. Thus,
 in a way, we are running the computation history
 $\ttrl{\rho}{\ecfd\ctxof{r}}$ backwards. This lemma is first stated in a restricted form
 (in which computation takes place inside a shallow context and hence
 referred to as the Shallow Expansion Lemma below), and then
 is extended to its general form with the help of the Projection Lemma
 and the Substitution Lemma. 

% \begin{lemma}
% If $\bangtrm{\rho}{\ecftwo\ctxof{\ecf\ctxof{R}}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\ecftwod\ctxof{\ecfd\ctxof{r}}}}{\ecftwo\ctxof{\ecf\ctxof{R'}}}$ and $\lstate{\ecf\ctxof{R'}}{\ttrl{\rho}{\ecftwod\ctxof{\ecfd\ctxof{r}}}}{\ecftwo}\eval
%  \rstate{W}{\sigma}$, then $\lstate{\ecf\ctxof{R}}{\rho}{\ecftwo}\eval
%  \rstate{W}{\ttrl{\ecfd\ctxof{r}}{\sigma}}$.
% \end{lemma}

% \begin{proof}
%   By induction on $\ecf$.

%   \begin{itemize}
%   \item $\ecf=\hole$. We must prove that if $\bangtrm{\rho}{\ecftwo\ctxof{R}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\ecftwod\ctxof{r}}}{\ecftwo\ctxof{R'}}$ and $\lstate{R'}{\ttrl{\rho}{\ecftwod\ctxof{r}}}{\ecftwo}\eval
%  \rstate{W}{\sigma}$, then $\lstate{R}{\rho}{\ecftwo}\eval
%  \rstate{W}{\ttrl{r}{\sigma}}$. This has been proved as Lem.~\ref{lem:merge_small_big:i}.

%   \item     $\ecf = \apptrm{\ecf'}{M}$. We have to prove  $\lstate{\apptrm{\ecf'\ctxof{R}}{M}}{\rho}{\ecftwo}\eval
%  \rstate{W}{\ttrl{(\apptrm{\decompile{\ecf'}\ctxof{r}}{\decompile{M}})}{\sigma}}$. From
%  the hypothesis $\lstate{\ecf\ctxof{R'}}{\ttrl{\rho}{\ecftwod\ctxof{\ecfd\ctxof{r}}}}{\ecftwo}\eval
%  \rstate{W}{\sigma}$ we have the following, where $\sigma=\ttrl{\apptrl{\rho}{\tau}}{\ttrl{\batrl{a}{\decompile{R}}{\tgt{\tau}}}{\sigma'}}$:

% \begin{equation}
% \reduce{$E-\beta$}
%   {
%     \begin{gathered}
%       \lstate{\ecf'\ctxof{R'}}{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{\decompile{\ecf'}\ctxof{r}}{M}}}}{\ecftwo\ctxof{\hole\,M}}\eval\rstate{\lamtrm{a}{P}}{\tau} \\
%       \lstate{M}{\ttrl{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{\decompile{\ecf'}\ctxof{r}}{M}}}}{\ecftwod\ctxof{\apptrm{\tau}{\decompile{M}}} }}{\ecftwo\ctxof{(\lamtrm{a}{P})\,\hole}}
%       \eval \rstate{V}{\mu} \\
%       \lstate{\appsub{P}{V}{a}}{\ttrl{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{\decompile{\ecf'}\ctxof{r}}{M}}}}{\ecftwod\ctxof{\ttrl{\apptrm{\tau}{\mu}}{\batrl{a}{\decompile{R}}{\tgt{\tau}}}}}}{\ecftwo}
%       \eval
%       \rstate{W}{\nu}
%     \end{gathered}
%   }
%   {
%     \lstate{\apptrm{\ecf'\ctxof{R'}}{M}}{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{\decompile{\ecf'}\ctxof{r}}{M}}}}{\ecftwo}
%     \eval
%     \rstate{W}{\ttrl{\apptrl{\rho}{\tau}}{\ttrl{\batrl{a}{\decompile{R}}{\tgt{\tau}}}{\nu}}}
%   }
%   \label{eq:app:left:i}
% \end{equation}

% From the hypothesis 
% \[
%   \bangtrm{\rho}{\ecftwo\ctxof{\apptrm{\ecf'\ctxof{R}}{M}}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\ecftwod\ctxof{\apptrm{\decompile{\ecf'}\ctxof{r}}{M}}}}{\ecftwo\ctxof{\apptrm{\ecf'\ctxof{R'}}{M}}}
% \]
% if we set $\ecftwo' \eqdef \ecftwo\ctxof{\hole\,M}$, then we may
% rewrite this judgement as:
% \begin{equation}
%   \bangtrm{\rho}{\ecftwo'\ctxof{\ecf'\ctxof{R}}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\decompile{\ecftwo'}\ctxof{\decompile{\ecf'}\ctxof{r}}}}{\ecftwo'\ctxof{\ecf'\ctxof{R'}}}
%   \label{eq:app:left:ii}
% \end{equation}
% Also, we can rewrite the topmost judgement in (\ref{eq:app:left:i}) as:
% \begin{equation}
%   \lstate{\ecf'\ctxof{R'}}{\ttrl{\rho}{\decompile{\ecftwo'}\ctxof{\decompile{\ecf'}\ctxof{r}}}}{\ecftwo'}\eval\rstate{\lamtrm{a}{P}}{\tau}
%   \label{eq:app:left:iii}
% \end{equation}
% From the \ih on (\ref{eq:app:left:ii}) and (\ref{eq:app:left:iii}), we deduce:
% \[
% \lstate{\ecf'\ctxof{R}}{\rho}{\ecftwo'}\eval\rstate{\lamtrm{a}{P}}{\ttrl{(\apptrl{\decompile{\ecf'}\ctxof{r}}{M})}{\tau}} \\
%   \]
% We now prove the judgement $\lstate{\apptrm{\ecf'\ctxof{R}}{M}}{\rho}{\ecftwo}\eval
% \rstate{W}{\ttrl{(\apptrm{\decompile{\ecf'}\ctxof{r}}{\decompile{M}})}{\sigma}}$.

% \[
%   \reduce{$E-\beta$}
%   {
%     \begin{gathered}
%  \lstate{\ecf'\ctxof{R}}{\rho}{\ecftwo\ctxof{\hole\,M}}\eval\rstate{\lamtrm{a}{P}}{\ttrl{\decompile{\ecf'}\ctxof{r}}{\tau}} \\
%            \lstate{M}{\ttrl{\rho}{\ecftwod\ctxof{\ttrl{(\apptrl{\decompile{\ecf'}\ctxof{r}}}{\tau})\,M}}}{\ecftwo\ctxof{(\lamtrm{a}{P})\,\hole}}
%       \eval \rstate{V}{\mu} \\
%       \lstate{\appsub{P}{V}{a}}{\ttrl{\rho}{\ecftwod\ctxof{\ttrl{\apptrl{(\ttrl{\decompile{\ecf'}\ctxof{r}}{\tau})}{\mu}}{\batrl{a}{\decompile{S}}{\tgt{\tau}}}}}}{\ecftwo}
%       \eval
%       \rstate{W}{\nu}
%     \end{gathered}
%   }
%   {
%     \lstate{\apptrm{\ecf'\ctxof{R}}{M}}{\rho}{\ecftwo}
%     \eval
%     \rstate{W}{\ttrl{\apptrl{(\ttrl{\decompile{\ecf'}\ctxof{r}}{\tau})}{\mu}}{\ttrl{\batrl{a}{\decompile{S}}{\tgt{\tau}}}{\nu}}}
%   }
% \]
% where $S \eqdef (\lamtrm{a}{P})\,V$.

% \item     $\ecf=\apptrm{V}{\ecf'}$. Then $V=\lamtrm{a}{P}$ for some $P$.
%   \begin{equation}
% \reduce{$E-\beta$}
%   {
%     \begin{gathered}
%       \lstate{V}{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{V}{\decompile{\ecf'}\ctxof{r}}}}}{\ecftwo\ctxof{\hole\, \ecf'\ctxof{R'}}}
%       \eval
%       \rstate{V}{\decompile{V}} \\
%       \lstate{\ecf'\ctxof{R'}}{\ttrl{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{V}{\decompile{\ecf'}\ctxof{r}}}}}{\ecftwod\ctxof{\apptrm{\decompile{V}}{\decompile{\ecf'\ctxof{R'}}}} }}{\ecftwo\ctxof{V\,\hole}}
%       \eval \rstate{U}{\mu} \\
%       \lstate{\appsub{P}{U}{a}}{\ttrl{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{V}{\decompile{\ecf'}\ctxof{r}}}}}{\ecftwod\ctxof{\ttrl{\apptrm{\decompile{V}}{\mu}}{\batrl{a}{\decompile{S}}{\tgt{\tau}}}}}}{\ecftwo}
%       \eval
%       \rstate{W}{\nu}
%     \end{gathered}
%   }
%   {
%     \lstate{\apptrm{V}{\ecf'\ctxof{R'}}}{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{V}{\decompile{\ecf'}\ctxof{r}}}}}{\ecftwo}
%     \eval
%     \rstate{W}{\ttrl{\apptrl{\decompile{V}{\mu}}}{\ttrl{\batrl{a}{\decompile{S}}{\tgt{\tau}}}{\nu}}}
%   }
%   \label{eq:app:right:i}
% \end{equation}
% where $S=(\lamtrm{a}{P})\,U$.


% From the hypothesis 
% \[
%   \bangtrm{\rho}{\ecftwo\ctxof{\apptrm{V}{\ecf'\ctxof{R}}}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\ecftwod\ctxof{\apptrm{V}{\decompile{\ecf'}\ctxof{r}}}}}{\ecftwo\ctxof{\apptrm{V}{\ecf'\ctxof{R'}}}}
% \]
% if we set $\ecftwo' \eqdef \ecftwo\ctxof{V\,\hole}$, then we may
% rewrite this judgement as:
% \begin{equation}
%   \bangtrm{\rho}{\ecftwo'\ctxof{\ecf'\ctxof{R}}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\decompile{\ecftwo'}\ctxof{\decompile{\ecf'}\ctxof{r}}}}{\ecftwo'\ctxof{\ecf'\ctxof{R'}}}
%   \label{eq:app:right:ii}
% \end{equation}
% Also, given this definition of $\ecftwo'$ and that
% $\ttrl{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{V}{\decompile{\ecf'}\ctxof{r}}}}}{\ecftwod\ctxof{\apptrm{\decompile{V}}{\decompile{\ecf'\ctxof{R'}}}}
% } \simeq
% \ttrl{\rho}{\ecftwod\ctxof{\apptrl{V}{\decompile{\ecf'}\ctxof{r}}}}$, we can rewrite the second topmost judgement in (\ref{eq:app:right:i}) as:
% \begin{equation}
%   \lstate{\ecf'\ctxof{R'}}{\ttrl{\rho}{\decompile{\ecftwo'}\ctxof{\decompile{\ecf'}\ctxof{r}}}}{\ecftwo'}
%   \eval
%   \rstate{U}{\mu}
%   \label{eq:app:right:iii}
% \end{equation}
% From the \ih on (\ref{eq:app:right:ii}) and (\ref{eq:app:right:iii}), we deduce:
% \[
% \lstate{\ecf'\ctxof{R}}{\rho}{\ecftwo'}\eval\rstate{\lamtrm{a}{P}}{\ttrl{\decompile{\ecf'}\ctxof{r}}{\tau}} \\
% \]
% We now prove the judgement
% $\lstate{\apptrm{V}{\ecf'\ctxof{R}}}{\rho}{\ecftwo}\eval
% \rstate{W}{\ttrl{(\apptrm{\decompile{V}}{\decompile{\ecf'}\ctxof{r}})}{\sigma}}$.

% \[
% \reduce{$E-\beta$}
%   {
%     \begin{gathered}
%       \lstate{V}{\rho}{\ecftwo\ctxof{\hole\, \ecf'\ctxof{R}}}
%       \eval
%       \rstate{V}{\decompile{V}} \\
%       \lstate{\ecf'\ctxof{R}}{\ttrl{\rho}{\ecftwod\ctxof{\apptrm{\decompile{V}}{\decompile{\ecf'\ctxof{R}}}} }}{\ecftwo\ctxof{V\,\hole}}
%       \eval \rstate{U}{\ttrl{\decompile{\ecf'}\ctxof{r}}{\tau}} \\
%       \lstate{\appsub{P}{U}{a}}{\ttrl{\ttrl{\rho}{\ecftwod\ctxof{\apptrl{V}{\decompile{\ecf'}\ctxof{r}}}}}{\ecftwod\ctxof{\ttrl{\apptrm{\decompile{V}}{\mu}}{\batrl{a}{\decompile{S}}{\tgt{\tau}}}}}}{\ecftwo}
%       \eval
%       \rstate{W}{\nu}
%     \end{gathered}
%   }
%   {
%     \lstate{\apptrm{V}{\ecf'\ctxof{R}}}{\rho}{\ecftwo}
%     \eval
%     \rstate{W}{\ttrl{\ttrl{(\apptrl{\decompile{V}}{\decompile{\ecf'}\ctxof{r}})}{\decompile{V}\,\mu}}{\ttrl{\batrl{a}{\decompile{S}}{\tgt{\tau}}}{\nu}}}
%   }
% \]
% where $S \eqdef (\lamtrm{a}{P})\,V$.


% \item     $\ecf=\lettrm{u}{\ecf'}{M}$. We have to prove  $\lstate{\lettrm{u}{\ecf'\ctxof{R}}{M}}{\rho}{\ecftwo}\eval
%  \rstate{W}{\ttrl{(\lettrm{u}{\ecf'\ctxof{r}}{\decompile{M}})}{\sigma}}$. From
%  the hypothesis $\lstate{\ecf\ctxof{R'}}{\ttrl{\rho}{\ecftwod\ctxof{\ecfd\ctxof{r}}}}{\ecftwo}\eval
%  \rstate{W}{\sigma}$ we have the following, where
%  $\sigma=\ttrl{\lettrm{u}{\mu}{N}}{\ttrl{\ttrl{\bbtrl{u}{\decompile{S}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}{\nu}}$:
 
%  \begin{equation}
%   \reduce{$E-\betabox$}
%   {
%     \begin{gathered}
%       \lstate{\ecf'\ctxof{R'}}{\ttrl{\rho}{\ecftwod\ctxof{\ecfd\ctxof{r}}}}{\ecftwo\ctxof{\lettrm{u}{\hole}{N}}} \eval \rstate{\bangtrm{\tau}{V}}{\mu} \\
% %      \delta = [(V, \tau, \src{\tau})/u] \\
% \lstate{N\sub{\bangtrm{\tau}{V}}{u}}
%           {\ttrl{\rho}{\ecftwod\ctxof{\ttrl{\ttrl{\ecfd\ctxof{r}}{\lettrm{u}{\mu}{N}}}{\ttrl{\bbtrl{u}{\decompile{S}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}}}}
%           {\ecftwo}
% \eval
% \rstate{W}{\nu}
%     \end{gathered}
%   }
%   {
%     \lstate{\lettrm{u}{\ecf'\ctxof{R'}}{N}}{\ttrl{\rho}{\ecftwod\ctxof{\ecfd\ctxof{r}}}}{\ecftwo}
%     \eval
%     \rstate{W}{\ttrl{\lettrm{u}{\mu}{N}}{\ttrl{\ttrl{\bbtrl{u}{\decompile{S}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}{\nu}}}
%   }
%   \label{eq:let:i}
% \end{equation}
% where $S\eqdef \lettrm{u}{\bangtrm{\tau}{V}}{N}$

% From the hypothesis 
% \[
%   \bangtrm{\rho}{\ecftwo\ctxof{\lettrm{u}{\ecf'\ctxof{R}}{N}}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\ecftwod\ctxof{\lettrm{u}{\ecf'\ctxof{r}}{N}}}}{\ecftwo\ctxof{\lettrm{u}{\ecf'\ctxof{R'}}{N}}}
% \]
% if we set $\ecftwo' \eqdef \lettrm{u}{\hole}{N}$, then we may
% rewrite this judgement as:
% \begin{equation}
%   \bangtrm{\rho}{\ecftwo'\ctxof{\ecf'\ctxof{R}}} \sstepaxrel
%   \bangtrm{\ttrl{\rho}{\decompile{\ecftwo'}\ctxof{\decompile{\ecf'}\ctxof{r}}}}{\ecftwo'\ctxof{\ecf'\ctxof{R'}}}
%   \label{eq:let:ii}
% \end{equation}
% Also, given this definition of $\ecftwo'$, we can rewrite the second topmost judgement in (\ref{eq:let:i}) as:
% \begin{equation}
%   \lstate{\ecf'\ctxof{R'}}{\ttrl{\rho}{\decompile{\ecftwo'}\ctxof{\decompile{\ecf'}\ctxof{r}}}}{\ecftwo'}
%   \eval
%   \rstate{\bangtrm{\tau}{V}}{\mu}
%   \label{eq:let:iii}
% \end{equation}
% From the \ih on (\ref{eq:app:right:ii}) and (\ref{eq:app:right:iii}), we deduce:
% \[
%   \lstate{\ecf'\ctxof{R}}{\rho}{\ecftwo'}
%   \eval
%   \rstate{\bangtrm{\tau}{V}}{\ttrl{\decompile{\ecf'}\ctxof{r}}{\mu}} \\
% \]
% We now prove the judgement
% $\lstate{\lettrm{u}{\ecf'\ctxof{R}}{N}}{\rho}{\ecftwo}\eval
% \rstate{W}{\ttrl{\lettrm{u}{\decompile{\ecf'}\ctxof{r}}{\decompile{N}}}{\sigma}}$.
% \[
%     \reduce{$E-\betabox$}
%   {
%     \begin{gathered}
%      \lstate{\ecf'\ctxof{R}}{\rho}{\ecftwod\ctxof{\lettrm{u}{\hole}{N}}}
%   \eval
%   \rstate{\bangtrm{\tau}{V}}{\ttrl{\decompile{\ecf'}\ctxof{r}}{\mu}} \\
% %      \delta = [(V, \tau, \src{\tau})/u] \\
% \lstate{N\sub{\bangtrm{\tau}{V}}{u}}
%           {\ttrl{\rho}{\ecftwod\ctxof{\ttrl{\lettrm{u}{(\ttrl{\ecfd\ctxof{r}}{\mu})}{N}}{\ttrl{\bbtrl{u}{\decompile{S}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}}}}
%           {\ecftwo}
% \eval
% \rstate{W}{\nu}
%     \end{gathered}
%   }
%   {
%     \lstate{\lettrm{u}{\ecf'\ctxof{R}}{N}}{\rho}{\ecftwo}
%     \eval
%     \rstate{W}{\ttrl{\lettrm{u}{(\ttrl{\ecfd\ctxof{r}}{\mu})}{N}}{\ttrl{\ttrl{\bbtrl{u}{\decompile{S}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}{\nu}}}
%   }
%   \]
%   Note that
%   \[\begin{array}{rl}
%      &
%        \ttrl{\lettrm{u}{(\ttrl{\ecfd\ctxof{r}}{\mu})}{N}}{\ttrl{\ttrl{\bbtrl{u}{\decompile{S}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}{\nu}}\\
%      \simeq &
%               \ttrl{\lettrm{u}{\decompile{\ecf'}\ctxof{r}}{\decompile{N}}}{\ttrl{\lettrm{u}{\mu}{N}}{\ttrl{\ttrl{\bbtrl{u}{\decompile{S}}{\cd{N}}}{\decompile{N}\sub{\tau}{u}}}{\nu}}}
%       \\
%       = & \ttrl{\lettrm{u}{\decompile{\ecf'}\ctxof{r}}{\decompile{N}}}{\sigma}
%               \end{array}\]
  
%   \end{itemize}

% \end{proof}


\begin{lemma}[Shallow Expansion]
  \label{lem:big-step:expansion}
  For all $(R,R',r)$ in the redex table,   
$\lstate{\ecf\ctxof{R'}}{\ttrl{\rho}{\ecftwod\ctxof{\ecfd\ctxof{r}}}}{\ecftwo}\eval
 \rstate{W}{\sigma}$, implies $\lstate{\ecf\ctxof{R}}{\rho}{\ecftwo}\eval
 \rstate{W}{\ttrl{\ecfd\ctxof{r}}{\sigma}}$.
\end{lemma}

The Projection Lemma uses the notion of position of the hole
in a context: $\poshole{\hole} \eqdef \epsilon$,
$\poshole{\apptrm{\ecf}{M}}\eqdef 1.\poshole{\ecf}$,
$\poshole{\apptrm{(\lamtrm{a}{M})}{\ecf}}\eqdef 2.\poshole{\ecf}$, and
$\poshole{\lettrm{u}{\ecf}{M}}\eqdef 1\cdot\poshole{\ecf}$.
\begin{lemma}[Projection]
\label{lem:projection}
  If $\proves{\pi}{\lstate{\ecf\ctxof{M}}{\rho}{\ecftwo}\eval
    \rstate{U}{\sigma}}$, then there exists $V$ and $\tau$ s.t.
$\atPosProves{\pi}{p}{\lstate{M}{\rho}{\ecftwo\ctxof{\ecf}}\eval
 \rstate{V}{\tau}}$, where $p=\poshole{\ecf}$.


\end{lemma}

In the following corollary, $\atPosProves{\pi}{1.p}{\lstate{M}{\tau}{\ecftwo\ctxof{\ecf}}\eval
 \rstate{V}{\mu}}$ denotes the subderivation of $\pi$ at position $1.p$. 
\begin{corollary}
  If $\proves{\pi}{\lstate{\bangtrm{\tau}{\ecf\ctxof{M}}}{\rho}{\ecftwo}\eval
    \rstate{U}{\sigma}}$, then there exists $V$ and $\mu$ s.t.
$\atPosProves{\pi}{1.p}{\lstate{M}{\tau}{\ecftwo\ctxof{\ecf}}\eval
 \rstate{V}{\mu}}$, where $p$ is the position of the hole in $\ecf$.


\end{corollary}


\begin{lemma}[Substitution]
 \label{lem:substitution}
  If
  \begin{itemize}
  \item $\proves{\pi}{\lstate{\ecf\ctxof{M}}{\rho}{\ecftwo}\eval
      \rstate{U}{\sigma}}$;
  \item     $\atPosProves{\pi}{p}{\lstate{M}{\rho}{\ecftwo\ctxof{\ecf}}\eval
      \rstate{V}{\tau}}$ for $p=\poshole{\ecf}$
    and for some $V$ and $\tau$; and
  
  \item   $\proves{\pi_N}{\lstate{N}{\rho}{\ecftwo\ctxof{\ecf}}\eval
      \rstate{V}{\tau}}$;
  \end{itemize}
  then $\proves{\replaceAt{\pi}{p}{\pi_N}}{\lstate{\ecf\ctxof{N}}{\rho}{\ecftwo}\eval
    \rstate{U}{\sigma}}$.

\end{lemma}

    \begin{lemma}[Expansion]
    \label{lem:big-step:expansion:general}
  For all $(R,R',r)$ in the redex table, if $\lstate{\ec\ctxof{\bangtrm{\ttrl{\rho}{\ecfd\ctxof{r}}}{\ecf\ctxof{R'}}}}{\sigma}{\ecftwo}\eval
 \rstate{W}{\tau}$, then $\lstate{\ec\ctxof{\bangtrm{\rho}{\ecf\ctxof{R}}}}{\sigma}{\ecftwo}\eval
 \rstate{W}{\tau}$.
\end{lemma}

The proof of Prop.~\ref{prop:cbv:evaluation:simulates:reduction} is by
induction  on the length of the derivation sequence using
Lem.~\ref{lem:big-step:expansion:general}.

\begin{proposition}[Evaluation simulates reduction]
\label{prop:cbv:evaluation:simulates:reduction}
  $M \ssteprelrt V$ implies for all $\rho,\ecftwo$ there exists $\sigma$ such that
  $\lstate{M}{\rho}{\ecftwo}\eval\rstate{V}{\sigma}$.
\end{proposition}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
