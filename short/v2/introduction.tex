

\section{Introduction}


Auditing computation is becoming increasingly important in many areas
of Computer Science. The task of producing a trail or log as a witness
to some computational activity is crucial for  accountability,
reproducibility, process debugging, etc.
%It also applies to data where
%there is extensive work on 
%\emph{provenance}~\cite{DBLP:journals/vldb/HerschelDL17}.
In fact, the cheap availability of fast 
memory and
CPUs could make it viable for certain applications to be developed
in the setting of a language where \emph{all} activity is logged, as
is the case of the system we study in this paper.
Unfortunately, most notions of logs,
trails or some form of recording the history of a computation in
programming languages that we know of, are typically
modelled as an ad-hoc process. Based on studies of the  Curry-Howard
isomorphism  applied to \emph{Justification Logic} (and the \emph{Logic of
Proofs}, a fragment of the former)
~\cite{Artemov:1995,Artemov:2001,Artemov:2008}, a logically-founded proposal was put forward,
namely  the
\lamhold-calculus~\cite{DBLP:conf/ictac/BaveraB10,DBLP:journals/logcom/BaveraB18}.
The \lamhold-calculus is a lambda
calculus with a locally scoped notion of audited computation and in
which computation is history-dependent. Both terms and trails are
logically justified.
The current 
stage of development of \lamhold as a programming language, however, is rather
primitive. This paper sets out to revisit \lamhold,  introducing a
simplified presentation of it which we dub the \lamh-calculus, and then develop
notions of evaluation (big-step) semantics, reduction (small-step)
semantics, and abstract machines for it.   

\smallskip
\textbf{Justification Logic (\JL).} \JL is a modal logic with a modality of the form $s:A$
where $s$ is known as a \emph{proof polynomial} and encodes a Hilbert style
proof that $A$ is true. Its axioms include those of 
\Siv but where the box has been decorated with proof polynomials as follows: $\valid{s}{A}\implica A$, $\valid{s}{(A\implica
  B)}\implica (\valid{t}{A}\implica \valid{\app{s}{t}}{B})$ and
$\valid{s}{A} \implica \valid{\bang{s}}{\valid{s}{A}}$, here ``$\cdot$''
and ``$!$'' denote proof polynomial constructors. \JL enjoys a finite reflection property called \emph{lifting}: if
there is a Hilbert proof $\pi$  of $A$, then $\overline{\pi}:A$ is
provable too, where $\overline{\pi}$ is a proof polynomial encoding $\pi$. In a
Natural Deduction setting~\cite{DBLP:conf/lfcs/ArtemovB07} the witness $s$ encodes a Natural Deduction
proof and logical judgements have the form $\Gamma;\Delta\vdash
A\,|\,s$ and is read as,  ``assuming truth hypothesis $\Gamma$ and
validity hypothesis $\Delta$, proposition  $A$ is true with witness
$s$''. Lifting is modeled as the rule $\indrulename{T-!}$ below left,
where $\cdot$ denotes an empty set of truth hypothesis:
\begin{center}
  $
  \begin{array}{cc}
    \reduce{T-!}
  {
      \wenv; \noenv \vdash A \,|\,s
  }{
      \wenv; \tenv \vdash \bangtyp{s}{A} \,|\, \bangtrm{}{s}
    }
    &
          \reduce{C-!'}
  {
    \begin{gathered}
      \wenv; \noenv \vdash M: A \,|\,t
      \quad
      \judgEq{\wenv}{\noenv}{\rho}{s\succcurlyeq_A t}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\bangtrm{\rho}{M}}{\bangtyp{s}{A}}{\bangwit{s}}}
      } 
  \end{array}
  $
  \end{center}
  As discussed
in~\cite{DBLP:conf/lfcs/ArtemovB07,DBLP:journals/logcom/BaveraB18}, Subject Reduction fails since
although derivations are internalized into the object logic,
normalisation of these derivations, \ie reduction in $s$, is not.
This is salvaged by internalizing normalization of
derivations into the type system through an appropriate  generalization
of $\indrulename{T-!}$, referred to as $\indrulename{C-!'}$ above
right. 
It incorporates witnesses to normalisation
called \emph{trails} and written $\rho$ above. In $\indrulename{C-!'}$
we also include a term assignment (\ie $M$ and $\bangtrm{\rho}{M}$). The
expression $\bangtrm{}{s}$ in $\indrulename{T-!}$ is called a 
\emph{source term} of \lamh and is part of the programmer syntax,
whereas $\bangtrm{\rho}{M}$ is called a \emph{configuration} of \lamh and is part of  the
runtime syntax. Note that $\rho$ is locally scoped in $M$. As
explained later (and as part of our revised presentation of $\lamhold$), we shall not require $t$ above in our
proposed type system; $t$ shall be read off from $M$ via a
\emph{decompilation} function.

\smallskip
\textbf{Evaluation and Reduction for \lamh.} We are interested in devising appropriate evaluation
semantics for \lamh and then establishing a correspondence with
reduction semantics. We propose evaluation judgements of the form
$\lstate{M}{\rho}{\ecf} \eval \rstate{V}{\sigma}$ stating that evaluation
of $\bangtrm{\rho}{\ecf\ctxof{M}}$ produces the configuration
$\bangtrm{\ttrl{\rho}{\decompile{\ecf}\ctxof{\sigma}}}{\ecf\ctxof{V}}$,
where $\ecf$ is an appropriate notion of evaluation context. 
Trail 
$\sigma$ is the ``additional work'' that is
performed under $\ecf$, and on $M$,  to produce $V$. The function
$\decompile{\bullet}$ is the decompilation function referred to
above (\cf~Sec.~\ref{sec:configurations} and \ref{sec:cbv} for details).
A sample evaluation rule is:
\begin{center}
  $
        \reduce{E-!}
    {\lstate{M}{\sigma}{\hole}\eval \rstate{V}{\tau}}
    {\lstate{\bangtrm{\sigma}{M}}{\rho}{\ecf}\eval \rstate{\bangtrm{\ttrl{\sigma}{\tau}}{V}}{\bangtrm{}{\src{\sigma}}}}
    $
\end{center}
Note how evaluation switches from the current trail $\rho$ to $\sigma$, reflecting the
locally scoped nature of trails implicitly bound by ``!''.  Note also
how the computation performed under ``!'', namely $\tau$, is not
visible outside it as indicated by the trail
$\bangtrm{}{\src{\sigma}}$. The latter is the unit trail reflecting that no
work has been done; $\src{\sigma}$ denotes the source
of the trail $\sigma$, all trails have a term from the source syntax as source and a
term from the source syntax as target. Relating evaluation and
reduction will require us to reason modulo some additional structure
on trails. For example, stating that trail composition is associative
$(\rho;\sigma);\tau  \simeq \rho;(\sigma;\tau)$ and that  composition
commutes with term constructors such as in 
        $(\apptrl{\rho_1}{\sigma_1}); (\apptrl{\rho_2}{\sigma_2}) \simeq \apptrl{(\rho_1;\rho_2)}{(\sigma_1;\sigma_2)}$.

\smallskip
\textbf{Abstract Machine for CBN reduction for \lamh.} We develop
an abstract machine based on Krivine's \kam~\cite{DBLP:journals/lisp/Krivine07}. Computational interpretations of intuitionistic modal logic typically understand
expressions of type $\Box A$ as ``uninterpreted code'' or code that, when
executed, will produce an expression that computes a value of type
$A$~\cite{DBLP:journals/jacm/DaviesP01}. This staged-computation
interpretation would hence disallow reduction under
the ``$!$'' constructor. In contrast, in our
setting, this does not make sense since all computation occurs inside
some audited and thus every expression would be in 
normal form. Hence our abstract machine will also include a dump for
managing computation under ``!''.


\smallskip
\textbf{Contributions.}
A summary of the contributions follows.
\begin{itemize}
\item Revised presentation of \lamhold. In formulating \lamh we make a distinction
  between a source syntax, in which programmers write programs, and 
  runtime syntax, which models execution of programs, leading to a clean
  separation of concerns. Our abstract machine runs
  source syntax. We get rid of trail
replacements~\cite{DBLP:journals/logcom/BaveraB18,DBLP:conf/csl/RicciottiC17,DBLP:conf/ictac/RicciottiC18}
  in favor of the standard application
  operation of the lambda calculus. Also, we drop 
  witnesses $t$ in judgements $\Gamma;\Delta\vdash M:A
  \,|\, t$ as mentioned above; these can be computed from $M$ via decompilation (\cf~Sec.~\ref{sec:configurations}).

\item   Structural equivalence for trails history. All
  meta-theoretic results rely on \emph{structural equivalence}
  for trails, adopted from work on proof terms in rewriting~\cite[Ch.8]{TERESE:2003}.

\item   Evaluation and reduction semantics. We present call-by-value and
  call-by-name evaluation and reduction semantics. Evaluation
  semantics for \lamh\ are presented for the first time here.  Adoption of structural equivalence has made
  these presentations and their proof of correspondence possible. 

\item We present an abstract machine for the call-by-name \lamh based
  on Krivine's KAM~\cite{DBLP:journals/lisp/Krivine07}.
  
\end{itemize}



\textbf{Related work.}

In~\cite{DBLP:conf/csl/RicciottiC17}, the original 
proof of strong normalization~\cite{DBLP:conf/ictac/BaveraB10,DBLP:journals/logcom/BaveraB18} is generalized to all typable terms using Girard's
candidates of reducibility technique. Further work by the
same authors~\cite{DBLP:conf/ictac/RicciottiC18} explored adding
explicit substitutions 
to a de Bruijn indices presentation  of \lamh with the aim of reducing
terms more efficiently.  The substitution
subcalculus is based on $\lambda\sigma$. Unfortunately, no
result on efficiency is stated. The authors do, however, propose an abstract
machine for CBV reduction based on the SECD machine~\cite{Landin:1964}. None of these
works study big-step semantics. Call-by-name abstract machines are not
studied either.  Further literature on audit trails that we are aware
of is~\cite{DBLP:conf/csfw/VaughanJMZ08,DBLP:conf/icfp/JiaVMZZSZ08}.

\bigskip

\textbf{Structure of the paper.} We present the source syntax of \lamh
in Sec.~\ref{sec:source_syntax}. After introducing trails in
Sec.~\ref{sec:trails} we present the runtime syntax, namely
configurations in Sec.~\ref{sec:configurations}. We then  introduce
CBV evaluation and reduction semantics and prove their
correspondence. This is done in Sec.~\ref{sec:cbv}. CBN follows in Sec.~\ref{sec:cbn}, where after presenting the evaluation and reduction semantics,
we focus on the abstract machine. Finally, we conclude. An appendix is
provided with detailed proofs.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
