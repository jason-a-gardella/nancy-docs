
% Compiling terms to configurations:
% $\compile{\_}:\termsName\longrightarrow \configsName$.

% \begin{alignat}{2}
%   \compile{\atom}  \eqdef\ &  \atom \\
%   \compile{(\lamwit{a}{s})} \eqdef\ & \lamtrm{a}{\compile{s}} \\
%   \compile{(\appwit{s}{t})} \eqdef\ &\apptrm{\compile{s}}{\compile{t}} \\
%   \compile{(\bangwit{s})} \eqdef\ &\bangtrm{s}{\compile{s}} \\
%   \compile{(\letwit{u}{s}{t})} \eqdef\ &\lettrm{u}{\compile{s}}{\compile{t}} 
% \end{alignat}

%We  write $\rcd{M}$ as a shorthand for $\decompile{M}$.





\section{Runtime Syntax}
\label{sec:configurations}

Configurations represent the runtime state modeling the execution of
terms of the source syntax. They are similar to terms except that audited unit 
terms $\bangtrm{}{s}$ carry a computation history or
trail that that has led to $s$: $\bangtrm{\rho}{s}$.  The set \configsName\ of configurations is defined as follows:

\begin{center}
  $\begin{array}{rcl}
  M, N, O, P, Q  & ::=& 
                        \titrm{A}\,|\,\vartrm{a}\,|\,\avartrm{u}\,|\,\lamtrm{a}{M}\,|\,\apptrm{M}{N}\,|\,\bangtrm{\rho}{M}\,|\,\lettrm{u}{M}{N}
   \end{array}$
\end{center}
    Structural equivalence (Def.~\ref{def:structural_equivalence}) extends to configurations as expected. In
    particular, $\bangtrm{\rho}{M}\simeq \bangtrm{\sigma}{N}$ implies $\rho\simeq \sigma$. 
The following two notions of substitution for
configurations will be used when presenting our
operational semantics. We comment further on them below and include an example.

\begin{center}
  \begin{tabular}{|l|l|}
    \hline
    Substitution of Configurations for Term Variables & $M\sub{N}{a}$ \\\hline

    Substitution of Audited Units for Audited Unit Variables & $M\sub{\bangtrm{\rho}{N}}{u}$ \\\hline
    \end{tabular}
  \end{center}

   Substitution of all free occurrences of $a$ in $M$ with $N$, is
   defined as follows:
% \begin{definition}[Substitution of Configurations for Term Variables]
\begin{center}
  $\begin{array}{rcl}
   \titrm{A} \sub{N}{a} &\eqdef\ &{\titrm{A}} \\
  \vartrm{a}\sub{N}{b} &\eqdef\ &{\begin{cases}
    N,           &a = b \\
    \vartrm{a}, &a \neq b
  \end{cases}} \\
  \avartrm{u}\sub{N}{a} &\eqdef\ &{\avartrm{u}} \\
  (\lamtrm{a}{R}) \sub{N}{a} &\eqdef\ &{\lamtrm{a}{R \sub{N}{a}}} \\
  (\apptrm{R}{S}) \sub{N}{a} &\eqdef\ &{\apptrm{R \sub{N}{a}}{S \sub{N}{a}}} \\
  (\bangtrm{\rho}{R}) \sub{N}{a} &\eqdef\ &{\bangtrm{\rho}{R}} \\
  (\lettrm{v}{R}{S}) \sub{N}{a} &\eqdef\ &{\lettrm{v}{R \sub{N}{a}}{S \sub{N}{a}}} \\
%  \trmsubrule{(\titrm{\trmbranches})}{\titrm{\trmsub{\trmbranches}}}
\end{array}$
   \end{center}
%\end{definition}
Substitution of audited units in configurations must be done with
some care\footnote{A fact already noted in~\cite{bruggink:phd2008} where proof terms
  for Higher-Order Rewrite Systems are proposed.}. It would not be
correct to blindly replace audited unit variables by
trails. For example, $\ttrl{u}{u}\simeq u$ however replacing $u$ with
$\rho$ leads to $\ttrl{\rho}{\rho}\simeq \rho$ which is, in general, incorrect
since one cannot compose a trail with itself (unless it has the
same source and target). This leads to
the following definition (see, in particular, the next-to-last clause)~\cite{DBLP:journals/logcom/BaveraB18,DBLP:conf/csl/RicciottiC17}:

%\begin{definition}[Substitution of Audited Units for Audited Unit Variables]
%\label{def:substitution:auditedunits:for:auditedvariables}
\begin{center}
  $\begin{array}{rcl}
  \titrm{A}\sub{\bangtrm{\rho}{N}}{u} &\eqdef\ &{\titrm{A}} \\
  \vartrm{a}\sub{\bangtrm{\rho}{N}}{u} &\eqdef\ &{\vartrm{a}} \\
  \avartrm{v}\sub{\bangtrm{\rho}{N}}{u} &\eqdef\ &{\begin{cases}
    N,           &u = v \\
    \avartrm{v}, &u \neq v
  \end{cases}} \\
  (\lamtrm{a}{R}) \sub{\bangtrm{\rho}{N}}{u} &\eqdef\ &{\lamtrm{a}{R \sub{\bangtrm{\rho}{N}}{u}}} \\
  (\apptrm{R}{S}) \sub{\bangtrm{\rho}{N}}{u} &\eqdef\ &{\apptrm{R \sub{\bangtrm{\rho}{N}}{u}}{S \sub{\bangtrm{\rho}{N}}{u}}} \\
  (\bangtrm{\sigma}{R}) \sub{\bangtrm{\rho}{N}}{u} &\eqdef\ &{\bangtrm{\ttrl{\sigma\sub{\src{\rho}}{u}}{\decompile{R}\sub{\rho}{u}}}{R\sub{\bangtrm{\rho}{N}}{u}}} \\
  (\lettrm{v}{R}{S}) \sub{\bangtrm{\rho}{N}}{u} &\eqdef\ &{\lettrm{v}{R \sub{\bangtrm{\rho}{N}}{u}}{S \sub{\bangtrm{\rho}{N}}{u}}} 
%  \trmsubrule{(\titrm{\trmbranches})}{\titrm{\trmsub{\trmbranches}}}
   \end{array}$
 \end{center}
%\end{definition}

As an example consider a term such as
$\lettrm{u}{\bangtrm{\rho}{V}}{\bangtrm{\bangwit{u}}{\bangtrm{u}{u}}}$
and suppose that $s$ is the source of $\rho$. Its 
reduct is
$(\bangtrm{\bangwit{u}}{\bangtrm{u}{u}})\sub{\bangtrm{\rho}{V}}{u} =
\bangtrm{\ttrl{\bangwit{s}}{\bangwit{s}}}{\bangtrm{\ttrl{s}{\rho}}{V}}\simeq
 \bangtrm{\bangwit{s}}{\bangtrm{\rho}{V}}$. Note how the trail has
 been persisted in the innermost computation. Also note how the
 outermost ``!'' cannot see the computation under it.


\textbf{Typing for Configurations.} A \defin{configuration typing judgement} is an expression of the form
$\withenvs{\wenv}{\tenv}{\trmtypwit{M}{A}{s}}$. Typing rules defining
the meaning of this judgement  are those
for terms (\cf~Fig.~\ref{fig:source_terms_typing}) except that $\indrulename{T-!}$ is replaced with
$\indrulename{C-!}$:

%\begin{remark}
%$\withenvs{\wenv}{\tenv}{\trmtypwit{M}{A}{s}}$ implies $\decompile{M}=s$.
%  \end{remark}

% \[
%   \mathcal{T}^{B}(\psi) \triangleq
%     \begin{cases}
%       B &(\psi = \dbrch, \rbrch, \betabrch, \betaboxbrch, \tibrch, \trplebrch) \\
%       \lamtyp{B}{B} &(\psi = \lambrch) \\
%       \lamtyp{B}{\lamtyp{B}{B}} &(\psi = \tbrch, \appbrch, \letbrch, \trplcbrch)
%     \end{cases}
% \]

\begin{center}
  $
  \reduce{C-!}
  {
    \begin{gathered}
      \withenvs{\wenv}{\noenv}{\trmtypwit{M}{A}{t}}
      \quad
      \judgEq{\wenv}{\noenv}{\rho}{s\succcurlyeq_A \decompile{M}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\bangtrm{\rho}{M}}{\bangtyp{s}{A}}{\bangwit{s}}}
      }
      $
      \label{rule:C-Bang}
    \end{center}
\noindent Expression $\decompile{M} $ in $\indrulename{C-!}$ is a term
called the 
\defin{decompilation} of the configuration $M$. The decompilation
function
$\decompile{\_}:\configsName\longrightarrow
\termsName$ produces
the term that is currently being computed in the runtime configuration
$M$:
\begin{center}
  $\begin{array}{cc}
     \begin{array}{rcl}
  \decompile{\atom}  & \eqdef &  \atom \\
  \decompile{(\lamtrm{a}{M})}  &\eqdef & \lamwit{a}{\decompile{M}} \\
   \decompile{(\apptrm{M}{N})} & \eqdef & \appwit{\decompile{M}}{\decompile{N}} \\
 %  \decompile{(\bangtrm{\rho}{M})} &\eqdef &\bangwit{\src{\rho}} \\
 %  \decompile{(\lettrm{u}{M}{N})} &\eqdef &\letwit{u}{\decompile{M}}{\decompile{N}} 
     \end{array}
                               &
     \begin{array}{rcl}
 % \decompile{\atom}  & \eqdef &  \atom \\
 %  \decompile{(\lamtrm{a}{M})}  &\eqdef & \lamwit{a}{\decompile{M}} \\
 %  \decompile{(\apptrm{M}{N})} & \eqdef & \appwit{\decompile{M}}{\decompile{N}} \\
   \decompile{(\bangtrm{\rho}{M})} &\eqdef &\bangwit{\src{\rho}} \\
   \decompile{(\lettrm{u}{M}{N})} &\eqdef
                               &\letwit{u}{\decompile{M}}{\decompile{N}}
       \\
    \ 
     \end{array}
   \end{array}$
 \end{center}
 Note that since, as mentioned in the introduction, trails are
locally scoped within their audited units, decompilation  of
$\bangtrm{\rho}{M} $ produces $\bangwit{\src{\rho}}$: any computation
performed inside the audited unit is not visible from outside it. 

%\begin{remark}
%  \label{rem:decompilation:bang-free:configurations}
%  If $M$ is bang-free then $\decompile{M}=M$.
%\end{remark}




% \[
%   \reduce{T-TI}
%   {
%     \begin{gathered}
%       d \in \dom{\trmbranches} \\
%       \dom{\trmbranches} = \dom{\witbranches} \\
%       [\withenvs{\wenv}{\noenv}{\trmtypwit{\trmbranches(\psi)}{\mathcal{T}^B(\psi)}{\witbranches(\psi)}}]_{\psi \in \dom{\trmbranches}}
%     \end{gathered}
%   }{
%     \withenvs{\wenv}{\tenv}{\trmtypwit{\titrm{\trmbranches}}{B}{\tiwit{\witbranches}}}
%   }
% \]

%\begin{definition}
%\label{def:well-formed_configuration}
A typed expression of the form $\bangtrm{\rho}{N}$ is said to be \defin{consistent} if
$\tgt{\rho}=\decompile{N}$.
A typed configuration $M$ is \defin{well-formed} if every sub-expression of
$M$ of the form $\bangtrm{\rho}{N}$ is consistent.
%\end{definition}

\begin{lemma}
  Well-typed configurations are well-formed.
  \end{lemma}

  
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
