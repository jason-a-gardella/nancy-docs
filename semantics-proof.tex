\documentclass{article}

\input{./preamble.tex}

\title{Semantic Equivalence Proof}

\begin{document}
\maketitle

Prove: $\bstep{M}{q}{V}{q'} \iff \mstep{\bangtrm{q}{M}}{\bangtrm{\ttrl{q}{q'}}{V}}$

\section{Big to Small}

Prove: $\bstep{M}{q}{V}{q'} \Longrightarrow \mstep{\bangtrm{q}{M}}{\bangtrm{\ttrl{q}{q'}}{V}}$ \\

Prove by induction on derivation of $\bstep{M}{q}{V}{q'}$.

\subsection{Base Case $M = V$}

The derivation ends in:

\[
  \frac{}
  {
    \bstep{V}{q}{V}{\rcd{V}}
  }
\]

Prove: $\mstep{\bangtrm{q}{V}}{\bangtrm{\ttrl{q}{\rcd{V}}}{V}}$ \\

Trivial, because $\mstep{\bangtrm{q}{V}}{\bangtrm{q}{V}}$ in 0 steps. $\rcd{V}$ represents no computation,
so the trails $e$ and $\ttrl{q}{\rcd{V}}$ are equivalent, and therefore the terms $\bangtrm{q}{V}$ and
$\bangtrm{\ttrl{q}{\rcd{V}}}{V}$ are equivalent.

\newcommand{\bangm}{\bangtrm{q_1}{N}}
\subsection{Bang Case $M = \bangm$}

The derivation ends in:

\[
  \frac{
    \bstep{N}{q_1}{V}{q_3}
  }
  {
    \bstep{\bangm}{q_2}{\bangtrm{\ttrl{q_1}{q_3}}{V}}{q_2}
  }
\]

Prove: $\mstep{\bangtrm{q_2}{\bangm}}{\bangtrm{\ttrl{q_2}{q_2}}{\bangtrm{\ttrl{q_1}{q_3}}{V}}}$ \\

By the inductive hypothesis:

\begin{qnumerate}
  \item $\mstep{\bangtrm{q_1}{N}}{\bangtrm{\ttrl{q_1}{q_3}}}{V}$
\end{qnumerate}

Proof:
\[
  \bangtrm{q_2}{\bangm} \xtwoheadrightarrow{a} \bangtrm{q_2}{\bangtrm{\ttrl{q_1}{q_3}}{V}}
\]

The trails $e_2$ and $\ttrl{q_2}{q_2}$ are equivalent, so the terms are equivalent.

\newcommand{\betam}{\apptrm{M_1}{M_2}}
\subsection{Beta Case $M = \betam$}

The derivation ends in:

\[
  \frac{
    \begin{gathered}
      \bstep{M_1}{q_1}{\lamtrm{a}{M_1'}}{q_2} \\
      \bstep{M_2}{\ttrl{q_1}{\apptrl{q_2}{\rcd{M_2}}}}{V}{q_3} \\
      \bstep{\appsub{M_1'}{V}{a}}{\ttrl{q_1}{\ttrl{\apptrl{q_2}{q_3}}{\batrl{a}{\tgt{q_2}}{\tgt{q_3}}}}}{W}{q_4}
    \end{gathered}
  }
  {
    \bstep{\betam}{q_1}{W}{\ttrl{\apptrl{q_2}{q_3}}{\ttrl{\batrl{a}{\tgt{q_2}}{\tgt{q_3}}}{q_4}}}
  }
\]

Prove: $\mstep{\bangtrm{q_2}{\betam}}{\bangtrm{\ttrl{q_1}{\ttrl{\apptrl{q_2}{q_3}}{\ttrl{\batrl{a}{\tgt{q_2}}{\tgt{q_3}}}{q_4}}}}{W}}$ \\

By the inductive hypothesis:

\begin{qnumerate}
  \item $\mstep{\bangtrm{q_1}{M_1}}{\bangtrm{\ttrl{q_1}{q_2}}{\lamtrm{a}{M_1'}}}$
  \item $\mstep{\bangtrm{\ttrl{q_1}{\apptrl{q_2}{\rcd{M_2}}}}{M_2}}{\bangtrm{\ttrl{q_1}{\ttrl{\apptrl{q_2}{\rcd{M_2}}}{q_3}}}{V}}$
  \item $\mstep{\bangtrm{\ttrl{q_1}{\apptrl{q_2}{q_3}}}{\appsub{M_1'}{V}{a}}}{\bangtrm{\ttrl{q_1}{\ttrl{\apptrl{q_2}{q_3}}{\batrl{a}{\tgt{q_2}}{\tgt{q_3}}}}}{W}}$
\end{qnumerate}

Lemma 1 (L1):

\[
\mstep{\bangtrm{q_1}{M}}{\bangtrm{\ttrl{q_1}{q_2}}{P}} \Longrightarrow \mstep{\bangtrm{q_1}{\apptrm{M}{N}}}{\bangtrm{\ttrl{q_1}{\apptrl{q_2}{\rcd{N}}}}{\apptrm{P}{N}}}
\]

Lemma 2 (L2):

\[
  \mstep{\bangtrm{\ttrl{q_1}{\apptrl{q_2}{\rcd{M}}}}{M}}{\bangtrm{\ttrl{q_1}{\ttrl{\apptrl{q_2}{\rcd{M}}}{q_3}}}{V}}
  \Longrightarrow
  \mstep{\bangtrm{\ttrl{q_1}{\apptrl{q_2}{\rcd{M}}}}{\apptrm{(\lamtrm{a}{P})}{M}}}{\bangtrm{\ttrl{q_1}{\apptrl{q_2}{q_3}}}{\apptrm{(\lamtrm{a}{P})}{V}}}
\]

Proof:
\begin{align*}
  \bangtrm{q_1}{\betam} &\xtwoheadrightarrow{a, L1} \bangtrm{\ttrl{q_1}{\apptrl{q_2}{\rcd{M_2}}}}{\apptrm{(\lamtrm{a}{M_1'})}{M_2}} \\
                        &\xtwoheadrightarrow{b, L2} \bangtrm{\ttrl{q_1}{\apptrl{q_2}{q_3}}}{\apptrm{(\lamtrm{a}{M_1'})}{V}} \\
                        &\xrightarrow{\beta\ \ \ } \bangtrm{\ttrl{q_1}{\ttrl{\apptrl{q_2}{q_3}}{\batrl{a}{\cd{M_1'}}{\cd{V}}}}}{\appsub{M_1'}{V}{a}} \\
                        &\xtwoheadrightarrow{c\ \ \ } \bangtrm{\ttrl{q_1}{\ttrl{\apptrl{q_2}{q_3}}{\ttrl{\batrl{a}{\cd{M_1'}}{\cd{V}}}}{q_4}}}{W}
\end{align*}

\newcommand{\betaboxm}{\lettrm{u}{M}{N}}
\subsection{Beta Box Case $M = \betaboxm$}

The derivation ends in:

\[
  \frac{
    \begin{gathered}
      \bstep{M}{q_1}{\bangtrm{q_2}{V}}{q_3} \\
      \delta = [(V, e_2, \src{q_2})/u] \\
      \bstep{\trmsub{N}}{\ttrl{q_1}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\trlsub{N}}}}}{W}{q_4}
    \end{gathered}
  }
  {
    \bstep{\betaboxm}{q_1}{W}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\ttrl{\trlsub{N}}{q_4}}}}
  }
\]

Prove: $\mstep{\bangtrm{q_1}{\betaboxm}}{\bangtrm{\ttrl{q_1}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\ttrl{\trlsub{N}}{q_4}}}}}}{W}$ \\

By the inductive hypothesis:

\begin{qnumerate}
  \item $\mstep{\bangtrm{q_1}{M}}{\bangtrm{\ttrl{q_1}{q_3}}{\bangtrm{q_2}{V}}}$
  \item $\mstep{\bangtrm{\ttrl{q_1}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\trlsub{N}}}}}{\trmsub{N}}}
    {\bangtrm{\ttrl{q_1}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\ttrl{\trlsub{N}}{q_4}}}}}{W}}$
\end{qnumerate}

Lemma 3 (L3):

\[
  \mstep{\bangtrm{q_1}{M}}{\bangtrm{\ttrl{q_1}{q_3}}{\bangtrm{q_2}{V}}}
  \Longrightarrow
  \mstep{\bangtrm{q_1}{\lettrm{u}{M}{N}}}{\bangtrm{\ttrl{q_1}{\lettrl{u}{q_3}{\rcd{N}}}}{\lettrm{u}{V}{N}}}
\]

Proof:
\begin{align*}
  \bangtrm{q_1}{\betaboxm} &\xtwoheadrightarrow{a, L3} \bangtrm{\ttrl{q_1}{\lettrl{u}{q_3}{\rcd{N}}}}{\lettrm{u}{V}{N}} \\
                           &\xrightarrow{\betabox\ \ } \bangtrm{\ttrl{q_1}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\trlsub{N}}}}}{\trmsub{N}} \\
                           &\xtwoheadrightarrow{b\ \ \ } \bangtrm{\ttrl{q_1}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\ttrl{\trlsub{N}}{q_4}}}}}{W}
\end{align*}

\newcommand{\tim}{\titrm{\trmbranches}}
\section{TI Case $M=\tim$}

The derivation ends in

\[
  \frac{
    \begin{gathered}
      \bstep{\trmbranches}{q_1}{\trmbranches^{V}}{q_{\trmbranches}} \\
      \bstep{(\ttrl{q_1}{q_{\trmbranches}})\trmbranches}{\ttrl{q_1}{\ttrl{q_{\trmbranches}}{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}}}{W}{q_2}
    \end{gathered}
  }{
    \bstep{\tim}{q_1}{W}{\ttrl{q_{\trmbranches}}{\ttrl{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}{q_2}}}
  }
\]

By the inductive hypothesis:

\begin{qnumerate}
  \item $\mstep{\bangtrm{q_1}{\trmbranches}}{\bangtrm{\ttrl{q_1}{q_{\trmbranches}}}{\trmbranches^{V}}}$
  \item $\mstep{\bangtrm{\ttrl{q_1}{\ttrl{q_{\trmbranches}}{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}}}{q_1\trmbranches^{V}}}{\bangtrm{\ttrl{q_1}{\ttrl{q_{\trmbranches}}{\ttrl{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}{q_2}}}}{W}}$
\end{qnumerate}

Lemma 4:

\[
  \mstep{\bangtrm{q_1}{\trmbranches}}{\bangtrm{\ttrl{q_1}{q_{\trmbranches}}}\trmbranches^{V}}
  \Longrightarrow
  \mstep{\bangtrm{q_1}{\titrm{\trmbranches}}}{\bangtrm{\ttrl{q_1}{q_{\trmbranches}}}{\titrm{\trmbranches^{V}}}}
\]

Proof:
\begin{align*}
  \bangtrm{q_1}{\tim} &\xtwoheadrightarrow{a\ } \bangtrm{\ttrl{q_1}{q_{\trmbranches}}}{\titrm{\trmbranches^{V}}} \\
                      &\xrightarrow{TI} \bangtrm{\ttrl{q_1}{\ttrl{q_{\trmbranches}}{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}}}{q_1\trmbranches^{V}} \\
                      &\xtwoheadrightarrow{b\ } \bangtrm{\ttrl{q_1}{\ttrl{q_{\trmbranches}}{\ttrl{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}{q_2}}}}{W}
\end{align*}

\end{document}
