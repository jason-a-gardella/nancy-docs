\documentclass{article}

\usepackage{preamble}

\title{Nancy Language Documentation}

\begin{document}
\maketitle

\section{Syntax}

\subsection{Terms}
\begin{alignat}{2}
  M, N, \ldots ::=&\ &&\vartrm{a} \\
                 |&\ &&\avartrm{u} \\
                 |&\ &&\lamtrm{a}{M} \\
                 |&\ &&\apptrm{M}{N} \\
                 |&\ &&\bangtrm{q}{M} \\
                 |&\ &&\lettrm{u}{M}{N} \\
                 |&\ &&\titrm{\trmbranches}
\end{alignat}

\subsection{Witnesses}
\begin{alignat}{2}
  s, t, \ldots ::=&\ &&\varwit{a} \\
                 |&\ &&\avarwit{u} \\
                 |&\ &&\lamwit{a}{s} \\
                 |&\ &&\appwit{s}{t} \\
                 |&\ &&\bangwit{s} \\
                 |&\ &&\letwit{u}{s}{t} \\
                 |&\ &&\tiwit{\witbranches}
\end{alignat}

\subsection{Trails}
\begin{alignat}{2}
  q, q', \ldots ::=&\ &&\rtrl{s} \\
                  |&\ &&\ttrllong{q}{q'} \\
                  |&\ &&\batrl{a}{s}{t} \\
                  |&\ &&\bbtrl{u}{s}{t} \\
                  |&\ &&\titrl{q}{\witbranches} \\
                  |&\ &&\lamtrl{a}{q} \\
                  |&\ &&\apptrl{q}{q'} \\
                  |&\ &&\lettrl{u}{q}{q'} \\
                  |&\ &&\trpltrl{\trlbranches}
\end{alignat}

\subsection{Trail Inspection Branches}
\begin{align}
  \witbranches, \witbranches', \ldots ::=\ &\{\tibranch{s}{\psi}\} \\
  \trmbranches, \trmbranches', \ldots ::=\ &\{\tibranch{M}{\psi}\} \\
  \trmbranches^{V}, \trmbranches^{V'}, \ldots ::=\ &\{\tibranch{V}{\psi}\} \\
  \trlbranches, \trlbranches', \ldots ::=\ &\{\tibranch{q}{\psi}\}
\end{align}

\subsection{Trail Inspection Branch Labels}
\begin{align}
  \psi, \psi', \ldots ::=&\ \rbrch \\
                        |&\ \tbrch \\
                        |&\ \betabrch \\
                        |&\ \betaboxbrch \\
                        |&\ \tibrch \\
                        |&\ \lambrch \\
                        |&\ \appbrch \\
                        |&\ \letbrch \\
                        |&\ \trplebrch \\
                        |&\ \trplcbrch \\
\end{align}

\section{Auxillary Functions}

\subsection{Substitution on Witnesses}

\subsubsection{Simple Variables}

For $\gamma = \sub{t}{a}$, substitution of simple variables on a witness $r$,
denoted as $r\gamma$, is defined as:

\begin{align}
  \witsubsimplerule{\varwit{c}}{\begin{cases}
    t  &(a = c) \\
    \varwit{c} &(a \neq c)
  \end{cases}} \\
  \witsubsimplerule{\avarwit{v}}{\avarwit{v}} \\
  \witsubsimplerule{\lamwit{c}{s}}{\lamwit{c}{s\gamma}} \\
  \witsubsimplerule{\appwit{s}{s'}}{\appwit{s\gamma}{s'\gamma}} \\
  \witsubsimplerule{\bangwit{s}}{\bangwit{s}} \\
  \witsubsimplerule{\letwit{v}{s}{s'}}{\letwit{v}{s\gamma}{s'\gamma}} \\
  \witsubsimplerule{\tiwit{\witbranches}}{\tiwit{\witbranches\gamma}}
\end{align}

$\witbranches\gamma$ is defined pointwise.

\subsubsection{Audited Variables}

For $\delta = \sub{t}{u}$, substitution of audited variables on a witness $r$,
denoted as $r\delta$, is defined as:

\begin{align}
  \witsubauditedrule{\varwit{a}}{\varwit{a}} \\
  \witsubauditedrule{\avarwit{v}}{\begin{cases}
    t  &(u = v) \\
    \varwit{v} &(u \neq v)
  \end{cases}} \\
  \witsubauditedrule{\lamwit{c}{s}}{\lamwit{c}{s\delta}} \\
  \witsubauditedrule{\appwit{s}{s'}}{\appwit{s\delta}{s'\delta}} \\
  \witsubauditedrule{\bangwit{s}}{\bangwit{s\delta}} \\
  \witsubauditedrule{\letwit{v}{s}{s'}}{\letwit{v}{s\delta}{s'\delta}} \\
  \witsubauditedrule{\tiwit{\witbranches}}{\tiwit{\witbranches\delta}}
\end{align}

$\witbranches\delta$ is defined pointwise.

\subsection{Audited Variable Substitution}

\[
  \delta = [(N, q, t)/u],\ \text{fix}\ \delta' = [t/u]
\]

\subsubsection{Term Substitution}

\begin{align}
  \trmsubrule{\vartrm{a}}{\vartrm{a}} \\
  \trmsubrule{\avartrm{v}}{\begin{cases}
    N           &(u = v) \\
    \avartrm{v} &(u \neq v)
  \end{cases}} \\
  \trmsubrule{(\lamtrm{a}{R})}{\lamtrm{a}{(\trmsub{R})}} \\
  \trmsubrule{(\apptrm{R}{S})}{\apptrm{(\trmsub{R})}{(\trmsub{S})}} \\
  \trmsubrule{(\bangtrm{q'}{R})}{\bangtrm{\ttrl{q'\delta'}{\trlsub{R}}}{(\trmsub{R})}} \\
  \trmsubrule{(\lettrm{v}{R}{S})}{\lettrm{v}{(\trmsub{R})}{(\trmsub{S})}} \\
  \trmsubrule{(\titrm{\trmbranches})}{\titrm{\trmsub{\trmbranches}}}
\end{align}

$\trmsub{\trmbranches}$ is defined pointwise on $\trmbranches$.

\subsubsection{Trail Substitution}

\begin{align}
  \trlsubrule{\vartrm{a}}{\rcd{a}} \\
  \trlsubrule{\avartrm{v}}{\begin{cases}
    q  &(u = v) \\
    \rcd{v} &(u \neq v)
  \end{cases}} \\
  \trlsubrule{\lamtrm{a}{R}}{\lamtrl{a}{\trlsub{R}}} \\
  \trlsubrule{\apptrm{R}{S}}{\apptrl{\trlsub{R}}{\trlsub{S}}} \\
  \trlsubrule{\bangtrm{q'}{R}}{\rtrl{\bangwit{\src{q'}\delta'}}} \\
  \trlsubrule{\lettrm{v}{R}{S}}{\lettrl{v}{\trlsub{R}}{\trlsub{S}}} \\
  \trlsubrule{\titrm{\trmbranches}}{\trpltrl{\trlsub{\trmbranches}}}
\end{align}

$\trlsub{\trmbranches}$ is defined pointwise on $\trmbranches$.

\subsection{Compute Witness}

\begin{align}
  \cdrule{\vartrm{a}}{\varwit{a}} \\
  \cdrule{\avartrm{u}}{\avarwit{u}} \\
  \cdrule{\lamtrm{a}{N}}{\lamwit{a}{\cd{N}}} \\
  \cdrule{\apptrm{N}{R}}{\appwit{\cd{N}}{\cd{R}}} \\
  \cdrule{\bangtrm{q}{N}}{\bangwit{\src{q}}} \\
  \cdrule{\lettrm{u}{N}{R}}{\letwit{u}{\cd{N}}{\cd{R}}} \\
  \cdrule{\titrm{\trmbranches}}{\tiwit{\cd{\trmbranches}}}
\end{align}

$\cd{\trmbranches}$ is defined pointwise.

\subsection{Source}

\begin{align}
  \srcrule{\rtrl{s}}{s} \\
  \srcrule{\ttrllong{q_1}{q_2}}{\src{q_1}} \\
  \srcrule{\batrl{a}{s_1}{s_2}}{\appwit{\lamwit{a}{s_1}}{s_2}} \\
  \srcrule{\bbtrl{v}{s_1}{s_2}}{\letwit{v}{!s_1}{s_2}} \\
  \srcrule{\titrl{q_1}{\witbranches}}{\tiwit{\witbranches}} \\
  \srcrule{\lamtrl{a}{q_1}}{\lamwit{a}{\src{q_1}}} \\
  \srcrule{\apptrl{q_1}{q_2}}{\appwit{\src{q_1}}{\src{q_2}}} \\
  \srcrule{\lettrl{v}{q_1}{q_2}}{\letwit{v}{\src{q_1}}{\src{q_2}}} \\
  \srcrule{\trpltrl{\trlbranches}}{\tiwit{\src{\trlbranches}}}
\end{align}

$\src{\trlbranches}$ is defined pointwise.

\subsection{Target}

\begin{align}
  \tgtrule{\rtrl{s}}{s} \\
  \tgtrule{\ttrllong{q_1}{q_2}}{\tgt{q_2}} \\
  \tgtrule{\batrl{a}{s_1}{s_2}}{\appsub{s_1}{s_2}{a}} \\
  \tgtrule{\bbtrl{v}{s_1}{s_2}}{\appsub{s_2}{s_1}{v}} \\
  \tgtrule{\titrl{q_1}{\witbranches}}{q_1 \witbranches} \\
  \tgtrule{\lamtrl{a}{q_1}}{\lamwit{a}{\tgt{q_1}}} \\
  \tgtrule{\apptrl{q_1}{q_2}}{\appwit{\tgt{q_1}}{\tgt{q_2}}} \\
  \tgtrule{\lettrl{v}{q_1}{q_2}}{\letwit{v}{\tgt{q_1}}{\tgt{q_2}}} \\
  \tgtrule{\trpltrl{\trlbranches}}{\tiwit{\tgt{\trlbranches}}}
\end{align}

$\tgt{\trlbranches}$ is defined pointwise.

\subsection{Abbreviations}

\begin{align}
  \rcd{M} &= \rtrl{\cd{M}} \\
  \ttrl{q_1}{q_2} &= \ttrllong{q_1}{q_2}
\end{align}

\section{Types}

\begin{alignat}{2}
A, B, \ldots ::=&\ &&\lamtyp{A}{B} \\
               |&\ &&\bangtyp{s}{A}
\end{alignat}

\section{Typing Judgements}

\[
  \mathcal{T}^{B}(\psi) \triangleq
    \begin{cases}
      B &(\psi = \dbrch, \rbrch, \betabrch, \betaboxbrch, \tibrch, \trplebrch) \\
      \lamtyp{B}{B} &(\psi = \lambrch) \\
      \lamtyp{B}{\lamtyp{B}{B}} &(\psi = \tbrch, \appbrch, \letbrch, \trplcbrch)
    \end{cases}
\]

\[
  \reduce{T-VAR}
  {
    \intenv{\trmtyp{\vartrm{a}}{A}}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\vartrm{a}}{A}{\varwit{a}}}
  }
\]

\[
  \reduce{T-ABS}
  {
    \withenvs{\wenv}{\tenv, \trmtyp{\vartrm{a}}{A}}{
      \trmtypwit{M}{B}{s}
    }
  }{
    \withenvs{\wenv}{\tenv}{
      \trmtypwit{\lamtrm{a}{M}}{\lamtyp{A}{B}}{\lamwit{a}{s}}
    }
  }
\]

\[
  \reduce{T-APP}
  {
    \begin{gathered}
      \withenvs{\wenv}{\tenv}{\trmtypwit{M}{\lamtyp{A}{B}}{s}} \\
      \withenvs{\wenv}{\tenv}{\trmtypwit{N}{A}{t}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\apptrm{M}{N}}{B}{\appwit{s}{t}}}
  }
\]

\[
  \reduce{T-AVAR}
  {
    \inwenv{\avartrm{u} :: A}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\avartrm{u}}{A}{\avarwit{u}}}
  }
\]

\[
  \reduce{T-BANG}
  {
    \begin{gathered}
      \withenvs{\wenv}{\noenv}{\trmtypwit{M}{A}{t}} \\
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\bangtrm{q}{M}}{\bangtyp{s}{A}}{\bangwit{s}}}
  }
\]

\[
  \reduce{T-LET}
  {
    \begin{gathered}
      \withenvs{\wenv}{\tenv}{\trmtypwit{M}{\bangtyp{r}{A}}{s}} \\
      \withenvs{\wenv, u :: A}{\tenv}{\trmtypwit{N}{C}{t}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{
      \trmtypwit{\lettrm{u}{M}{N}}
        {\appsub{C}{r}{u}}
        {\letwit{u}{s}{t}}
    }
  }
\]

\[
  \reduce{T-TI}
  {
    \begin{gathered}
      d \in \dom{\trmbranches} \\
      \dom{\trmbranches} = \dom{\witbranches} \\
      [\withenvs{\wenv}{\noenv}{\trmtypwit{\trmbranches(\psi)}{\mathcal{T}^B(\psi)}{\witbranches(\psi)}}]_{\psi \in \dom{\trmbranches}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\titrm{\trmbranches}}{B}{\tiwit{\witbranches}}}
  }
\]

\section{Big Step Semantics}

$(M, q)$ represents an expression $M$ with a current trail of $q$.

$(M, q_1) \Downarrow^{q_2} V$ means that under trail $q_1$, $M$ reduces to
$V$ with trail $q_2$.

\[
  V ::= \vartrm{a}\ |\ \avartrm{u}\ |\ \lamtrm{a}{M}\ |\ \bangtrm{q}{V}
\]

\[
  \reduce{B-V}
    {}
    {\bstep{V}{q}{V}{\rcd{V}}}
\]

\[
  \reduce{B-BANG}
    {\bstep{M}{q_1}{V}{3}}
    {\bstep{\bangtrm{1}{M}}{q_2}{\bangtrm{\ttrl{q_1}{q_3}}{V}}{\bangwit{\rtrl{\cd{\bangtrm{q_1}{M}}}}}}
\]

\[
  \reduce{B-\beta}
  {
    \begin{gathered}
      \bstep{M}{1}{\lamtrm{a}{M'}}{q_2} \\
      \bstep{N}{\ttrl{1}{\apptrl{q_2}{\rcd{N}}}}{V}{q_3} \\
      \bstep{\appsub{M'}{V}{a}}{\ttrl{1}{\ttrl{\apptrl{q_2}{q_3}}{\batrl{a}{\tgt{q_2}}{\tgt{q_3}}}}}{W}{q_4}
    \end{gathered}
  }
  {
    \bstep{\apptrm{M}{N}}{1}{W}{\ttrl{\apptrl{q_2}{q_3}}{\ttrl{\batrl{a}{\tgt{q_2}}{\tgt{q_3}}}{q_4}}}
  }
\]

\[
  \reduce{B-\betabox}
  {
    \begin{gathered}
      \bstep{M}{1}{\bangtrm{q_2}{V}}{q_3} \\
      \delta = [(V, 2, \src{q_2})/u] \\
      \bstep{\trmsub{N}}{\ttrl{1}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\trlsub{N}}}}}{W}{q_4}
    \end{gathered}
  }
  {
    \bstep{\lettrm{u}{M}{N}}{1}{W}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\ttrl{\trlsub{N}}{q_4}}}}
  }
\]

\[
  \reduce{B-TI}
  {
    \begin{gathered}
      \bstep{\trmbranches}{1}{\trmbranches^{V}}{q_{\trmbranches}} \\
      \bstep{(\ttrl{1}{q_{\trmbranches}})\trmbranches}{\ttrl{q_1}{\ttrl{q_{\trmbranches}}{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}}}{W}{q_2}
    \end{gathered}
  }
  {
    \bstep{\titrm{\trmbranches}}{1}{W}{\ttrl{q_{\trmbranches}}{\ttrl{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}{q_2}}}
  }
\]

\[
  \reduce{B-TRPL}
  {
    \begin{gathered}
      \bstep{\trmbranches_{r}}{1}{\trmbranches_{r}^{V}}{q_r} \\
      \bstep{\trmbranches_{t}}{\ttrl{1}{\trpltrl{q_r, \rcd{\trmbranches_{t}}, \ldots}}}{\trmbranches_{t}^{V}}{q_t} \\
      \ldots \\
      \bstep{\trmbranches_{trpl}}{\ttrl{1}{\trpltrl{q_r, q_t, \ldots, \rcd{\trmbranches_{trpl}}}}}{\trmbranches_{trpl}^{V}}{q_{trpl}}
    \end{gathered}
  }
  {
    \bstep{\trmbranches}{1}{\trmbranches^{V}}{\trpltrl{q_r, q_t, \ldots, q_{trpl}}}
  }
\]

\section{Small Step Semantics}

\subsection{Term Evaluation Context}

\[
  \ecf ::=
    \hole |
    \lamtrm{a}{\ecf} |
    \apptrm{\ecf}{M} |
    \apptrm{V}{\ecf} |
    \lettrm{u}{\ecf}{M} |
    \titrm{\tibranches{\tibranch{V}{\psi}}{\ecf/\psi'}{\tibranch{N}{\psi''}}}
\]

\subsection{Trail Evaluation Context}

\begin{align}
  \ecq &::=
    \hole |
    \ttrllong{\ecq}{q} |
    \ttrllong{q}{\ecq} |
    \apptrl{\ecq}{q} |
    \apptrl{q}{\ecq} |
    \lamtrl{a}{\ecq} \\ &|
    \lettrl{\ecq}{u}{q} |
    \lettrl{q}{u}{\ecq} |
    \trpltrl{\tibranches{\tibranch{q_1}{\psi}}{\ecq/\psi'}{\tibranch{q_2}{\psi''}}}
\end{align}


\subsection{Canonical Trail Context}

\begin{align}
  \canontrlctx{\hole} &\triangleq \hole \\
  \canontrlctx{\apptrm{\ecf}{M}} &\triangleq \apptrl{\ecq_{\ecf}}{\rcd{M}} \\
  \canontrlctx{\apptrm{V}{\ecf}} &\triangleq \apptrl{\rcd{V}}{\ecq_{\ecf}} \\
  \canontrlctx{\lettrm{u}{\ecf}{M}} &\triangleq \lettrl{u}{\ecq_{\ecf}}{\rcd{M}} \\
  \canontrlctx{\titrm{\tibranches{\tibranch{V}{\psi}}{\ecf/\psi'}{\tibranch{N}{\psi''}}}}
    &\triangleq \trpltrl{\tibranches{\tibranch{\rcd{V}}{\psi}}{\ecq_{\ecf}/\psi'}{\tibranch{\rcd{N}}{\psi''}}}
\end{align}

\subsection{Term Reduction}

\[
  \reduce{B-\beta}
  {}
  {
    \sstep{
      \bangtrm{q}{\inecf{\apptrm{\lamtrm{a}{M}}{V}}}
    }
    {
      \bangtrm{\ttrl{q}{\inecq{\batrl{a}{\cd{M}}{\cd{V}}}}}{\inecf{M[V/a]}}
    }
  }
\]

\[
  \reduce{B-BANG}
  {
    \sstep{M}{N}
  }
  {
    \sstep{\bangtrm{q}{\inecf{M}}}{\bangtrm{q}{\inecf{N}}}
  }
\]

\[
  \reduce{B-\betabox}
  {
    q_f = \ttrl{q}{\inecq{\ttrl{\bbtrl{u}{\src{q'}}{\cd{N}}}{\trlsubfull{N}{V}{q'}{\src{q'}}{u}}}}
  }
  {
    \sstep{
      \bangtrm{q}{\inecf{\lettrm{u}{\bangtrm{q'}{V}}{N}}}
    }
    {
      \bangtrm{q_f}{\inecf{\trmsubfull{N}{V}{q'}{\src{q'}}{u}}}
    }
  }
\]

\[
  \reduce {B-TI}
  {}
  {
    \sstep{
      \bangtrm{q}{\inecf{\titrm{\trmbranches}}}
    }
    {
      \bangtrm{\ttrl{q}{\inecq{\titrl{q}{\cd{\trmbranches}}}}}{\inecf{q\trmbranches}}
    }
  }
\]

\end{document}
