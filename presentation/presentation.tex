\documentclass[table]{beamer}
\usepackage[table,x11names]{xcolor}
\usepackage{fancyhdr}
\usepackage{graphicx, amssymb, changepage}
\usepackage{rotating}
\usepackage{setspace}
\usepackage{newverbs}
\usepackage{array}
\usepackage{lstlinebgrd}
\usepackage{../preamble}

\makeatletter
\newcount\bt@rangea
\newcount\bt@rangeb

\newcommand\btIfInRange[2]{%
    \global\let\bt@inrange\@secondoftwo%
    \edef\bt@rangelist{#2}%
    \foreach \range in \bt@rangelist {%
        \afterassignment\bt@getrangeb%
        \bt@rangea=0\range\relax%
        \pgfmathtruncatemacro\result{ ( #1 >= \bt@rangea) && (#1 <= \bt@rangeb) }%
        \ifnum\result=1\relax%
            \breakforeach%
            \global\let\bt@inrange\@firstoftwo%
        \fi%
    }%
    \bt@inrange%
}
\newcommand\bt@getrangeb{%
    \@ifnextchar\relax%
        {\bt@rangeb=\bt@rangea}%
        {\@getrangeb}%
}
\def\@getrangeb-#1\relax{%
    \ifx\relax#1\relax%
        \bt@rangeb=100000%   \maxdimen is too large for pgfmath
    \else%
        \bt@rangeb=#1\relax%
    \fi%
}

\newcommand<>{\btLstHL}[1]{%
  \only#2{\btIfInRange{\value{lstnumber}}{#1}{\color{orange!30}\def\lst@linebgrdcmd{\color@block}}{\def\lst@linebgrdcmd####1####2####3{}}}%
}%

\lstset{
  language=Caml,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\color{javapurple}\bfseries,
  stringstyle=\color{javared},
  commentstyle=\color{javagreen},
  morecomment=[s][\color{javadocblue}]{/**}{*/},
  numberstyle=\tiny\color{black},
  stepnumber=2,
  numbersep=10pt,
  tabsize=4,
  showspaces=false,
  showstringspaces=false,
  %    frame=single,
  backgroundcolor=\color{light-gray}
}

\newverbcommand{\bverb}
  {\begin{lrbox}{\verbbox}}
  {\end{lrbox}\colorbox{gray!30}{\box\verbbox}}

\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}}
}

\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\green}[1]{\textcolor{brown}{#1}}
\newcommand{\orange}[1]{\textcolor{orange}{#1}}

\beamertemplatenavigationsymbolsempty
\setbeamertemplate{footline}[frame number]

\setlist[itemize,1]{label={$\bullet$}}
\setlist[itemize,2]{label={$\bullet$}}

\title{Nancy}
\subtitle{A History-Aware Programming Language}
\author{Jason Gardella}

\begin{document}
\frame{\titlepage}

\begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents
\end{frame}

\section{Intro}
\begin{frame}
\frametitle{What is Nancy?}
\begin{itemize}
  \item Nancy is a typed, \blue{history-aware} programming language.
  \item As a program is reduced, a \blue{trail} describing reduction is maintained.
  \item This trail can be \blue{inspected} in the program to produce a value.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Why History?}
  \begin{itemize}
    \item A history-aware language offers a new approach to certain problems
    \item Possible applications:
      \begin{itemize}
        \item Security (access control)
        \item Machine learning insights
        \item Time-travel debugging
        \item Trace integrity (garbled circuits)
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The Curry-Howard Correspondence}
  \centering
  \footnotesize
	\begin{tabular}{| >{\centering\arraybackslash}m{5cm} | >{\centering\arraybackslash}m{5cm} |}
    \hline
    \textbf{Formal Logic} & \textbf{Programming Languages} \\[5pt]
    \hline
    Natural Deduction & Typed Lambda Calculus \\[5pt]
    \hline
    Type Schemes & ML Type System \\[5pt]
    \hline
    System F & Polymorphic Lambda Calculus \\[5pt]
    \hline
    Modal Logic & Monads \\[5pt]
    \hline
    Classical-Intuitionistic Embedding & Continuation Passing Style \\[5pt]
    \hline
    \rowcolor{yellow}
    Justification Logic & $\lamh$ \\[5pt]
    \hline
  \end{tabular}
  \normalsize
\end{frame}

\subsection{Motivating Examples}
\begin{frame}[fragile]
  \frametitle{A Simple Example}
  \begin{lstlisting}
!((fun (x:int) -> (x+1)) 1)
  \end{lstlisting}

  Result: $!_q 2$ where $q$ is the trail:

  \begin{alignat*}{2}
  &\red{r}(\texttt{(((fun (x:int) -> (x + 1)) 1))});                   \quad && \textrm{(witness to initial term)} \\
  &app(\red{r}(\texttt{fun (x:int) -> (x + 1)}), \red{r}(\texttt{1})); \quad && \textrm{(app of lambda to \texttt{1})}\\
  &ba(\texttt{x:int}, \texttt{1}, \texttt{fun (x:int) -> (x + 1)});    \quad && \textrm{(beta-step)} \\
  &plus(\red{r}(\texttt{1}), \red{r}(\texttt{1}))                      \quad && \textrm{(addition of \texttt{1} to \texttt{1})}
  \end{alignat*}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Audited Composition Example}
  \begin{lstlisting}
!let! (u:int) = !(1+1),
      (v:int) = !(2+3)
in <u> + <v>
  \end{lstlisting}

  Result: $!_q 7$ where $q$ encodes both the reduction that led to the values \verb|u|
  and \verb|v|, and the reduction that took place to combine them.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Inspection Example}
\begin{lstlisting}[basicstyle=\tiny\ttfamily,
    linebackgroundcolor={%
      \btLstHL<1>{1}%
      \btLstHL<2>{2}%
      \btLstHL<3>{12}%
      \btLstHL<4>{30}%
}]
!let! (u:int) = !((fun (x:int) -> x) 1)
 in let (numApps:int) = inspect {
    r -> 0
    t -> fun (t1:int) (t2:int) ->
      t1 + t2
    ba -> 0
    bb -> 0
    ti -> 0
    lam -> fun (bodyTrl:int) ->
      bodyTrl
    app -> fun (lamTrl:int) (argTrl:int) ->
      lamTrl + argTrl + 1
    pls -> fun (lTrl:int) (rTrl:int) ->
      lTrl + rTrl
    eq -> fun (lTrl:int) (rTrl:int) ->
      lTrl + rTrl
    ite -> fun (condTrl:int) (ifTrl:int) (elseTrl:int) ->
      condTrl + ifTrl + elseTrl
    alet -> fun (argTrl:int) (bodyTrl:int) ->
      argTrl + bodyTrl
    trpl -> fun (rTrl:int) (tTrl:int) (baTrl:int)
                (bbTrl:int) (tiTrl:int) (lamTrl:int)
                (appTrl:int) (plsTrl:int) (eqTrl:int)
                (iteTrl:int) (aletTrl:int) (trplTrl:int) ->
      rTrl + tTrl + baTrl +
      bbTrl + lamTrl + appTrl +
      plsTrl + eqTrl + iteTrl +
      aletTrl + trplTrl
  } in
    if numApps == 0
    then 0
    else 1
\end{lstlisting}
\end{frame}

\AtBeginSection[]{
	 \begin{frame}<beamer>
	 \tableofcontents[currentsection]
	 \end{frame}
}

\section{Language Specification}

\subsection{Syntax}
\begin{frame}
  \frametitle{Nancy Syntax - Terms}
  \begin{alignat*}{4}
    &\textrm{Terms:} &&   && \\
    &M, N, \ldots ::=&&\  &&\vartrm{a}            &&\textrm{simple variable}\\
    &                &&|\ &&\avartrm{u}           &&\textrm{audited variable}\\
    &                &&|\ &&\lamtrm{a}{M}         &&\textrm{abstraction}\\
    &                &&|\ &&\apptrm{M}{N}         &&\textrm{application}\\
    &                &&|\ &&\bangtrm{q}{M}        &&\textrm{audited unit}\\
    &                &&|\ &&\lettrm{u}{M}{N}      &&\textrm{audited composition}\\
    &                &&|\ &&\titrm{\trmbranches}  &&\textrm{trail inspection}
  \end{alignat*}
\end{frame}

\begin{frame}
  \frametitle{Nancy Syntax - Witnesses}
  \begin{alignat*}{4}
    &\textrm{Witnesses:}\\
    &s, t, \ldots ::=&&\  &&\varwit{a}           \qquad &&\textrm{simple variable}\\
    &                &&|\ &&\avarwit{u}          \qquad &&\textrm{audited variable}\\
    &                &&|\ &&\lamwit{a}{s}        \qquad &&\textrm{abstraction}\\
    &                &&|\ &&\appwit{s}{t}        \qquad &&\textrm{application}\\
    &                &&|\ &&\bangwit{s}          \qquad &&\textrm{audited unit}\\
    &                &&|\ &&\letwit{u}{s}{t}     \qquad &&\textrm{audited composition}\\
    &                &&|\ &&\tiwit{\witbranches} \qquad &&\textrm{trail inspection}
  \end{alignat*}
\end{frame}

\begin{frame}
  \frametitle{Nancy Syntax - Trails}
  \begin{alignat*}{3}
    &\textrm{Trail Const}&&\textrm{ructors:}           \quad&&\textrm{Trail Inspection Branch Labels:}\\
    &q, q', \ldots ::=&&\ \ \rtrl{s}                   \quad&&\psi, \psi', \ldots ::=\rbrch\ |\ \tbrch\ |\ \betabrch \\
    &                &&|\ \ttrllong{q}{q'}             \quad&&\quad\quad\quad\quad\quad\ \ \betaboxbrch\ |\ \tibrch\ |\ \lambrch \\
    &                &&|\ \batrl{a}{s}{t}              \quad&&\quad\quad\quad\quad\quad\ \ \appbrch\ |\ \letbrch\ |\ \trplbrch \\
    &                &&|\ \bbtrl{u}{s}{t} \\
    &                &&|\ \lamtrl{a}{q} \\
    &                &&|\ \apptrl{q}{q'} \\
    &                &&|\ \lettrl{u}{q}{q'} \\
    &                &&|\ \titrl{q}{\witbranches} \\
    &                &&|\ \trpltrl{\trlbranches} \\
  \end{alignat*}
  \normalsize
\end{frame}

\subsection{Auxiliary Functions}
\begin{frame}
  \frametitle{Witness Substitution}
  \begin{itemize}
    \item Simple Variables \\
      For simple variables, recurses over the structure of a witness $r$, replacing all
      occurrences of simple variable $a$ with witness $t$:\\
      \footnotesize
      \vspace{.5cm}
      \begin{align*}
        \appwit{\texttt{(fun (x:int) -> x)}}{\texttt{\red{a}}} &\xrightarrow{\text{1/a}} \appwit{\texttt{(fun (x:int) -> x)}}{\texttt{\red{1}}}\\
      \end{align*}
  \normalsize
    \item Audited Variables \\
      For audited variables, recurses over the structure of a witness $r$,
      replacing all occurrences of audited variable $u$ with witness $t$:\\
      \footnotesize
      \vspace{.5cm}
      \begin{align*}
        \appwit{\texttt{(fun (x:int) -> x)}}{\texttt{\red{u}}} &\xrightarrow{\text{1/u}} \appwit{\texttt{(fun (x:int) -> x)}}{\texttt{\red{1}}}\\
        \bangwit{\texttt{\red{u}}} &\xrightarrow{\text{1/u}} \bangwit{\texttt{\red{1}}}
      \end{align*}
  \normalsize
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Audited Variable Substitution}
  An audited variable substitution recurses over a term, replacing occurrences of
  an audited variable $u$ with another term. It also updates any trails with the history
  of the audited term.

  \footnotesize
  \vspace{.5cm}
  \hspace*{5pt}
  \[
    \texttt{!\red{q}((fun (x:int) -> x) u)}
    \xrightarrow{\text{1/u}} \texttt{!\red{q'}((fun (x:int) -> x) 1)}
  \]
  \normalsize
\end{frame}

\subsection{Type System}
\begin{frame}
  \frametitle{Nancy Types}
  \begin{alignat*}{2}
    A, B, \ldots ::=&\ P              \quad &&\textrm{(base types)}\\
                   |&\ \lamtyp{A}{B}  \quad &&\textrm{(functions)}\\
                   |&\ \bangtyp{s}{A} \quad &&\textrm{(audited type)}
  \end{alignat*}
\end{frame}

\begin{frame}
\frametitle{Some Typing Rules}
\[
  \reduce{T-APP}
  {
    \begin{gathered}
      \withenvs{\wenv}{\tenv}{\trmtypwit{M}{\lamtyp{A}{B}}{\red{s}}} \\
      \withenvs{\wenv}{\tenv}{\trmtypwit{N}{A}{\blue{t}}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\apptrm{M}{N}}{B}{\appwit{\red{s}}{\blue{t}}}}
  }
\]

\vspace{0.5in}

\[
  \reduce{T-BANG}
  {
    \begin{gathered}
      \withenvs{\wenv}{\noenv}{\trmtypwit{M}{A}{\red{s}}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\bangtrm{q}{M}}{\bangtyp{s}{A}}{\bangwit{\red{s}}}}
  }
\]
\end{frame}

\subsection{Operational Semantics}

\begin{frame}
  \frametitle{Beta-Step Operational Semantics}
  A rule $\bstep{M}{q_1}{N}{q_2}$ is read as ``term $M$ under trail $q_1$ reduces with trail $q_2$ to term $N$''

\[
  \reduce{B-$\beta$}
  {
    \begin{gathered}
      \bstep{M}{\red{q_1}}{\lamtrm{a}{M'}}{\blue{q_2}} \\
      \bstep{N}{\ttrl{\red{q_1}}{\apptrl{\blue{q_2}}{\rcd{N}}}}{V}{\green{q_3}} \\
      \bstep{\appsub{M'}{V}{a}}{\ttrl{\red{q_1}}{\ttrl{\apptrl{\blue{q_2}}{\green{q_3}}}{\batrl{a}{\tgt{\blue{q_2}}}{\tgt{\green{q_3}}}}}}{W}{\orange{q_4}}
    \end{gathered}
  }
  {
    \bstep{\apptrm{M}{N}}{\red{q_1}}{W}{\ttrl{\apptrl{\blue{q_2}}{\green{q_3}}}{\ttrl{\batrl{a}{\tgt{\blue{q_2}}}{\tgt{\green{q_3}}}}{\orange{q_4}}}}
  }
\]

\vspace{0.5in}

The target of a trail, $\red{q^t}$, is the final term of that trail, after all reduction has finished.
\end{frame}

\begin{frame}
\frametitle{Audited Unit Operational Semantics}
\[
  \reduce{B-BANG}
    {\bstep{M}{\red{q_1}}{V}{\blue{q_3}}}
    {\bstep{\bangtrm{\red{q_1}}{M}}{q_2}{\bangtrm{\ttrl{\red{q_1}}{\blue{q_3}}}{V}}{\rtrl{\bangwit{\cd{M}}}}}
\]
\end{frame}

\section{Implementation}
\begin{frame}[fragile]
  \frametitle{Nancy Pipeline}
  \begin{tikzpicture}[scale=0.8]
    \node (LexerIn) at (-1, 2) {Program};
    \node (Lexer) at (1, 2) [rectangle,draw] {Lexer};
    \node (Parser) at (4, 2) [rectangle,draw] {Parser};
    \node (Interpreter) at (7, 0) [rectangle,draw] {Interpreter};
    \node (Typechecker) at (7, 4) [rectangle,draw] {Typechecker};
    \node (Value) at (10, 0) {Value};
    \node (Type) at (10, 4) {Type};

    \draw[->] (LexerIn) -- (Lexer);
    \draw[->] (Lexer) --node[above]{Tokens} (Parser);
    \draw (Parser) -- (4, 0) --node[above]{AST} (Interpreter);
    \draw (Parser) -- (4, 4) --node[above]{AST} (Typechecker);
    \draw[->] (Typechecker) -- (Type);
    \draw[->] (Interpreter) -- (Value);
  \end{tikzpicture}
\end{frame}

\subsection{Technology}
\begin{frame}
  \frametitle{Technology}
  \begin{itemize}
    \item Haskell
    \item Alex - Haskell Lexer Generator
    \item Happy - Haskell Parser Generator
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Monads}
  The Nancy typechecker and interpreter make use of Haskell's support for Monads and Monad Transformers,
  using the Reader monad for storing typing environments and trails, the Writer monad for logging, and
  the Except monad for handling errors.

\begin{lstlisting}[language=Haskell,basicstyle=\scriptsize\ttfamily,
    linebackgroundcolor={%
      \btLstHL<1>{2}%
      \btLstHL<2>{4}%
      \btLstHL<3>{6}%
      \btLstHL<4>{8}%
      \btLstHL<5>{10-11}%
  }]
typecheckTerm (App left right) = do
  (leftType, leftWit) <- typecheckTerm left
  case leftType of
    (L.LamType argType returnType) -> do
      (rightType, rightWit) <- typecheckTerm right
      if rightType == argType
      then return (returnType, L.App leftWit rightWit)
      else throwError
        (Err.InvalidArgType rightType argType)
    _ ->
      throwError (Err.ExpectedLam left leftType)
\end{lstlisting}

\begin{lstlisting}[language=Haskell,basicstyle=\scriptsize\ttfamily,
    linebackgroundcolor={%
      \btLstHL<6>{3}%
  }]
typecheckTerm (Bang body _) = do
  (bodyType, bodyWit) <-
    local
      (updateTruthEnv $ const E.empty) (typecheckTerm body)
  return
    (L.BangType bodyType bodyWit, L.Bang bodyWit L.NoTrail)
\end{lstlisting}

\end{frame}

\subsection{Language Extensions}
\begin{frame}
  \frametitle{Language Extensions}
  Nancy has some simple extensions on top of the previously defined syntax
  to enable writing more complex programs. These are:

  \begin{itemize}
    \item Base types: natural numbers and booleans
    \item Plus term: adds two numbers
    \item Equality term: checks if two numbers/booleans are equal
    \item If-then-else term: control flow
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Derived Forms}
\scriptsize
\begin{tabular}{|p{5cm}|p{5cm}|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Derived Form}}
    &\multicolumn{1}{c|}{\textbf{Parsed Equivalent}} \\ \hline
    \begin{lstlisting}[style=table]
fun (x:int) (y:int) ->
  x + y
    \end{lstlisting}&
    \begin{lstlisting}[style=table]
fun (x:int) ->
  fun (y:int) ->
    x + y
    \end{lstlisting} \\ \hline
    \begin{lstlisting}[style=table]
let (x:int) = 1 in x
    \end{lstlisting}&
    \begin{lstlisting}[style=table]
(fun (x:int) -> x) 1
    \end{lstlisting} \\ \hline
    \begin{lstlisting}[style=table]
let (x:int) = 1,
    (y:int) = 2
in x + y
    \end{lstlisting}&
    \begin{lstlisting}[style=table]
let (x:int) = 1
in let (y:int) = 2
   in x + y
    \end{lstlisting} \\ \hline
    \begin{lstlisting}[style=table]
let! (x:int) = !1,
     (y:int) = !2
in <x> + <y>
    \end{lstlisting}&
    \begin{lstlisting}[style=table]
let! (x:int) = !1
in let! (y:int) = !2
   in <x> + <y>
    \end{lstlisting} \\ \hline
\end{tabular}
\normalsize
\end{frame}

\subsection{Implementing a History-Aware Language}
\begin{frame}
  \frametitle{Trail Management}
  \begin{itemize}
    \item As reduction proceeds, the trail describing reduction necessarily grows.
    \item This leads to large memory overhead and also results in a complicated trail
      which is difficult to understand and work with in a useful way.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Prose Trails}
  To help understand trails, we can print them in more easily understandable way:

  \scriptsize
  \begin{alltt}
  We witness \verb|(((fun (x:int) -> (x + 1)) 1))|
  We reduce under an application, reducing the left term:
    We witness \verb|fun (x:int) -> (x + 1)|
  and the right term:
    We witness \verb|1|
  We perform a beta step, substituting the formal parameter
    \verb|(x:int)| with \verb|1| in \verb|fun (x:int) -> (x + 1)|
  We reduce under a plus, reducing the left term:
    We witness \verb|1|
  and the right term:
    We witness \verb|1|
  \end{alltt}
  \normalsize
\end{frame}

\begin{frame}[fragile]
  \frametitle{Non-audited Terms}
  \begin{itemize}
    \item All of the previous examples have been wrapped in a top-level ``bang'' term which captures the trail.
    \item How should we deal with terms which are not enclosed in a bang, e.g. \lstinline{(fun (x:int) -> x) 1}?
    \item The answer is simple: in a history-aware language, these terms simply do not make sense.
      All terms must has be enclosed in some ``bang'' term to capture history.
    \item At parsing, Nancy wraps any non-bang terms at the top level in a new bang-term; the above
      example would be parsed as \lstinline{!(fun (x:int) -> x) 1}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Substitution \& Environments}
  \begin{itemize}
    \item There are two main ways to handle variable resolution in an interpreter: substitution and environments
    \item Typically these are trivially interchangable, but environments are typically simpler for interpreters
    \item Nancy uses substitution
    \item Audited variables have history, so an environment for audited variables is non-trivial
  \begin{lstlisting}
!let! (u:int) = !(1+1),
      (v:int) = !(2+3)
in <u> + <v>
  \end{lstlisting}
  \item Such ``audited environments'' would need to somehow store the history associated to these variables, so that
    when they are looked up their history is reflected in the trail.

  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Witnesses \& Terms}
  \begin{itemize}
    \item As is made clear by Nancy's syntax, witnesses and terms are almost exactly the same
    \item The only difference is that bang witnesses do not have a trail, while bang terms do
    \item In the Nancy language, there is a type constructor \bverb|(Expr a)| in which the type variable \bverb|a| represents
      the type of the trail of the bang term.
    \item Terms are simply a type alias to this expression type \bverb|(Expr Trail)|
    \item Witnesses are a type alias \bverb|(Expr NoTrail)|
    \item This greatly simplifies implementation, reducing two types with the same structure to one
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Evaluation Strategy}
  \begin{itemize}
    \item Evaluation strategy naturally has an effect on what trails are produced
    \item Nancy is call-by-value
    \item A call-by-need evaluation strategy introduces problems when reducing the branches
      of a trail inspection
  \end{itemize}

  \begin{lstlisting}
...
inspect {
    ...
    app -> M
    ...
    eq -> ((fun (x:int) ->
            fun (lTrl:int) (rTrl:int) ->
              lTrl + rTrl) 1)
    ...
  \end{lstlisting}
\end{frame}

\section{Conclusion}

\subsection{Related Work}
\begin{frame}
\frametitle{Related Work}
\begin{itemize}
  \item Foundations of Nancy
    \begin{itemize}
      \item $\lamh$~\cite{lamh}
      \item $\lamhc$~\cite{lamhc}
    \end{itemize}
  \item Transparent ML~\cite{tml}
\end{itemize}
\end{frame}

\subsection{Future Work}
\begin{frame}
  \frametitle{Future Work}
  \begin{itemize}
    \item Nancy is a very simple language, but it demonstrates some problems with implementing a history-aware language
    \item We believe the problems demonstrated by Nancy must be solved before a more fully featured language
      could prove useful, as they are fundamental to language-level provenance
      \begin{itemize}
        \item Call-by-need evaluation strategy non-trivial
        \item Trails quickly become so complex that they are difficult to work with. A better strategy
          for querying trails is needed, perhaps based on temporal logic formulas
        \item Trails cause constantly increasing memory usage
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}Thank You\par%
  \end{beamercolorbox}
  \vfill
\end{frame}

\backupbegin
\appendix
\begin{frame}
  \frametitle{Witness Substitution}
  For $\gamma = \sub{t}{a}$, substitution of simple variables on a witness $r$,
  denoted as $r\gamma$, is defined as:

  \begin{align*}
    \witsubsimplerule{\varwit{c}}{\begin{cases}
      t  &(a = c) \\
      \varwit{c} &(a \neq c)
    \end{cases}} \\
    \witsubsimplerule{\bangwit{s}}{\bangwit{s}} \\
  \end{align*}

  For $\delta = \sub{t}{u}$, substitution of audited variables on a witness $r$,
  denoted as $r\delta$, is defined as:

  \begin{align*}
    \witsubauditedrule{\varwit{a}}{\varwit{a}} \\
    \witsubauditedrule{\avarwit{v}}{\begin{cases}
      t  &(u = v) \\
      \varwit{v} &(u \neq v)
    \end{cases}} \\
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Audited Variable Substitution}

  \[
    \delta = [(N, q, t)/u],\ \text{fix}\ \delta' = [t/u]
  \]

  \begin{columns}
    \begin{column}{0.5\textwidth}
      \centering
      \begin{figure}
        \begin{align*}
          \trmsubrule{\avartrm{v}}{\begin{cases}
            N           &(u = v) \\
            \avartrm{v} &(u \neq v)
          \end{cases}} \\
          \trmsubrule{(\bangtrm{q'}{R})}{\bangtrm{\ttrl{q'\delta'}{\trlsub{R}}}{(\trmsub{R})}} \\
        \end{align*}
        \caption{Term Substitution}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \centering
      \begin{figure}
        \begin{align*}
          \trlsubrule{\avartrm{v}}{\begin{cases}
            q  &(u = v) \\
            \rcd{v} &(u \neq v)
          \end{cases}} \\
          \trlsubrule{\bangtrm{q'}{R}}{\rtrl{\bangwit{\src{q'}\delta'}}} \\
        \end{align*}
        \caption{Trail Substitution}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Compute Witness}

  Converts a term to a witness. Trivial in all cases except the bang case.

  \begin{align*}
    \cdrule{\bangtrm{q}{N}}{\bangwit{\src{q}}}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Source}
  Trails represent how one term reduces to another term. With this in mind, the source of a trail
  is the initial term of that trail, before any reduction took place.

  \begin{align*}
    \srcrule{\rtrl{s}}{s} \\
    \srcrule{\ttrllong{q_1}{q_2}}{\src{q_1}} \\
    \srcrule{\batrl{a}{s_1}{s_2}}{\appwit{\lamwit{a}{s_1}}{s_2}} \\
    \srcrule{\bbtrl{v}{s_1}{s_2}}{\letwit{v}{\bangwit{s_1}}{s_2}} \\
    \srcrule{\titrl{q_1}{\witbranches}}{\tiwit{\witbranches}} \\
    \srcrule{\lamtrl{a}{q_1}}{\lamwit{a}{\src{q_1}}} \\
    \srcrule{\apptrl{q_1}{q_2}}{\appwit{\src{q_1}}{\src{q_2}}} \\
    \srcrule{\lettrl{v}{q_1}{q_2}}{\letwit{v}{\src{q_1}}{\src{q_2}}} \\
    \srcrule{\trpltrl{\trlbranches}}{\tiwit{\src{\trlbranches}}}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Target}
  The target of a trail is the final term of that trail, after all reduction has finished.

  \begin{align*}
    \tgtrule{\rtrl{s}}{s} \\
    \tgtrule{\ttrllong{q_1}{q_2}}{\tgt{q_2}} \\
    \tgtrule{\batrl{a}{s_1}{s_2}}{\appsub{s_1}{s_2}{a}} \\
    \tgtrule{\bbtrl{v}{s_1}{s_2}}{\appsub{s_2}{s_1}{v}} \\
    \tgtrule{\titrl{q_1}{\witbranches}}{q_1 \witbranches} \\
    \tgtrule{\lamtrl{a}{q_1}}{\lamwit{a}{\tgt{q_1}}} \\
    \tgtrule{\apptrl{q_1}{q_2}}{\appwit{\tgt{q_1}}{\tgt{q_2}}} \\
    \tgtrule{\lettrl{v}{q_1}{q_2}}{\letwit{v}{\tgt{q_1}}{\tgt{q_2}}} \\
    \tgtrule{\trpltrl{\trlbranches}}{\tiwit{\tgt{\trlbranches}}}
  \end{align*}
\end{frame}

\begin{frame}
\frametitle{Typing Rules I}
\[
  \reduce{T-VAR}
  {
    \intenv{\trmtyp{\vartrm{a}}{A}}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\vartrm{a}}{A}{\red{\varwit{a}}}}
  }
\]

\[
  \reduce{T-ABS}
  {
    \withenvs{\wenv}{\tenv, \trmtyp{\vartrm{a}}{A}}{\trmtypwit{M}{B}{\red{s}}}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\lamtrm{a}{M}}{\lamtyp{A}{B}}{\lamwit{a}{\red{s}}}}
  }
\]

\[
  \reduce{T-APP}
  {
    \begin{gathered}
      \withenvs{\wenv}{\tenv}{\trmtypwit{M}{\lamtyp{A}{B}}{\red{s}}} \\
      \withenvs{\wenv}{\tenv}{\trmtypwit{N}{A}{\blue{t}}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\apptrm{M}{N}}{B}{\appwit{\red{s}}{\blue{t}}}}
  }
\]

\[
  \reduce{T-AVAR}
  {
    \inwenv{\avartrm{u} :: A}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\avartrm{u}}{A}{\red{\avarwit{u}}}}
  }
\]
\end{frame}

\begin{frame}
\frametitle{Typing Rules II}
\[
  \reduce{T-BANG}
  {
    \begin{gathered}
      \withenvs{\wenv}{\noenv}{\trmtypwit{M}{A}{\red{s}}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\bangtrm{q}{M}}{\bangtyp{s}{A}}{\bangwit{\red{s}}}}
  }
\]

\[
  \reduce{T-LET}
  {
    \begin{gathered}
      \withenvs{\wenv}{\tenv}{\trmtypwit{M}{\bangtyp{\orange{r}}{A}}{\red{s}}} \\
      \withenvs{\wenv, \green{u} :: A}{\tenv}{\trmtypwit{N}{C}{\blue{t}}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{
      \trmtypwit{\lettrm{\green{u}}{M}{N}}
        {\appsub{C}{\orange{r}}{\green{u}}}
        {\letwit{\green{u}}{\red{s}}{\blue{t}}}
    }
  }
\]

\[
  \reduce{T-TI}
  {
    \begin{gathered}
      \withenvs{\wenv}{\noenv}{\trmtypwit{\trmbranches(\psi)}{\mathcal{T}^B(\psi)}{\red{\witbranches(\psi)}}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\titrm{\trmbranches}}{B}{\tiwit{\red{\witbranches}}}}
  }
\]
\end{frame}

\begin{frame}
  \frametitle{Semantics I}
\[
  \reduce{B-V}
    {}
    {\bstep{V}{\red{q}}{V}{\red{\rcd{V}}}}
\]

\[
  \reduce{B-BANG}
    {\bstep{M}{\red{q_1}}{V}{\blue{q_3}}}
    {\bstep{\bangtrm{\red{q_1}}{M}}{q_2}{\bangtrm{\ttrl{\red{q_1}}{\blue{q_3}}}{V}}{\rtrl{\bangwit{\cd{M}}}}}
\]

\[
  \reduce{B-$\beta$}
  {
    \begin{gathered}
      \bstep{M}{\red{q_1}}{\lamtrm{a}{M'}}{\blue{q_2}} \\
      \bstep{N}{\ttrl{\red{q_1}}{\apptrl{\blue{q_2}}{\rcd{N}}}}{V}{\green{q_3}} \\
      \bstep{\appsub{M'}{V}{a}}{\ttrl{\red{q_1}}{\ttrl{\apptrl{\blue{q_2}}{\green{q_3}}}{\batrl{a}{\tgt{\blue{q_2}}}{\tgt{\green{q_3}}}}}}{W}{\orange{q_4}}
    \end{gathered}
  }
  {
    \bstep{\apptrm{M}{N}}{\red{q_1}}{W}{\ttrl{\apptrl{\blue{q_2}}{\green{q_3}}}{\ttrl{\batrl{a}{\tgt{\blue{q_2}}}{\tgt{\green{q_3}}}}{\orange{q_4}}}}
  }
\]
\end{frame}

\begin{frame}
  \frametitle{Semantics II}
\[
  \reduce{B-$\betabox$}
  {
    \begin{gathered}
      \bstep{M}{\red{q_1}}{\bangtrm{\blue{q_2}}{V}}{\green{q_3}} \\
      \delta = [(V, \blue{q_2}, \src{\blue{q_2}})/u] \\
    \bstep{\trmsub{N}}{\ttrl{\red{q_1}}{\ttrl{\lettrl{u}{\green{q_3}}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{\blue{q_2}}}{\cd{N}}}{\trlsub{N}}}}}{W}{\orange{q_4}}
    \end{gathered}
  }
  {
    \bstep{\lettrm{u}{M}{N}}{\red{q_1}}{W}{\ttrl{\lettrl{u}{\green{q_3}}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{\blue{q_2}}}{\cd{N}}}{\ttrl{\trlsub{N}}{\orange{q_4}}}}}
  }
\]

\[
  \reduce{B-TRPL}
  {
    \begin{gathered}
      \bstep{\trmbranches_{r}}{\red{q_1}}{\trmbranches_{r}^{V}}{\blue{q_r}} \\
      \bstep{\trmbranches_{t}}{\ttrl{\red{q_1}}{\trpltrl{\blue{q_r}, \rcd{\trmbranches_{t}}, \ldots}}}{\trmbranches_{t}^{V}}{\green{q_t}} \\
      \ldots \\
      \bstep{\trmbranches_{trpl}}{\ttrl{\red{q_1}}{\trpltrl{\blue{q_r}, \green{q_t}, \ldots, \rcd{\trmbranches_{trpl}}}}}{\trmbranches_{trpl}^{V}}{\orange{q_{trpl}}}
    \end{gathered}
  }
  {
    \bstep{\trmbranches}{\red{q_1}}{\trmbranches^{V}}{\trpltrl{\blue{q_r}, \green{q_t}, \ldots, \orange{q_{trpl}}}}
  }
\]

\[
  \reduce{B-TI}
  {
    \begin{gathered}
      \bstep{\trmbranches}{\red{q_1}}{\trmbranches^{V}}{\blue{q_{\trmbranches}}} \\
      \bstep{(\ttrl{\red{q_1}}{\blue{q_{\trmbranches}}})\trmbranches}{\ttrl{\red{q_1}}{\ttrl{\blue{q_{\trmbranches}}}{\titrl{\ttrl{\red{q_1}}{\blue{q_{\trmbranches}}}}{\cd{\trmbranches}}}}}{W}{\green{q_2}}
    \end{gathered}
  }
  {
    \bstep{\titrm{\trmbranches}}{\red{q_1}}{W}{\ttrl{\blue{q_{\trmbranches}}}{\ttrl{\titrl{\ttrl{\red{q_1}}{\blue{q_{\trmbranches}}}}{\cd{\trmbranches}}}{\green{q_2}}}}
  }
\]

\end{frame}


\begin{frame}[allowframebreaks]
\frametitle{Bibliography}
\bibliographystyle{alpha}
\bibliography{references}
\end{frame}

\backupend

\end{document}
