\chapter{Introduction}
Provenance is of increasing importance in the field of software; applications and users want to know
where there data came from, or how it was computed. In the field of databases, provenance has a long history, tracking
the origins of data and how it changes in the database system~\cite{provDbSurvey}. In these provenance-aware database systems,
some amount of computation tracking is possible, but it is necessarily limited by the granularity and structure
of the system in which the data is being stored. A value stored in a database which was the result of some complicated computation
likely has no information regarding that computation, as it occurred outside the provenance-aware database.
Most often, computation is specified and takes place in a programming language, but the provenance of this computation
is often lost, as programming languages are not provenance-aware. A crucial, missing piece of the provenance equation is
therefore computational provenance, which lives alongside and tracks the history of a computation.

To this end, we define Nancy, a history-aware programming language. Nancy is a toy language; it is very simple
and is merely intended to be used for exploration of the practicality of a history-aware language. By keeping Nancy simple,
we are able to better ensure that the history-aware properties of its implementation are correct.

While Nancy is very simple, it is the first step toward a fully-featured history-aware language. Such a language
may provide novel approaches to certain problems. One possible application in security is mentioned by Bavera
and Bonelli~\cite{lamh}, in the case of access control. A history-aware language may allow the implementation
at the language-level of history-based access control, in which a program's allowed level of access is determined
by what it has done previously. For example, a program which has previously called a function which is labeled with a low access level
may not be permitted to call any functions requiring a higher access level.

Another intriguing application is to time-travel debugging, which allows a programmer to debug their program by
manually stepping forward and backward in execution. The ability to debug a program by slowly stepping forward
is prevalent in debugging tools, but the ability to move backward in execution has only recently been explored.
For example, Microsoft's WinDbg tool allows developers to record the execution of a process, and then
step forward and backward in the execution of that process~\cite{windbg}. A history-aware language could possibly allow
for more advanced time-travel debugging, as history information is intrinsically available.

There are also possible applications in providing insights into the behavior of machine learning algorithms. Some machine
learning algorithms are considerably opaque, functioning essentially as a black box; they may give a correct result, but
how they arrived at that result is a mystery even to the designers of the algorithm. An implementation of such an algorithm
in a history-aware language could possibly provide insights into how the algorithm is functioning.

The remainder of this thesis proceeds as follows: in Chapter 1 we provide a review of preliminaries;
in Chapter 2 we will specify Nancy's syntax, typing rules, and semantics;
in Chapter 3 we describe the technology used to implement Nancy's typechecker and interpreter, and discuss
some interesting problems faced when implementing a history-aware language; in Chapter 5 we conclude
with an evaluation of our work, and discuss areas of possible future work.

We now provide some basic example programs, to exhibit what differentiates Nancy from other programming languages.
All examples provided through the paper are valid Nancy programs, and can be executed using the Nancy interpreter. The
Nancy interpreter is available at \url{github.com/jgardella/nancy}. Additionally, all of the examples are available in
the same repository at \verb|examples/thesis|. Steps for executing the examples or any Nancy program are provided in the linked repository.

\section{Motivating Examples}

Nancy gets its name from the fact that it supports computational \textit{provenance}; a trail of
computation is maintained as reduction proceeds, and this trail can be inspected later in the program to
produce a value. Example~\ref{ex:1} below shows a simple Nancy program.

\begin{exmp}\label{ex:1}
\begin{lstlisting}[style=example]
!((fun (x:int) -> (x+1)) 1)
\end{lstlisting}
\end{exmp}

Aside from the leading \verb|!| character, this program looks like something that could written
in the standard simply-typed lambda calculus. The \verb|!| character declares that the term it encloses
is an ``audited term''. The \verb|!| will capture a trail representing how the enclosed term is reduced.
The final result of reduction will be $!_q 2$, where $q$ is a trail representing how reduction proceeded, depicted
in Figure~\ref{fig:ex1Trail}. The result of any Nancy program will be some bang term $!_q V$, where $q$ describes the reduction
that resulted in the value $V$.

\begin{figure}[ht]
\begin{alignat*}{2}
&r(\texttt{(((fun (x:int) -> (x + 1)) 1))});                      \quad && \textrm{(witness to initial term)} \\
&app(r(\texttt{fun (x:int) -> (x + 1)}), r(\texttt{1}));          \quad && \textrm{(application of lambda to \texttt{1})}\\
&ba(\texttt{x:int}, \texttt{1}, \texttt{fun (x:int) -> (x + 1)}); \quad && \textrm{(beta-step)} \\
&plus(r(\texttt{1}), r(\texttt{1}))                               \quad && \textrm{(addition of \texttt{1} to \texttt{1})}
\end{alignat*}
\caption{Trail of Example~\ref{ex:1}}\label{fig:ex1Trail}
\end{figure}

Example~\ref{ex:2} performs two different audited computations,
then composes their results. In the following examples, we will omit the trail produced by program, as even for these simple examples trails quickly
become very verbose. The trail can be seen by running the program in the Nancy interpreter

\begin{exmp}\label{ex:2}
\begin{lstlisting}[style=example]
!let! (u:int) = !(1+1),
      (v:int) = !(2+3)
in <u> + <v>
\end{lstlisting}
\end{exmp}

Two simple addition operations are performed; these operations are wrapped in an enclosing \verb|!| (bang) expression, which
captures the trail produced by the computation performed within it. A \verb|let!| expression is used to unwrap the result of two
different audited computations, storing them in the audited variables \verb|u| and \verb|v|. The audited variables are then
used in the body of the \verb|let!|; the notation \verb|<u>| indicates that \verb|u| is an audited variable, and not a simple variable.
The final result will be $!_q 7$, where $q$ encodes both the reduction that led to the values \verb|u| and \verb|v|, plus the final
reduction which took place to combine them.

We have seen that all Nancy programs produce a bang term including a trail describing reduction, and that these
bang terms can be composed using audited lets, and their results referred to using audited variables. The final differentiating aspect of Nancy
is trail inspection, which allows the programmer to produce a value based on the current trail. We perform a trail inspection in Example~\ref{ex:3}.

\noindent\begin{minipage}{\linewidth}
\begin{exmp}\label{ex:3}
\begin{lstlisting}[style=example]
!let! (u:int) = !((fun (x:int) -> x) 1)
 in let (numApps:int) = inspect {
    r -> 0
    t -> fun (t1:int) (t2:int) ->
      t1 + t2
    ba -> 0
    bb -> 0
    ti -> 0
    lam -> fun (bodyTrl:int) ->
      bodyTrl
    app -> fun (lamTrl:int) (argTrl:int) ->
      lamTrl + argTrl + 1
    pls -> fun (lTrl:int) (rTrl:int) ->
      lTrl + rTrl
    eq -> fun (lTrl:int) (rTrl:int) ->
      lTrl + rTrl
    ite -> fun (condTrl:int)
               (ifTrl:int)
               (elseTrl:int) ->
      condTrl + ifTrl + elseTrl
    alet -> fun (argTrl:int) (bodyTrl:int) ->
      argTrl + bodyTrl
    trpl -> fun (rTrl:int) (tTrl:int)
                (baTrl:int) (bbTrl:int)
                (tiTrl:int) (lamTrl:int)
                (appTrl:int) (plsTrl:int)
                (eqTrl:int) (iteTrl:int)
                (aletTrl:int) (trplTrl:int) ->
      rTrl + tTrl + baTrl +
      bbTrl + lamTrl + appTrl +
      plsTrl + eqTrl + iteTrl +
      aletTrl + trplTrl
  } in
    if numApps == 0
    then 0
    else 1
\end{lstlisting}
\end{exmp}
\end{minipage}

We first perform a simple function application on line 1, following this we perform a trail inspection, which
counts the number of applications in the reduction history.  The inspection performs a traversal over the current trail,
replacing occurrences of the trail constructor to the left of the \verb|->| with the value to the right. Trail constructors
which have sub-trails expect their associated values in the traversal to be functions of arity equivalent to the
number of sub-trails they have. For example, the $app$ constructor has two trails, one for the reduction of
the left term (the function) and one for the reduction of the right term (the argument). As a result, the
\verb|app| case on line 10 contains a function of two arguments.

The interesting case in this inspection is the \verb|app| case 10, which returns the values from its subtrails plus 1, to count
the application which is denoted by the $app$ constructor it maps to. All of the other cases simply sum up their
subtrails, if they have subtrails, or return 0 if they are base cases, since they do not indicate application.
We use the value resulting from this inspection to influence control flow on line 30, returning \verb|1|
if there were applications, and \verb|0| if not.
Since at the time of inspection, the trail will include an application constructor from the computing
of the value for audited variable \verb|u| on line 1, the final result will be \verb|1|.

This example is very trivial, but one can imagine using inspection to produce more useful values, which
may describe something interesting about the history of computation and then be used to compute a future value
or influence control flow.

\section{State of the Art}
There are some existing proposals for adding provenance to programming languages; however, these
are generally ad-hoc additions to existing languages, or are more specialized in their scope. The
Jif programming language~\cite{jif}, for example, extends Java with labels that control information flow.
After a Jif program is verified to follow information flow restrictions, it is then transpiled to
a normal Java program. Starflow~\cite{starflow} targets Python, providing a workflow management environment
which supports tracking the provenance of scripts run to perform data analysis; this is implemented through a combination of user-provided
annotations, static analysis, and runtime analysis. These approaches all use an ad-hoc form of provenance, as provenance information
is inserted either manually or dynamically into a language which is not provenance-aware. Nancy varies from these approaches as it provides
a complete language-level implementation of computational provenance, based on a verified and sound logic through the Curry-Howard correspondence.

The only other specification for a history-aware programming language of which we are aware is
Transparent ML proposed by Acar et.\ al.~\cite{tml}. Transparent ML is a pure, higher-order functional language
which provides traces based on operational derivations. Notably, extraction functions can be applied to these traces,
producing annotated values which can encode provenance-related information. Unfortunately, an implementation of
the language is not provided.

Nancy is based strongly on Riciotti and Cheney's~\cite{lamhc} $\lamhc$, which itself is a
simplified version of Bavera and Bonelli's~\cite{lamh} $\lamh$, a computational interpretation
of Justification Logic through the Curry-Howard correspondence. Both $\lamh$ and $\lamhc$ provide
a solid theoretical system for modeling computational provenance, but they do not provide an
implementation of the system. The Nancy language and its interpreter fill this gap, taking $\lamhc$
as a starting point, modifying it to simplify implementation, and in the end providing a simple and correct
history-aware programming language which can be used to explore the practical uses of computational provenance.
