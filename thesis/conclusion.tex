\chapter{Conclusion}
We have built the Nancy typechecker and interpreter not only to explore the problems of implementing a history-aware
language, but also to explore the practical uses of such a language.
Certainly, one problem with Nancy is that it is a very simple language, missing many of the constructs which make
functional languages practically useful. We have kept Nancy simple to ensure that the provided implementation
is correct; in many cases, the addition of new terms and language constructs is trivial, as demonstrated
by the addition of the if-then-else, plus, and equality terms. However, there are other changes which would
make sense for a fully-featured language but which are non-trivial. One of these changes is in evaluation strategy; most programming
languages use a call-by-need evaluation strategy, only reducing terms if they are actually needed, but Nancy is designed to use
call-by-value, reducing terms regardless of if they are actually needed or not. However, converting Nancy to use call-by-need is
non-trivial, mainly due to the reduction of the inspection term. In essence, it is not possible to state whether a given branch is needed
and should be reduced, as the reduction of following branches may cause that branch to be needed when it previously was not.

The abridged example Nancy program in Figure~\ref{fig:callByNeed} motivates this problem. It contains an inspect term,
which is preceded by some other terms. If we assume that the current trail does not have an $app$ constructor
(i.e. the terms preceding the inspection did not contain an application), when reducing the branches
we will not reduce the \verb|app| branch's term \verb|M| on line 4 to a value, as it is not needed. However, assume the current trail does have
a $pls$ constructor; we will reduce the following \verb|pls| case on line 5, which contains an application. This will introduce an $app$ constructor
into the current trail; recall that the trail which is inspected includes the reduction performed on the inspection's branches, so
the \verb|app| case is now needed and will have to be reduced. As the trail to be inspected is affected by the reduction of the
inspection branches, we cannot know whether a branch is needed and therefore should be reduced.

\begin{figure}[ht]
\begin{lstlisting}[style=example]
...
inspect {
  ...
  app -> M
  pls -> ((fun (x:int) ->
    fun (lTrl:int) (rTrl:int) ->
      lTrl + rTrl) 1)
  ...
}
\end{lstlisting}
\caption{Call-by-need Problem}\label{fig:callByNeed}
\end{figure}

In the end, however, while Nancy is not a fully featured language, we believe it already speaks to a lot of the
problems facing a history-aware language. We believe that these problems are more pressing than the addition of
more history-aware language features, as they are fundamental to language-level provenance, and any additional
features will suffer from them as well. Nancy can record the reduction procedure in its trails, but the trails
quickly become so complex as to be difficult to work with and reason about. Either a different trail structure, or
a new way of querying trails is required to make them practically useful. Additionally, the size of the trail
continually grows, resulting in a constant increase in memory usage. The traditional call-by-need evaluation strategy for
programming languages is non-trivial to implement in Nancy due to the reduction of trail inspection branches.
Before a more practically useful, fully featured history-aware language can be developed, these problems must be addressed.
