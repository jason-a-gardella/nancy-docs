\chapter{Implementation}

\section{Technology}
The Nancy typechecker and interpreter was written using the Haskell programming language. Haskell's pure functional style
works well with building an interpreter, as it is essentially the composition of various steps
(lexing, parsing, typechecking, interpreting) to convert an input string of characters to a value. Additionally,
its strong type system gives one a great degree of confidence in the correctness of the code.

The general pipeline of evaluation a Nancy program is outlined in Figure~\ref{fig:nancyPipeline}. The lexer
is generated using Alex~\cite{alex}, a Haskell lexer generator, and the parser is generated using Happy~\cite{happy}, a Haskell
parser generator. The lexer takes a stream of characters as input (the program) and converts the
characters to tokens. The tokens are fed to the parser, which constructs an Abstract Syntax Tree (AST)
representing the program; this is a tree of terms which the typechecker and parser can easily
work with. Note that at this point the pipeline splits, representing that the typechecker does
not add any additional information to the AST that it sends to the interpreter; it passed along the same AST that it
received from the parser. Naturally, a program which does not pass typechecking should not
be interpreted, and Nancy does implement this behavior, but in fact the interpreter can receive
any AST as input, even one which is not typable, requiring no additional information from the typechecker.

\begin{figure}[ht]
\centering
\begin{tikzpicture}
  \node (LexerIn) at (-1, 2) {Program};
  \node (Lexer) at (1, 2) [rectangle,draw] {Lexer};
  \node (Parser) at (4, 2) [rectangle,draw] {Parser};
  \node (Interpreter) at (7, 0) [rectangle,draw] {Interpreter};
  \node (Typechecker) at (7, 4) [rectangle,draw] {Typechecker};
  \node (Value) at (10, 0) {Value};
  \node (Type) at (10, 4) {Type};

  \draw[->] (LexerIn) -- (Lexer);
  \draw[->] (Lexer) --node[above]{Tokens} (Parser);
  \draw (Parser) -- (4, 0) --node[above]{AST} (Interpreter);
  \draw (Parser) -- (4, 4) --node[above]{AST} (Typechecker);
  \draw[->] (Typechecker) -- (Type);
  \draw[->] (Interpreter) -- (Value);
\end{tikzpicture}
\caption{The Nancy Pipeline}\label{fig:nancyPipeline}
\end{figure}

The typechecker and interpreter utilize monads and monad transformers to make implementation easier
and cleaner. A monad is a concept from category theory which has found use as an abstract interface
in programming languages; monad transformers are used to combine different monads together. Specifically,
the typechecker and interpreter use the following monads:

\begin{itemize}
  \item Except: handling errors
  \item Reader: managing typing contexts/current trail
  \item Writer: logging
\end{itemize}

\section{Language Extensions}
In addition to the standard features detailed in the language specification, Nancy has some
additional terms to facilitate the writing of more interesting programs. Specifically, the
implementation includes two base types, natural numbers and booleans, and some
operations on them, specifically a plus term for adding two numbers, an equality
term for checking if two numbers or two booleans are equal, and an if-then-else
term for control flow. The typing rules and operational semantics for these terms
are routine.

In addition to these terms, Nancy has some derived forms which make certain programs more readable.
These include a let form of writing application, lambda functions with multiple parameters, and
audited composition with multiple parameters. Figure~\ref{fig:nancyDerivedForms} depicts some examples
of these derived forms, with the equivalent terms to which they are parsed.

\begin{figure}
  \begin{center}
  \begin{tabular}{|p{6cm}|p{6cm}|}
    \hline
		\multicolumn{1}{|c|}{\textbf{Derived Form}}
		&\multicolumn{1}{c|}{\textbf{Parsed Equivalent}} \\ \hline
    \begin{lstlisting}[style=table]
fun (x:int) (y:int) ->
  x + y
    \end{lstlisting}&
    \begin{lstlisting}[style=table]
fun (x:int) ->
  fun (y:int) ->
    x + y
    \end{lstlisting} \\ \hline
    \begin{lstlisting}[style=table]
let (x:int) = 1 in x
    \end{lstlisting}&
    \begin{lstlisting}[style=table]
(fun (x:int) -> x) 1
    \end{lstlisting} \\ \hline
    \begin{lstlisting}[style=table]
let (x:int) = 1,
    (y:int) = 2
in x + y
    \end{lstlisting}&
    \begin{lstlisting}[style=table]
let (x:int) = 1
in let (y:int) = 2
   in x + y
    \end{lstlisting} \\ \hline
    \begin{lstlisting}[style=table]
let! (x:int) = !1,
     (y:int) = !2
in <x> + <y>
    \end{lstlisting}&
    \begin{lstlisting}[style=table]
let! (x:int) = !1
in let! (y:int) = !2
   in <x> + <y>
    \end{lstlisting} \\ \hline
  \end{tabular}
  \end{center}
\caption{Nancy Derived Forms}\label{fig:nancyDerivedForms}
\end{figure}

\section{Implementing a History-Aware Language}
In this section, we discuss some unique problems faced when implementing an interpreter for a history-aware language.

\subsection{Trail Management}
One obvious concern with a history-aware language is the fact that, if dealt with naively,
the history grows indefinitely. This is the case with the Nancy interpreter; as reduction proceeds,
the trail describing reduction necessarily grows. Aside from the large memory overhead of keeping track of this trail,
a trail quickly becomes so complicated so as to be difficult to understand and deal with
in any useful way. Even the relatively simple Example~\ref{ex:2} and Example~\ref{ex:3} given earlier
have very complex trails.

One step we have taken to help in understanding these complex trails is the implementation of ``prose trails''.
Essentially, this is just another way of displaying the information encoded in the trails. Instead of displaying
the trails using their trail constructor syntax, we instead produce a series of sentences which describe how
reduction is proceeding. For example, the example trail given in the introduction for Example~\ref{ex:1} has the
following prose trail:

\scriptsize
\begin{alltt}
We witness \verb|(((fun (x:int) -> (x + 1)) 1))|
We reduce under an application, reducing the left term:
  We witness \verb|fun (x:int) -> (x + 1)|
and the right term:
  We witness \verb|1|
We perform a beta step, substituting the formal parameter \verb|(x:int)| with \verb|1| in \verb|fun (x:int) -> (x + 1)|
We reduce under a plus, reducing the left term:
  We witness \verb|1|
and the right term:
  We witness \verb|1|
\end{alltt}
\normalsize

These trails convey the same information as the trail constructors, but are much easier to understand. While this
does not help to simplify dealing with trails when writing a Nancy program, it does help to understand what
information Nancy is encoding in its trails.

By far the most complex part of both the interpreter is the management of these trails. As is demonstrated
by the operational semantics, when a term is reduced using several ``sub-reductions'', the state of the
trail must be updated with each reduction and threaded through to the next. In the current implementation,
Nancy handles this with a helper function for each trail update that must be done; used in concert with the
Reader monad which manages the trail state, an update to the current trail is performed for each ``sub-reduction''.
A more complex case is that of interpreting the branches of a trail replacement, using the B-TRPL rule. This
reduction requires a number of ``sub-reductions'' equal to the number of branches in the trail replacement,
and the trail describing the reduction of each previous branch must be reflected in the trail when reducing
the next branch. This could of course be performed with the same approach as the other rules, with a helper
function to set the trail for reducing each branch; however, this results in a lot of additional functions.
Additionally, if new terms are added, along with new trail constructors, a new helper function must be added
and the rule must be updated to reduce that trail branch.

This hints at a more general problem related to trail constructors: adding a new term to the language
requires the addition of a new witness, trail constructor, and trail branch for that term. This makes
the addition of new terms much more complicated than in traditional languages. In the current implementation,
we alleviate the concern raised above regarding threading the updated trail through the interpretation of
trail replacement branches by converting the branches from their native representation as a record type to a list.
This sacrifices some of the type safety that the original record type provides, ensuring that all required branches exist,
but treating the branches as a list allows for the addition of new terms and their associated trail constructors and branches,
without any change required to the reduction rule for the trail replacement.

$\lamhc$ has something of a remedy for the complexity trail branches add to the language: in addition to the trail branches for each trail constructor,
$\lamhc$ also has a default branch; the default branch will be used if no branch is given for a trail constructor.
We like the concept of the default branch, as it can make the trail inspection terms much more simple, but we were uncertain of
how to correctly implement it. Since trail branches have varying arity, based on the trail constructor they map to,
it is unclear how a default branch can fill the place of two or more missing branches with different artiy.
We favored simplicity and correctness in this case, removing the default branch and requiring a branch to be given for
every trail constructor, but we can certainly see the usefulness a default branch would have on making trail inspection terms
less verbose and more readable.

A more trivial concern regarding trails is how to deal with terms which do not have a trail. All of the previously given
example programs have been of ``bang'' terms, as they start with a \verb|!|. But what of terms which are not ``bang'' terms?
For example, consider the simple program \verb|(fun (x:int) -> x) 1|, which applies the integer identity function to the
argument 1; note that it does not start with at \verb|!|. How should Nancy deal with this term? The process of reducing
the initial term to the final value \verb|1| will necessarily produce some trail, but where will this trail be stored,
since there is no enclosing ``bang''? The answer to this is quite simple: when dealing with a history-aware language, it
does not make sense for a term to not be enclosed in a ``bang''; since all terms will produce a trail, there must be
some enclosing ``bang'' in which that trail can be stored. This can be handled quite simply at the parsing stage; if
the top-level term is a ``bang'' term, do nothing as there is no problem. If it is not, simply wrap it in a ``bang''
term. The example given above, then, would be parsed as \verb|!(fun x:int) -> x) 1|, with the original term wrapped
in a \verb|!|.

\subsection{Substitution \& Environments}
There are two main ways to handle variable resolution in an interpreter: substitution and environments. The
process of variable resolution is equivalent to the $\beta$-step of a $\lambda$-calculus; when a lambda is
applied to an argument, any occurrences of the formal parameter of the body of the lambda must be
replaced with the argument. With substitution, the term which is the body of the lambda is mapped over,
and any occurrence of the formal parameter is replaced with the argument. When using environments,
an environment containing a mapping between variables and values is part of all reduction steps; when a lambda
is applied, the formal parameter is added to the environment, mapping to the argument; the body is reduced
with this environment, and if the formal parameter is found, it will be looked up in the environment.
Typically, it is simple to convert between the use of substitution and environments; a $\lambda$-calculus written
using one can typically be easily converted to use the other, and maintain equivalent behavior. For implementing
interpreters, environments are generally preferred as they tend to be more straightforward to implement. Nancy
uses substitution, however, because the equivalence of substitution and environments in a language tracking
history seems to be non-trivial. Nancy has two types of variables, simple and audited. For simple variables,
the use of environments is indeed trivially implemented, as simple variables have no history. Conversely,
audited variables do have history, so an environment for audited variables is non-trivial. Such
``audited environments'' would need to somehow store the history associated to these variables, so that
when they are looked up their history is reflected in the trail. This is easily handled by the audited variable substitution
function given in the language specification, but its equivalent when using environments is unclear.

\subsection{Witnesses \& Terms}
As is made clear by Nancy's syntax, witnesses and terms are almost exactly the same; the only difference being that
bang witnesses do not have a trail, while bang terms do. With this in mind, it is beneficial to model them
using the same type, with a subtype for the bang term which can include either a trail for terms, or no trail for witnesses.
This is how witnesses and terms are implemented in Nancy; there is a type constructor \verb|(Expr a)| in which the type
variable \verb|a| represents the type of the trail of the bang term. Following from this, terms are simply a type alias
to this expression type, \verb|(Expr Trail)|, while witnesses are a type alias \verb|(Expr NoTrail)|. This greatly simplifies
implementation, reducing two types with the same structure to one.
