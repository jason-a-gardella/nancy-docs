\chapter{Language Specification}
In this section, we provide the formal specification for the Nancy language. This specification follows
almost exactly from the definition of $\lamhc$ provided by Riciotti and Cheney~\cite{lamhc}.
The main difference is in semantics; Nancy's semantics are defined in big-step as opposed to the small-step definition
of $\lamhc$.

\section{Syntax}

The full syntax of Nancy is depicted in Figure~\ref{fig:nancySyntax}. Again, this syntax follows
very much from the syntax of $\lamhc$. The terms of note are the bang term $\bangtrm{q}{M}$ which
represents a term $M$ whose reduction process will be stored in the trail $q$, the audited let
term $\lettrm{u}{M}{N}$ which is used to compose bang terms, the audited variable term $\avartrm{u}$
which is used to refer to a variable introduced by the audited let, and the trail inspection term
$\titrm{\trmbranches}$, which inspects a trail, essentially converting it to a term which will
then follow standard reduction procedures. Note also that every term has a corresponding witness. All witnesses mirror their terms exactly, except for the bang witness
$\bangwit{s}$ which does not have a trail $q$.


\begin{figure}[ht]
  \begin{alignat*}{7}
    &\textrm{Terms:} &&   &&                          &&\textrm{Witnesses:}\\
    &M, N, \ldots ::=&&\  &&\vartrm{a}           \quad&&s, t, \ldots ::=&&\ &&\varwit{a}           \qquad &&\textrm{simple variable}\\
    &                &&|\ &&\avartrm{u}          \quad&&                &&|\ &&\avarwit{u}          \qquad &&\textrm{audited variable}\\
    &                &&|\ &&\lamtrm{a}{M}        \quad&&                &&|\ &&\lamwit{a}{s}        \qquad &&\textrm{abstraction}\\
    &                &&|\ &&\apptrm{M}{N}        \quad&&                &&|\ &&\appwit{s}{t}        \qquad &&\textrm{application}\\
    &                &&|\ &&\bangtrm{q}{M}       \quad&&                &&|\ &&\bangwit{s}          \qquad &&\textrm{audited unit}\\
    &                &&|\ &&\lettrm{u}{M}{N}     \quad&&                &&|\ &&\letwit{u}{s}{t}     \qquad &&\textrm{audited composition}\\
    &                &&|\ &&\titrm{\trmbranches} \quad&&                &&|\ &&\tiwit{\witbranches} \qquad &&\textrm{trail inspection}
  \end{alignat*}
  \begin{alignat*}{4}
    &\textrm{Trail Const}&&\textrm{ructors:}           \quad&&\textrm{Trail Inspection Branch Labels:}\\
    &q, q', \ldots ::=&&\ \ \rtrl{s}                   \quad&&\psi, \psi', \ldots ::=\rbrch\ |\ \tbrch\ |\ \betabrch \\
    &                &&|\ \ttrllong{q}{q'}             \quad&&\quad\quad\quad\quad\quad\ \ \betaboxbrch\ |\ \tibrch\ |\ \lambrch \\
    &                &&|\ \batrl{a}{s}{t}              \quad&&\quad\quad\quad\quad\quad\ \ \appbrch\ |\ \letbrch\ |\ \trplbrch \\
    &                &&|\ \bbtrl{u}{s}{t} \\
    &                &&|\ \lamtrl{a}{q}                \quad&&\textrm{Trail Inspection Branches:}\\
    &                &&|\ \apptrl{q}{q'}               \quad&&\witbranches, \witbranches', \ldots ::=\ \{\tibranch{s}{\psi}\} \\
    &                &&|\ \lettrl{u}{q}{q'}            \quad&&\trmbranches, \trmbranches', \ldots ::=\ \{\tibranch{M}{\psi}\} \\
    &                &&|\ \titrl{q}{\witbranches}      \quad&&\trmbranches^{V}, \trmbranches^{V'}, \ldots ::=\ \{\tibranch{V}{\psi}\} \\
    &                &&|\ \trpltrl{\trlbranches}       \quad&&\trlbranches, \trlbranches', \ldots ::=\ \{\tibranch{q}{\psi}\}
  \end{alignat*}

  \caption{Nancy Syntax}\label{fig:nancySyntax}
\end{figure}

There are 9 different trail constructors, which are used to encode how a term is reduced:

\begin{itemize}
  \item A reflexivity trail $\rtrl{s}$ provides a witness $s$ to some term.
  \item A transitivity trail $\ttrllong{q}{q'}$ indicates that the reduction described by trail $q$
    is followed by the reduction described by trail $q'$.
  \item A beta trail $\batrl{a}{s}{t}$ indicates that a beta step (substitution in an application)
    has occurred, where the variable $a^A$ in the term witnessed by $s$ was replaced by the term witnessed by $t$.
  \item A beta-box trail $\bbtrl{u}{s}{t}$ indicates that a beta-box step (substitution in an audited composition)
    has occurred, where the variable $u^A$ in the term witnessed by $t$ was replaced by the term witnessed by $s$.
  \item A lambda trail $\lamtrl{a}{q}$ indicates that the reduction described by $q$ was performed under a
    lambda with parameter $a^A$.
  \item An application trail $\apptrl{q}{q'}$ indicates that reduction was performed under an application,
    with the reduction of the left term of the application described by $q$ and the reduction
    of the right term described by $q'$.
  \item An audited composition trail $\lettrl{u}{q}{q'}$ indicates that reduction was performed under
    an audited composition with parameter $u^A$, where the reduction of the argument of the composition
    is described by $q$ and the reduction of the body is described by $q'$.
  \item A trail inspection trail $\titrl{q}{\witbranches}$ indicates that a trail inspection was performed,
    loading the trail $q$, with the terms of the branches of the inspection witnessed by $\witbranches$.
  \item A trail replacement trail $\trpltrl{\trlbranches}$ indicates that a reduction was performed under
    a trail inspection, where the reduction of each branch of the inspect term is described by $\trlbranches$.
\end{itemize}

There are 9 trail inspection branch labels, one for each trail constructor. Finally, there are 4 types of trail inspection branches
which each map different variations of data to the 9 trail branch labels, namely branches of witnesses ($\witbranches$),
terms ($\trmbranches$), values ($\trmbranches^{V})$ and trails ($\trlbranches$).

\section{Auxiliary Functions}

We define a set of auxiliary functions which are used during typing and term reduction. Again, these
definitions follow closely the analogous definitions of $\lamhc$. The following definitions have
many cases in which the function being defined is recursively mapped over the subterms of the structure;
these cases are omitted for brevity.  The full definitions of these functions, with the recursive components
fully detailed, are included in Appendix A.

\subsection{Witness Substitution}

First, we define the substitution over witnesses of both simple and audited variables. In both cases,
the definition is trivial: all matching variables in a witness are replaced with the provided witness.

For $\gamma = \sub{t}{a}$, substitution of simple variables on a witness $r$,
denoted as $r\gamma$, is defined as:

\begin{align*}
  \witsubsimplerule{\varwit{c}}{\begin{cases}
    t  &(a = c) \\
    \varwit{c} &(a \neq c)
  \end{cases}} \\
  \witsubsimplerule{\bangwit{s}}{\bangwit{s}} \\
\end{align*}

For $\delta = \sub{t}{u}$, substitution of audited variables on a witness $r$,
denoted as $r\delta$, is defined as:

\begin{align*}
  \witsubauditedrule{\varwit{a}}{\varwit{a}} \\
  \witsubauditedrule{\avarwit{v}}{\begin{cases}
    t  &(u = v) \\
    \varwit{v} &(u \neq v)
  \end{cases}} \\
\end{align*}

Note that simple variable substitution does not recurse into bang witnesses, while
audited variable substitution does.

\subsection{Audited Variable Substitution}

Next, we define two forms of substitution over terms. The first produces a term as its result, while
the second produces a trail. In both substitutions, we use the shorthand:

\[
  \delta = [(N, q, t)/u],\ \text{fix}\ \delta' = [t/u]
\]

\subsubsection{Term Substitution}

\begin{align*}
  \trmsubrule{\avartrm{v}}{\begin{cases}
    N           &(u = v) \\
    \avartrm{v} &(u \neq v)
  \end{cases}} \\
  \trmsubrule{(\bangtrm{q'}{R})}{\bangtrm{\ttrl{q'\delta'}{\trlsub{R}}}{(\trmsub{R})}} \\
\end{align*}

\subsubsection{Trail Substitution}

\begin{align*}
  \trlsubrule{\avartrm{v}}{\begin{cases}
    q  &(u = v) \\
    \rcd{v} &(u \neq v)
  \end{cases}} \\
  \trlsubrule{\bangtrm{q'}{R}}{\rtrl{\bangwit{\src{q'}\delta'}}} \\
\end{align*}

\subsection{Compute Witness}

The compute witness function produces a witness for a given term. All cases are trivial, since
terms map directly to witnesses, except for the bang case, in which a bang witness
is returned with the inner witness being the source of the bang's trail.

\begin{align*}
  \cdrule{\bangtrm{q}{N}}{\bangwit{\src{q}}}
\end{align*}

\subsection{Source}

Trails represent how one term reduces to another term. With this in mind, the source of a trail
is the initial term of that trail, before any reduction took place.

\begin{align*}
  \srcrule{\rtrl{s}}{s} \\
  \srcrule{\ttrllong{q_1}{q_2}}{\src{q_1}} \\
  \srcrule{\batrl{a}{s_1}{s_2}}{\appwit{\lamwit{a}{s_1}}{s_2}} \\
  \srcrule{\bbtrl{v}{s_1}{s_2}}{\letwit{v}{\bangwit{s_1}}{s_2}} \\
  \srcrule{\titrl{q_1}{\witbranches}}{\tiwit{\witbranches}} \\
  \srcrule{\lamtrl{a}{q_1}}{\lamwit{a}{\src{q_1}}} \\
  \srcrule{\apptrl{q_1}{q_2}}{\appwit{\src{q_1}}{\src{q_2}}} \\
  \srcrule{\lettrl{v}{q_1}{q_2}}{\letwit{v}{\src{q_1}}{\src{q_2}}} \\
  \srcrule{\trpltrl{\trlbranches}}{\tiwit{\src{\trlbranches}}}
\end{align*}

$\src{\trlbranches}$ is defined pointwise.

\subsection{Target}

The target of a trail is the final term of that trail, after all reduction has finished.

\begin{align*}
  \tgtrule{\rtrl{s}}{s} \\
  \tgtrule{\ttrllong{q_1}{q_2}}{\tgt{q_2}} \\
  \tgtrule{\batrl{a}{s_1}{s_2}}{\appsub{s_1}{s_2}{a}} \\
  \tgtrule{\bbtrl{v}{s_1}{s_2}}{\appsub{s_2}{s_1}{v}} \\
  \tgtrule{\titrl{q_1}{\witbranches}}{q_1 \witbranches} \\
  \tgtrule{\lamtrl{a}{q_1}}{\lamwit{a}{\tgt{q_1}}} \\
  \tgtrule{\apptrl{q_1}{q_2}}{\appwit{\tgt{q_1}}{\tgt{q_2}}} \\
  \tgtrule{\lettrl{v}{q_1}{q_2}}{\letwit{v}{\tgt{q_1}}{\tgt{q_2}}} \\
  \tgtrule{\trpltrl{\trlbranches}}{\tiwit{\tgt{\trlbranches}}}
\end{align*}

$\tgt{\trlbranches}$ is defined pointwise.

\subsection{Abbreviations}

We introduce some abbreviations to make the typing rules and semantics more readable.
$\rtrl{\cd{M}}$, the witness of a term wrapped in a reflexivity trail is often
used to represent the original witness to that term in a trail; it is abbreviated
$\rcd{M}$.

The transitivity trail $\ttrllong{q_1}{q_2}$ represents that the trail $q_1$ is followed
by the trail $q_2$; it is abbreviated $\ttrl{q_1}{q_2}$.

\section{Type System}

We now introduce the typing system for Nancy. Nancy has three type constructors, pictured in Figure~\ref{fig:nancyTypes};
the base type $P$, the standard function type $\lamtyp{A}{B}$, and the audited type $\bangtyp{s}{A}$, which is a type with an associated
witness $s$.

\begin{figure}[ht]
\[
  A, B, \ldots ::= P\ |\ \lamtyp{A}{B}\ |\ \bangtyp{s}{A}
\]
\caption{Nancy Types}\label{fig:nancyTypes}
\end{figure}

The typing rules for Nancy are given in Figure~\ref{fig:nancyTypingRules}. Most of these rules are trivial, following
directly from the typing of the simply typed $\lambda$-calculus. However, in all cases, the type of the term also
has a witness which encodes the typing derivation for that term. For example, the type of a lambda $\lamtyp{A}{B}$ has
a witness to its typing derivation $\lamwit{a}{s}$, where $a^A$ is the parameter of the lambda and $s$ witnesses the
body of the lambda.

\begin{figure}[ht]

\[
  \reduce{T-VAR}
  {
    \intenv{\trmtyp{\vartrm{a}}{A}}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\vartrm{a}}{A}{\varwit{a}}}
  }
\]

\[
  \reduce{T-ABS}
  {
    \withenvs{\wenv}{\tenv, \trmtyp{\vartrm{a}}{A}}{\trmtypwit{M}{B}{s}}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\lamtrm{a}{M}}{\lamtyp{A}{B}}{\lamwit{a}{s}}}
  }
\]

\[
  \reduce{T-APP}
  {
    \begin{gathered}
      \withenvs{\wenv}{\tenv}{\trmtypwit{M}{\lamtyp{A}{B}}{s}} \\
      \withenvs{\wenv}{\tenv}{\trmtypwit{N}{A}{t}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\apptrm{M}{N}}{B}{\appwit{s}{t}}}
  }
\]

\[
  \reduce{T-AVAR}
  {
    \inwenv{\avartrm{u} :: A}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\avartrm{u}}{A}{\avarwit{u}}}
  }
\]

\[
  \reduce{T-BANG}
  {
    \begin{gathered}
      \withenvs{\wenv}{\noenv}{\trmtypwit{M}{A}{s}} \\
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\bangtrm{q}{M}}{\bangtyp{s}{A}}{\bangwit{s}}}
  }
\]

\[
  \reduce{T-LET}
  {
    \begin{gathered}
      \withenvs{\wenv}{\tenv}{\trmtypwit{M}{\bangtyp{r}{A}}{s}} \\
      \withenvs{\wenv, u :: A}{\tenv}{\trmtypwit{N}{C}{t}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{
      \trmtypwit{\lettrm{u}{M}{N}}
        {\appsub{C}{r}{u}}
        {\letwit{u}{s}{t}}
    }
  }
\]

\[
  \reduce{T-TI}
  {
    \begin{gathered}
      \withenvs{\wenv}{\noenv}{\trmtypwit{\trmbranches(\psi)}{\mathcal{T}^B(\psi)}{\witbranches(\psi)}}
    \end{gathered}
  }{
    \withenvs{\wenv}{\tenv}{\trmtypwit{\titrm{\trmbranches}}{B}{\tiwit{\witbranches}}}
  }
\]

\caption{Nancy Typing Rules}\label{fig:nancyTypingRules}
\end{figure}

Note that Nancy has two typing contexts, the context $\Gamma$ for simple variables, which are introduced in the
T-VAR rule, and the context $\Delta$ for audited variables, introduced in the T-AVAR rule. The interesting, non-trivial
rules are those of terms which interact with the provenance information in Nancy, namely T-BANG, T-LET, and T-TI.
The T-BANG rule types bang terms; the type of a bang term is an audited type $\bangtyp{s}{A}$, where $A$ is the body
of the bang and $s$ is the witness to the body. The associated witness is $\bangwit{s}$. Note that this rule differs
from the analogous rule for $\lamhc$, in that is has no notion of trail equality. Trail equality is necessary for
proving properties of $\lamhc$, like strong normalization, but since Nancy is designed as a programming language,
information on trail equality is not needed.

The T-LET rule types audited composition. The type of the argument term is expected to be an audited type
$\bangtyp{r}{A}$. The body of the composition is then typed with this type introduced to the audited typing
context $\Gamma$. As we have effectively substituted the type of the argument for audited variable $u$ in the body, we must reflect this substitution in the
type of the body; all occurrences of the audited variable $u$ in the type of the body are substituted with the witness to the argument term, $r$.

\begin{figure}[ht]
\[
  \mathcal{T}^{B}(\psi) \triangleq
    \begin{cases}
      B &(\psi = \rbrch, \betabrch, \betaboxbrch, \tibrch) \\
      \lamtyp{B}{B} &(\psi = \lambrch) \\
      \lamtyp{B}{\lamtyp{B}{B}} &(\psi = \tbrch, \appbrch, \letbrch) \\
      \overbrace{B \supset B \supset \ldots}^\textrm{9 times} \subset B &(\psi = \trplbrch)
    \end{cases}
\]
\caption{Nancy Inspection Branch Types}\label{fig:nancyBranchTypes}
\end{figure}

The final typing rule T-TI types trail inspection. Typing a trail inspection term requires typing the term
provided for each branch of the inspection. The types required for each branch are given by $\mathcal{T}^{B}$
in Figure~\ref{fig:nancyBranchTypes}. The type of a branch must be a function of arity equal to the number
of subtrails in the trail constructor associated to that branch. The trails without arity, for example,
simply map to some type $B$, while the transitivity branch $\tbrch$ maps to a function $\lamtyp{B}{\lamtyp{B}{B}}$,
because a transitivity trail $\ttrllong{q}{q'}$ has two subtrails $q$ and $q'$.

\section{Semantics}

We now define the operational semantics for the Nancy language. To better facilitate the implementation
of the interpreter, the semantics are defined in big-step form, as opposed to the small-step form
of $\lamhc$. We first define the terms which are considered to be values, depicted in Figure~\ref{fig:nancyValues}.
These are the base values $B$, simple variables, audited variables, lambda terms (with their body not reduced),
and bang terms where the enclosed term is a value.

\begin{figure}[ht]
\[
  V ::= B\ |\ \vartrm{a}\ |\ \avartrm{u}\ |\ \lamtrm{a}{M}\ |\ \bangtrm{q}{V}\
\]
\caption{Nancy Values}\label{fig:nancyValues}
\end{figure}

The operational semantics for Nancy appear in Figure~\ref{fig:nancySemantics}. A statement
$(M, q_1) \Downarrow^{q_2} V$ is read as ``term $M$ with current trail $q_1$ reduces in a big-step
to $V$, producing trail $q_2$''. The trail $q_2$ describes the reduction performed in the big-step
from $M$ to $V$. In the trivial case B-V when reducing a value, the result is the
same value $V$. Note that, even though no reduction has been performed, the trail of the ``reduction''
is $\rcd{V}$. In a sense, the value $V$ is witnessed to have remained the same.

The B-BANG reduces a bang term. The important part of this reduction is that when the body of the
bang $M$ is reduced, the trail of the reduction $q_3$ does not escape the bang. The bang
captures $q_3$, appending it to the original trail $q_1$ using the transitivity trail constructor.
The returned trail $\rtrl{\bangwit{\cd{M}}}$ witnesses that reduction was performed under
a bang term, but the trail of that reduction is not exposed.

\begin{figure}[ht]
\[
  \reduce{B-V}
    {}
    {\bstep{V}{q}{V}{\rcd{V}}}
\]

\[
  \reduce{B-BANG}
    {\bstep{M}{q_1}{V}{q_3}}
    {\bstep{\bangtrm{q_1}{M}}{q_2}{\bangtrm{\ttrl{q_1}{q_3}}{V}}{\rtrl{\bangwit{\cd{M}}}}}
\]

\[
  \reduce{B-$\beta$}
  {
    \begin{gathered}
      \bstep{M}{q_1}{\lamtrm{a}{M'}}{q_2} \\
      \bstep{N}{\ttrl{q_1}{\apptrl{q_2}{\rcd{N}}}}{V}{q_3} \\
      \bstep{\appsub{M'}{V}{a}}{\ttrl{q_1}{\ttrl{\apptrl{q_2}{q_3}}{\batrl{a}{\tgt{q_2}}{\tgt{q_3}}}}}{W}{q_4}
    \end{gathered}
  }
  {
    \bstep{\apptrm{M}{N}}{q_1}{W}{\ttrl{\apptrl{q_2}{q_3}}{\ttrl{\batrl{a}{\tgt{q_2}}{\tgt{q_3}}}{q_4}}}
  }
\]

\[
  \reduce{B-$\betabox$}
  {
    \begin{gathered}
      \bstep{M}{q_1}{\bangtrm{q_2}{V}}{q_3} \\
      \delta = [(V, q_2, \src{q_2})/u] \\
      \bstep{\trmsub{N}}{\ttrl{q_1}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\trlsub{N}}}}}{W}{q_4}
    \end{gathered}
  }
  {
    \bstep{\lettrm{u}{M}{N}}{q_1}{W}{\ttrl{\lettrl{u}{q_3}{\rcd{N}}}{\ttrl{\bbtrl{u}{\src{q_2}}{\cd{N}}}{\ttrl{\trlsub{N}}{q_4}}}}
  }
\]

\[
  \reduce{B-TRPL}
  {
    \begin{gathered}
      \bstep{\trmbranches_{r}}{q_1}{\trmbranches_{r}^{V}}{q_r} \\
      \bstep{\trmbranches_{t}}{\ttrl{q_1}{\trpltrl{q_r, \rcd{\trmbranches_{t}}, \ldots}}}{\trmbranches_{t}^{V}}{q_t} \\
      \ldots \\
      \bstep{\trmbranches_{trpl}}{\ttrl{q_1}{\trpltrl{q_r, q_t, \ldots, \rcd{\trmbranches_{trpl}}}}}{\trmbranches_{trpl}^{V}}{q_{trpl}}
    \end{gathered}
  }
  {
    \bstep{\trmbranches}{q_1}{\trmbranches^{V}}{\trpltrl{q_r, q_t, \ldots, q_{trpl}}}
  }
\]

\[
  \reduce{B-TI}
  {
    \begin{gathered}
      \bstep{\trmbranches}{q_1}{\trmbranches^{V}}{q_{\trmbranches}} \\
      \bstep{(\ttrl{q_1}{q_{\trmbranches}})\trmbranches}{\ttrl{q_1}{\ttrl{q_{\trmbranches}}{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}}}{W}{q_2}
    \end{gathered}
  }
  {
    \bstep{\titrm{\trmbranches}}{q_1}{W}{\ttrl{q_{\trmbranches}}{\ttrl{\titrl{\ttrl{q_1}{q_{\trmbranches}}}{\cd{\trmbranches}}}{q_2}}}
  }
\]
\caption{Nancy Semantics}\label{fig:nancySemantics}
\end{figure}

The B-$\beta$ rule is the reduction rule for a $\beta$-step, in which a lambda function is applied
to an argument. The reduction takes place in 3 steps, as in the analogous rule for the standard
lambda calculus. However, note that the current trail is updated for each step in the reduction,
to reflect what previous reduction has occurred. For example, when reducing the right term, $N$, of
the application, the left term has already been reduced; as a result, the right term is reduced
under a trail which is the original trail, $q_1$, followed by an application trail constructor containing
a trail $q_2$ which describes the reduction of the left term, and a trail $\rcd{N}$ which witnesses
the right term, which has not yet been reduced.

The B-$\betabox$ rule encodes a $\betabox$-step, in which a value enclosed in bang is substituted
in another term; this allows for composition of bang terms. The argument term to the composition
$M$ is first reduced to a bang term containing a value, $\bangtrm{q_2}{V}$. Note that the trail $q_2$
represents the provenance of the value $V$.  The trail substitution function $\ltimes$ then is applied
to the body of the composition $N$, substituting occurrences of the audited variable $u$ with $V$,
and also substituting the provenance of $V$, $q_2$, in $N$. Note that the trail under which the
reduction of the body is performed is augmented with a trail describing the reduction of the argument,
a trail describing the $\betabox$-step, and finally a trail produced by executing the trail substitution
function $\times$ on the body term $N$. The result $W$ and its trail $q_4$ are produced from this
final reduction.

The B-TRPL rule describes how a set of trail branches containing terms, $\trmbranches$, is reduced.
The reduction is somewhat complex because, when reducing a branch, the current trail must represent
the reduction of previous branches. For example, when reducing the transitivity branch's term $\trmbranches_{t}$,
the current trail is the initial trail, $q_1$, followed by a trail replacement trail constructor which
indicates in  its first subtrail $q_r$ the reduction of the reflexivity case, which has already occurred.
This pattern of reduction and threading through the reduction of previous branches in the trail continues
until all branches are reduced to values, yielding the result $\trmbranches^{V}$.

The final rule B-TI describes reduction for a trail inspection term $\iota{\trmbranches}$. First, the
trail branches are reduced to values, using the B-TRPL rule; this entire reduction is recorded in the
trail $q_{\trmbranches}$. Next, the trail branches of values are applied to the original trail $q_1$ followed
by the trail $q_{\trmbranches}$ describing the reduction of the branches; we must encode this reduction here
as it has already occurred. The resulting term is then reduced to the final value $W$. Note that this reduction was performed under the original trail $q_1$, followed
by the trail $q_{\trmbranches}$ describing the reduction of the branches, followed by a $ti$ trail containing
the inspected trail $\ttrl{q_1}{q_{\trmbranches}}$, plus a witness $\cd{\trmbranches}$ to the branches before they were reduced.
