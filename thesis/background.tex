\chapter{Background}
In this chapter, we explain some preliminaries required to sufficiently understand the rest of the thesis.
Specifically, we describe the $\lambda$-calculus, which is used to specify the Nancy language, the Curry-Howard
correspondence, which gives Nancy a strong foundation based in formal logic, and the Justification Logic,
which Nancy is linked to through this correspondence.

\section{The Lambda Calculus}
In 1940, Alonzo Church proposed a simply-typed $\lambda$-calculus~\cite{churchTypedLambda} which avoids some paradoxical properties of his original, untyped $\lambda$-calculus.
After the advent of computing technology, it was found that the simply typed $\lambda$-calculus is useful not only for modeling mathematics, but also
for modeling programming languages. It is used by computer scientists to specify new programming languages in a way which is
robust and allows one to prove interesting properties of a language, like correctness and soundness.

A typed $\lambda$-calculus is generally specified in four parts:

\begin{enumerate}[1.,nolistsep,leftmargin=3\parindent]
  \item Types
  \item Terms
  \item Typing Rules
  \item Operational Semantics
\end{enumerate}

As an example, let us define a simple $\lambda$-calculus. The types and terms for this calculus appear in Figures~\ref{fig:types} and~\ref{fig:terms}.
The calculus has two type constructors, numerals ($\mathbb{N}$) and functions ($\tau \rightarrow \tau$),
and corresponding terms which are numerals ($n$), variables ($x$), lambda terms which are functions that take an argument of a
given type and have a body, and application which should apply a given lambda function to an argument.

\begin{figure}[ht]
  \centering
  \[
    \tau ::= \mathbb{N}\ |\ \tau \rightarrow \tau
  \]
  \caption{Example Types}\label{fig:types}

  \begin{alignat*}{2}
    M, N, \ldots ::=&\ n\ \ &&\textrm{numeral} \\
                   |&\ x\ \ &&\textrm{variable} \\
                   |&\ \lambda x:\tau.M\ \ &&\textrm{lambda} \\
                   |&\ M N\ \ &&\textrm{application}
  \end{alignat*}
  \caption{Example Terms}\label{fig:terms}
\end{figure}

Using these definitions, we can now state whether or not given strings of characters are terms of our $\lambda$-calculus. For example,
\colorbox{lightgray}{$(\lambda x:\mathbb{N}.x)\ 1$} is a term. However, we are still able to write terms which are not typable; for example,
\colorbox{lightgray}{$(\lambda x:\mathbb{N}.x)\ (\lambda x:\mathbb{N}.4)$} is a term, but it does not seem correct, because we are trying to apply a function
which expects its argument to be of type $\mathbb{N}$ to an argument of type $\mathbb{N} \rightarrow \mathbb{N}$.

In order to specify which terms are typable, we must specify the typing rules for our calculus. These are specified in Figure~\ref{fig:typingRules}.
In these rules, terms are typed under a typing context $\Gamma$, which keeps track of the types associated to variables.
A statement $\Gamma \vdash M:\sigma$ is read as ``term $M$ has type $\sigma$ under typing context $\Gamma$''.
Each typing rule is written using a fraction notation, with the elements on the top being premises,
and the item on the bottom being a conclusion which is justified by those premises.

\begin{figure}[ht]
  \[
    \reduce{T-Nat}
    {}
    {\Gamma \vdash n:\mathbb{N}}
    \qquad
    \reduce{T-Var}
    {x:\sigma \in \Gamma}
    {\Gamma \vdash x:\sigma}
  \]

  \[
    \reduce{T-Lam}
    {\Gamma,x:\tau \vdash M:\sigma}
    {\Gamma \vdash (\lambda x:\tau.M):(\tau \rightarrow \sigma)}
    \qquad
    \reduce{T-App}
    {\Gamma \vdash M:(\tau \rightarrow \sigma)\ \ \Gamma \vdash N:\tau}
    {\Gamma \vdash (M N):\sigma}
  \]
  \caption{Example Typing Rules}\label{fig:typingRules}
\end{figure}

The rules for typing our $\lambda$-calculus are quite simple. A natural number always has type $\mathbb{N}$.
To get the type of a variable, look it up in the current type context $\Gamma$. A lambda has the type $\tau \rightarrow \sigma$ where $\tau$ is given by the lambda's argument type, and $\sigma$ is the
type of the body of the lambda. Note that the body of the lambda is typed under a typing context which has been augmented with the type of the lambda's argument.
An application is typable with type $\sigma$ if the left term is a lambda which returns type $\sigma$ and the right term has the type of the lambda's argument.

With these typing judgements, the term \colorbox{lightgray}{$(\lambda x:\mathbb{N}.x)\ (\lambda x:\mathbb{N}.4)$} is no longer typable,
as the right term is not the correct type for the argument of the left lambda term; the judgement T-App will fail. The typing judgements
therefore reduce the set of terms of our $\lambda$-calculus to terms which are typable.

The type of a term also gives us some indication as to what the result of reducing that term will be. The process of reduction takes a term
and applies the calculus's operational semantics to reduce the term to normal form, or in other words, to a term which is considered a value.
There are many different ways to specify how a term reduces; we will use big-step operational semantics. Big-step semantics take a term and
take one ``big step'' to describe the result of evaluating the term. This is opposed to small-step semantics, in which many sequential ``small steps'' are taken
to transform a term to its result. We prefer big-step semantics in this case as our end goal is to define an interpreter for a programming language, and big-step semantics are generally
easier to use and more efficient when implementing an interpreter. The big-step operational semantics for our simple example calculus are specified in Figure~\ref{fig:semantics}.
The semantics use the same fractional form as the typing rules, with premises on the top and a conclusion on the bottom.

\begin{figure}[ht]
  \[ V ::= n\ |\ x\ |\ \lambda x:\tau.M \]

  \[
    \reduce{Value}
    {}
    {V \Downarrow V}
    \qquad
    \reduce{Application}
    {M \Downarrow (\lambda x:\tau.P)\ \ \ N \Downarrow V\ \ \ P[V/x] \Downarrow W}
    {M N \Downarrow W}
  \]
\caption{Example Semantics}\label{fig:semantics}
\end{figure}

We first define which terms are considered to be normal form, or values ($V$). Numbers, variables, and lambdas are values in our calculus.
We then define how to reduce all of the terms in our calculus. A statement $M \Downarrow N$ is read as ``term $M$ reduces in a big step to term $N$''.

A value $V$ is already in normal form; it reduces to itself. To reduce an application, first reduce the left term to a lambda, then reduce
the right term to a value. We then substitute this value in the body of the lambda; this is denoted by the notation $P[V/x]$,
which can be read as ``substitute all occurrences of the variable $x$ in term $P$ (the body of the lambda) term $V$ (the argument).
We reduce the term resulting from this substitution to produce the final value of the application, $W$.

With these semantics, we can now reduce the example term \colorbox{lightgray}{$(\lambda x:\mathbb{N}.x)\ 1$}. First, we apply
the application rule. We reduce the left term using the value rule since it is already a value, and then
reduce the right term with the value rule since it too is a value. Finally, we substitute the right value,
$1$, in the body of the lambda where we see the variable $x$. This results in the final value, \colorbox{lightgray}{$1$}.

\section{Curry-Howard Correspondence}
The $\lambda$-calculus is interesting for more than its use in describing mathematics and in specifying programming languages.
The type system of the $\lambda$-calculus corresponds very closely with Gerhard Gentzen's system of natural deduction in formal logic.
In fact, this correspondence is not limited to only the $\lambda$-calculus and natural deduction; it has been proven that many systems of
formal logic have direct equivalence to programming language constructs. This phenomenon is known as the Curry-Howard correspondence, as it was
first observed by Haskell Curry in 1934~\cite{curry}, and then expanded on later by William Howard~\cite{howard}.

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.5]{images/correspondence}
  \caption{Some notable examples of the Curry-Howard correspondence, from Phillip Wadler's presentation \textit{Propositions as Types}~\cite{wadler}.}
\end{figure}

The Curry-Howard correspondence is a truly remarkable realization; it strongly suggests that systems of formal logic and computer programming languages
are in fact two sides of the same coin. From this correspondence, both fields can benefit, as developments in one field can likely be extended
to the other. What can formal logic teach us about programming languages, and what can programming languages teach us about formal logic?

\section{Justification Logic and Audited Computation}
Justification Logic was introduced by Sergei Artemov in 2008~\cite{artemov}. In Justification Logic, statements of knowledge are accompanied by a justification
for that knowledge. The statement $A\ [[s]]$ means that proposition $A$ is true with the justifying proof polynomial $s$.

Let us consider the case of a vote between two possible options as an example. There are two options which can be voted for, $A$ and $B$; the option
with the most votes wins. If $a$ represents the number of votes option $A$ received, and $b$ represents the number of votes option $B$ received,
we could state $A\ [[a>b]]$ to express that option $A$ won, or $B\ [[b>a]]$ to express that option $B$ won. Another statement $C\ [[a=b]]$ could
be made to represent a stalemate.

Fransisco Bavera and Eduardo Bonelli proposed a $\lambda$-calculus, $\lamh$~\cite{lamh}, as the computational equivalent of Justification Logic through the
Curry-Howard correspondence. They refer to the computational equivalent as audited computation; instead of proofs justified by proof polynomials, we
instead have programs with trails describing their reduction. A trail is in fact an equivalence between terms, describing how one term was reduced
to another term.

Building on $\lamh$, Wilmer Riciotti and James Cheney proposed another calculus $\lamhc$~\cite{lamhc}, which is a simplified version of $\lamh$. Namely, it removes the
necessity of trail variables through the use of some auxiliary functions. The Nancy programming language is based on $\lamhc$.
